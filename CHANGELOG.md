# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- [Build] #98 : Copy `.desktop` file to build directory before updating-it
## [4.2.0] - 05-31-2021
- [Gui] #97 : Fix icon path for desktop shortcut
- [Gui] #93 : Ask to the user if he want to create a new archive in case of "Cancel" on archive selector at startup
- [Gui] #84 : Refactoring main GUI

## [4.1.1] - 05-08-2021
- [Test] #85 : Improve and review code coverage
- [Doc] #92 : Removing PlantUML class diagram 
- [CI] #94 : CI/CD refactoring and create several badges (coverage, number of tests and release version)
- [Doc] #95 : ReadMe review

## [4.1.0] - 05-01-2021
- [Core] #73 : Check if all files in the archive exist
- [Cli] #91 : Class and (cli) behavior review
- [Gui] #90 : Shortcut to check the archive
- [Cli] #89 : New keyword to check if all files in the archive exist
- [Cli] #75 : Check script
- [Gui] #76 : Script editor with error highlighting

## [4.0.0] - 04-09-2021
- [Cli] #82 : Move `new` keyword to `add`
- [Core] #81 : Create an API to create a new archive
- [Cli] #81 : Create `new` keyword
- [Gui] #81 : GUI implementation to create a new archive

## [3.2.0] - 04-07-2021
- [CI] #86 : Code analysis report
- [Core] #67 : GPG file support to encrypt and decrypt archive
- [Cli] #68 : Add `save` cli command (for GPG support)
- [Gui] #69 : Add menu to save in clear or with the encryption the archive

## [3.1.1] - 03-02-2021
- [Build] #74 : .desktop and icon are only installed when GUI is built
- [Gui] #63 : Fix keywords filtering and keywords display

## [3.1.0] - 02-25-2021
- [Cli] #64 : Ask the user to add additional keywords
- [Gui] #65 : Ask the user to add additional keywords
- [Build] #70 : Automatically configure git project environment if repository was cloned

## [3.0.2] - 02-21-2021
- [CI] #57 : Fixing Debian package 
- [CI] #61 : Add Arch Linux to CI
- [Build] #58 : Create a .desktop entry on `make install`

## [3.0.1] - 12-12-2020
- [CI] #24 : Deploy Debian package
- [Build] #55 : Using official packages instead of sources for third party libraries
- [Build] #51 : Make install command
- [CI] #56 : Running pipelines on Debian too

## [3.0.0] - 12-08-2020
- [Core] #27 : Keywords are now not saved into the archive file in case of confidential documents. This will also reduces the archive size on the filesystem and reduces the amount of RAM needed to store dynamically all ArchiveFiles
- [Core] #46 : Add an entrypoint to select dynamically the archive
- [Gui] #48 : Select the archive file to use at startup
- [Cli] #47 : choose dynamically the archive file with an argument of docarchiver-cli executable

## [2.7.0] - 11-21-2020
- [CI] #49 : Create custom fedora-based docker image with needed packages for pipelines
- [Core] #39 : File type provider
- [Core] #54 : Using PoDoFo instead of PDF-Writter

## [2.6.0] - 11-14-2020
- [Doc] #7 : Inserting class diagram into Doxygen doc
- [Build] #44 : Create `make docker` and `make run` targets to build and run tests into a Docker image
- [Doc] #43 : Create "Website" hosted on GitLab Pages
- [Doc] #4 : Code coverage
- [CI] #42 : Deploy .tar.gz and .zip archives

## [2.5.0] - 11-01-2020
- [Cli] #30 : Pass script as input
- |CI] #37 : Dockerfile
- |Gui] #34 : Add CLI environment into GUI
- [Gui] #32 : Folder recursivity

## [2.4.0] - 10-18-2020
- [Core] #26 : List files recursively
- [Build] #29 : Pull automatically submodule if didn't done yet
- [Cli] #31 : Add files recursively according to their extensions

## [2.3.0] - 10-11-2020
- [Build] #28 : Ninja build
- [Core] #22 : Refactoring read() / raw() methods

## [2.2.1] - 10-11-2020
- [Core] #18 : Pdf file are not successfully parsed

## [2.2.0] - 09-22-2020
- [Gui] #20 : Dedicated pop-up window to list keywords of a file
- [Core] #23 : Fix Debian build

## [2.1.0] - 09-09-2020
- [Gui] #17 : New window to filter file with keywords, path, extension and refactoring main window
- [Gui] #21 : Display the archive file (raw content)
- [Gui] #19 : Floating layout

## [2.0.0] - 09-06-2020
- [Gui] #3 : Add a first version of the GUI (add and remove files and filter them thanks to keywords

## [1.4.0] - 09-05-2020
- [Docs] Add missing Doxygen documentation
- #14 : Do not compile tests with flag `--no-test`
- #15 : No not use GUI with `--no-gui`

## [1.3.0] - 08-27-2020
- [Core] #8 : Configuration file

## [1.2.3] - 08-27-2020
- [Test] #6 : TearUp & TearDown

## [1.2.2] - 08-27-2020
- [Docs] #9 : How to contribute ?
- [CI/CD] #10 : Jobs only on `master` and `develop`
- [Docs] #12 : Commit message template

## [1.2.1] - 08-26-2020
- [Docs] Update UML diagram
- [Bug] #1 : Wrong file path
- [CI] Switch to a Fedora based image

## [1.2.0] - 08-19-2020
- [Core] PDF support

## [1.1.0] - 08-19-2020
- [CLI] Add `find`command

## [1.0.0] - 07-30-2020
- [Core][CLI] Add `list` command
- [Core][CLI] Add `exit` command
- [Core][CLI] Add `help` command
- [Core][CLI] Add `new` command
- [Core][CLI] Add `remove` command
