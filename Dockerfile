FROM registry.gitlab.com/arnauddm/docarchiver:debian

ENV APP_NAME "docarchiver"

# Copying all project's files
COPY . /${APP_NAME}/.

WORKDIR /${APP_NAME}/.

# Create Makefile
RUN ./configure.sh

# Build the project
WORKDIR /${APP_NAME}/build
RUN make

COPY docker/run_docker_test.sh .

ENTRYPOINT [ "./run_docker_test.sh" ]