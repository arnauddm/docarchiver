# Docarchiver

This project is enhanced by a personal issue : how to properly save documents to find them easily ? This software (tool) try to fix this issue by :

* Providing an "archive" mechanism to track files
* Keyword-based research mechanism
* Several CLI commands to search your documents without any graphic interface
* An easy to use graphical interface
* A mechanism to encrypt your archive(s) (if you want to track confidential documents or add sensitive data) 

## Getting Started

### Prerequisites

Following packages are needed :

* `cmake`
* `gcc` or `g++` or `ninja`
* `podofo`
* `qt5` (only for GUI)
* `git` (to retrieve submodules)
* `doxygen` (only for documentation)
* `gpgme` (used for the encryption)

### Installing

Before building the software, you need to generate the Makefile with CMake.
To do this, the [`configure.sh`](configure.sh) allows you to interact easily with CMake :

```shell
$ ./configure.sh --no-test
```

This script can take following options :
* `--no-gui` : compile only CLI
* `--no-test` : do not compile test suite
* `--debug` : enable debug mode
* `--ninja` : enable Ninja compiler

Then, build and install binaries :
```shell
$ cd build
$ make
$ sudo make install
```
Other useful commands :

```shell
$ make doc				# To generate the documentation (Doxygen developer doc)
$ make test 			# To run all unit tests
$ make coreTest 		# To run all tests related to the `Core`
$ make cliTest 			# To run all tests related to the `Cli`
$ make docker 			# To build the Docker image to "emulate" the Gitlab CI/CD
$ make run 				# To run the Docker image previously created
```

## Documentation

An external website is reachable [here](https://arnauddm.gitlab.io/docarchiver/). This website is automatically updated when a new release is publised and contains binaries (with all OS-based packages - for example `.deb` package) and useful informations (Getting Starting part).

Wiki pages will be added soon with a sort of `User Guide` about `How to use the CLI and the GUI to search and retrieve my files ?`

## Authors

  - [**Arnaud DE MATTEIS**](https://gitlab.com/arnauddm)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

However, you must respect these rules :

* Your commit message must respect the template [`.commitMessage`](.commitMessage), to apply this template, execute this command : 
```bash
$ git config --local include.path ../.gitconfig
```
* Tests must be written and must pass
* Document your code thanks to Doxygen
* Submit a Merge Request when have done your changes

## License

This project is licensed under the [GNU GPLv3](LICENSE.md)
Creative Commons License - see the [LICENSE.md](LICENSE.md) file for
details
