#include "IOMock.hpp"

/**
 * @brief Construct a new IOMock::IOMock object
 * 
 */
IOMock::IOMock(void)
{

}

/**
 * @brief Destroy the IOMock::IOMock object
 * 
 */
IOMock::~IOMock(void)
{

}

/**
 * @brief Append next "mocked" user input
 * 
 * @param input Next input
 */
void IOMock::addNextUserInput(const std::string & input)
{
    _userInputs.push(input);
}

/**
 * @brief Returns the next user input
 * 
 * @return std::string empty string if no remaining input, otherwise returns the "mocked" input
 */
std::string IOMock::nextUserInput(void)
{
    if(_userInputs.empty())
        return "";

    std::string input = _userInputs.front();
    _userInputs.pop();
    return input;
}

/**
 * @brief Clear the saved output text history
 * 
 */
void IOMock::clearOutput(void)
{
    _userOutput.clear();
}

/**
 * @brief Returns the output text
 * 
 * @return const std::string Text which is normally printed in the console
 */
const std::string IOMock::userOutput(void)
{
    return _userOutput.str();
}