#ifndef __IO_MOCK_HPP__
#define __IO_MOCK_HPP__

#include <queue>
#include <string>
#include <sstream>

class IOMock
{
    public:
        IOMock(void);
        ~IOMock(void);

        void addNextUserInput(const std::string & input);
        std::string nextUserInput(void);

        void clearOutput(void);
        const std::string userOutput(void);

    private:
        std::queue<std::string> _userInputs;

    protected:
        std::stringstream _userOutput;
};

#endif