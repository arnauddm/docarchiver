#include "actionsProviderFactoryMock.hpp"
#include "actionsProviderFactory.hpp"
#include "addActionMock.hpp"

/**
 * @brief Build an actions provider with all (or wanted) mocked actions
 * 
 * @return ActionsProvider * Actions provider pointer
 */
ActionsProvider * ActionsProviderFactoryMock::build(void)
{
    ActionsProvider * ap = ActionsProviderFactory::build();
    
    ap->add("add",      []() -> Action * { return new AddActionMock; });

    return ap;
}