#ifndef __ACTIONS_PROVIDER_FACTORY_MOCK_HPP__
#define __ACTIONS_PROVIDER_FACTORY_MOCK_HPP__

#include "actionsProvider.hpp"

class ActionsProviderFactoryMock
{
    public:
        static ActionsProvider * build(void);
};

#endif