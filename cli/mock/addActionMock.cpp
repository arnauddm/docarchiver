#include "addActionMock.hpp"
#include "textParser.hpp"
#include <iostream>

/**
 * @brief Construct a new Add Action Mock:: Add Action Mock object
 * 
 */
AddActionMock::AddActionMock(void)
{

}

/**
 * @brief Destroy the Add Action Mock:: Add Action Mock object
 * 
 */
AddActionMock::~AddActionMock(void)
{

}

/**
 * @brief Ask to the user if he want to add some additional keywords (mocked version)
 * 
 * @param file Pointer on the "current" archived file
 */
void AddActionMock::addAdditionalKeywords(ArchivedFile * file)
{
    std::string userResponse = "";
    do
    {
        _userOutput << "Do you want to add some additional keywords to this file ? [y/N]" << std::endl;
        userResponse = nextUserInput();
    } while(userResponse != "" && !userResponse.empty()
        && userResponse != "y" && userResponse != "Y"
        && userResponse != "n" && userResponse != "N");

    if(userResponse == "y" || userResponse == "Y")
    {
        _userOutput << "Please type all keywords you want to add (separated with space or ';' for example) : " << std::endl;
        std::string keywords;
        keywords = nextUserInput();

        TextParser parser;
        std::list<std::string> keywordsList = parser.parse(keywords);
        file->addPersonnalKeywords(keywordsList);
    }
}