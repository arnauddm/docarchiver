#ifndef __ADD_ACTION_MOCK_HPP__
#define __AD_ACTION_MOCK_HPP__

#include "addAction.hpp"
#include "IOMock.hpp"
#include "action.hpp"

class AddActionMock : public AddAction, public IOMock
{
    public:
        AddActionMock();
        ~AddActionMock();

    private:
        void addAdditionalKeywords(ArchivedFile * file);
};

#endif