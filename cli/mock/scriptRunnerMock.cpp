#include "scriptRunnerMock.hpp"
#include "actionsProviderFactoryMock.hpp"

/**
 * @brief Construct a new Script Runner Mock:: Script Runner Mock object
 * 
 * @param filepath 
 */
ScriptRunnerMock::ScriptRunnerMock(const std::string & filepath) : ScriptRunner(filepath)
{
    if(_actionsProvider)
    {
        delete _actionsProvider;
        _actionsProvider = ActionsProviderFactoryMock::build();
    }
    if(_cliParser)
    {
        delete _cliParser;
        _cliParser = new CliParser(_actionsProvider);
    }
}

/**
 * @brief Destroy the Script Runner Mock:: Script Runner Mock object
 * 
 */
ScriptRunnerMock::~ScriptRunnerMock(void)
{
    // Nothing to do...
}