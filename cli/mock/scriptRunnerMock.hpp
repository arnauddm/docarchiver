#ifndef __SCRIPT_RUNNER_MOCK_HPP
#define __SCRIPT_RUNNER_MOCK_HPP

#include "scriptRunner.hpp"

class ScriptRunnerMock : public ScriptRunner
{
    public:
        ScriptRunnerMock(const std::string & filepath);
        ~ScriptRunnerMock(void);
};

#endif