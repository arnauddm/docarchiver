#include "action.hpp"

std::string Action::WRONG_OPTION_STRING = "Wrong options...";

/**
 * @brief Construct a new Action:: Action object
 * 
 * @param name 
 */
Action::Action(std::string name) {
    _name = name;
}

/**
 * @brief Destroy the Action:: Action object
 * 
 */
Action::~Action(void) {
    
}

/**
 * @brief Get the name of the action
 * 
 * @return std::string 
 */
std::string Action::name(void) {
    return _name;
}

Action::Action(Action & action) : _name(action.name())
{
    for(unsigned int i = 0; i < action.optionsCount(); i++)
    {
        Action::Option * opt = action.option(i);
        addOption(opt->key(), opt->optionType(), opt->valueType());
    }
}

/**
 * @brief Create an option to the current @ref Action
 * 
 * @param key Option's key
 * @param type Option's type
 * @return Action::Option* Pointer on the created @ref Action::Option object
 */
Action::Option * Action::addOption(const std::string & key, Action::Option::OptionType optionType, Action::Option::ValueType valueType)
{
    Action::Option * option = new Action::Option(key, optionType, valueType);
    _options.insert({
        key,
        option
    });

    return option;
}

/**
 * @brief Check if all given options are good
 * 
 * @return true If all is good
 * @return false If there is an error
 */
bool Action::isCorrectOptions(void)
{
    bool atLeastOneOption = false;
    bool atLeastOneMandatory = false;
    bool onlyOptional = true;

    for(Options::iterator highIt = _options.begin();
        highIt != _options.end();
        highIt++)
    {
        Option * opt = highIt->second;
        // Not filled option
        if(opt->optionType() == Action::Option::OptionType::MANDATORY && !opt->isUsed())
            return false;
        if(opt->optionType() != Action::Option::OptionType::OPTIONAL && opt->isUsed() && opt->valueType() == Action::Option::ValueType::STRING && opt->value() == "")
            return false;

        // Too much values
        if(opt->valueType() == Action::Option::ValueType::STRING && opt->valuesCount() > 1)
            return false;

        if(opt->valueType() == Action::Option::ValueType::NONE && opt->value() != "")
            return false;

        if(opt->valueType() == Action::Option::ValueType::BOOLEAN && opt->isUsed() && (opt->value() != "true" && opt->value() != "false"))
            return false;

        for(unsigned int i = 0; i < opt->requiredFieldsCount() && opt->isUsed(); i++)
        {
            if(_options.find(opt->requiredField(i)) == _options.end())
                return false;

            if(!_options[opt->requiredField(i)]->isUsed())
                return false;
        }

        switch(opt->optionType())
        {
            case Action::Option::OptionType::MANDATORY:
                if(opt->isUsed())
                    atLeastOneMandatory = true;
                onlyOptional = false;
                break;
            case Action::Option::OptionType::OPTIONAL_OR_MANDATORY:
                if(opt->isUsed())
                    atLeastOneOption = true;
                onlyOptional = false;
                break;
            default:
                break;
        }
    }

    return onlyOptional || atLeastOneOption || atLeastOneMandatory;
}

/**
 * @brief Run the action
 * 
 * @return std::string Output string
 */
std::string Action::doAction(void)
{
    if(!isCorrectOptions())
        return WRONG_OPTION_STRING;

    return process();
}

/**
 * @brief Returns the number of options
 * 
 * @return unsigned int Number of options
 */
unsigned int Action::optionsCount(void)
{
    return _options.size();
}

/**
 * @brief Get the option
 * 
 * @param index Index of the option to get
 * @return Action::Option* If the option exists, returns a pointer on it and @ref nullptr otherwise
 */
Action::Option * Action::option(unsigned int index)
{
    if(index < 0 || index >= optionsCount())
        return nullptr;

    Options::iterator it = _options.begin();
    std::advance(it, index);
    return it->second;
}

/**
 * @brief Get the option
 * 
 * @param pattern Option's patternt to retrieve
 * @return Action::Option* If the option exists, returns a pointer on it and @ref nullptr otherwise
 */
Action::Option * Action::option(const std::string & pattern)
{
    if(_options.find(pattern) == _options.end())
        return nullptr;
    return _options.at(pattern);
}

/**
 * @brief Parse options from the command line
 * 
 * @param input Command line text entered by the user
 */
void Action::parseOptions(std::list<std::string> input) {
    if(input.size() <= 1)
        return;

    std::list<std::string>::iterator it = input.begin();
    std::advance(it, 1);

    for(; it != input.end(); it++) {
        std::string key = *it;

        // If no options like "-c" or "-file" or ...
        if(key.find("--") == std::string::npos)
        {
            Action::Option * opt = option("");
            if(!opt)
                continue;

            opt->addValue(key);
            opt->markAsUsed();
            continue;
        }

        it++;
        std::string value = "";
        if(it != input.end())
            value = *it;
        Action::Option * opt = option(key);
        if(opt)
        {
            opt->addValue(value);
            opt->markAsUsed();
        }
        
        // As it was incremented in the loop, we need to check if we are at the end of the list to avoid another incrementation before bound checking
        if(it == input.end())
            break;
    }
}