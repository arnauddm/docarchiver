#ifndef __ACTION_HPP__
#define __ACTION_HPP__

#include <string>
#include <map>
#include <vector>
#include <list>
#include <functional>

class Action {

    public:
        using ActionConstructor = Action* (*)();

        class Option {
            public:
                typedef enum {
                    MANDATORY = 0,                  // Must be filled
                    OPTIONAL,                       // Can be blank
                    OPTIONAL_OR_MANDATORY           // Optional if another option is filled, otherwise, this option became mandatory
                } OptionType;

                typedef enum {
                    STRING = 0,
                    STRING_LIST,
                    BOOLEAN,
                    NONE
                } ValueType;

                Option(const std::string & key, OptionType optionType, ValueType valueType);
                ~Option(void);

                Option * addRequiredField(const std::string & field);

                Option * addValue(const std::string & value);
                std::string value(void);
                int valuesCount(void);
                std::list<std::string> values(void);
                std::string key(void);
                OptionType optionType(void);
                ValueType valueType(void);
                unsigned int requiredFieldsCount(void);
                std::string requiredField(unsigned int index);

                Option * markAsUsed(void);
                bool isUsed(void);

            private:
                std::string _key;
                std::list<std::string> _values;
                OptionType _optionType;
                ValueType _valueType;
                std::vector<std::string> _requiredFields;
                bool _isUsed;

        };

    protected:
        typedef std::map<std::string, Action::Option *> Options;
    public:
        Action(std::string name);
        Action(Action & action);
        ~Action(void);

        std::string name(void);
        Option * addOption(const std::string & key, Option::OptionType optionType, Option::ValueType valueType);
        std::string doAction(void);
        virtual std::string description(void) = 0;
        bool isCorrectOptions(void);
        void parseOptions(std::list<std::string> input);

        unsigned int optionsCount(void);
        Action::Option * option(unsigned int index);
        Action::Option * option(const std::string & pattern);

        static std::string WRONG_OPTION_STRING;

    protected:
        virtual std::string process(void) = 0;

    private:
        std::string _name;

    protected:
        Options _options;
};

#endif