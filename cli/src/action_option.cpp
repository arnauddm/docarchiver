#include "action.hpp"
#include <stdarg.h>

/**
 * @brief Construct a new Action:: Option:: Option object
 * 
 * @param key 
 * @param optionType 
 * @param valueType 
 */
Action::Option::Option(const std::string & key, Action::Option::OptionType optionType, Action::Option::ValueType valueType) : 
    _key(key),
    _optionType(optionType),
    _valueType(valueType),
    _isUsed(false)
{
}

/**
 * @brief Destroy the Action:: Option:: Option object
 * 
 */
Action::Option::~Option(void)
{

}

/**
 * @brief Add a value to the option
 * 
 * @param value Value to add (or set)
 * @return Action::Option* Pointer on the current option
 */
Action::Option * Action::Option::addValue(const std::string & value)
{
    _values.push_back(value);
    return this;
}

/**
 * @brief Get value (only for single value option)
 * 
 * @return std::string Value or "" if there is no value or if the option requires a "string list" value
 */
std::string Action::Option::value(void)
{
    return valuesCount() >= 1 ? *(_values.begin()) : "";
}

/**
 * @brief Count the number of value for the option
 * 
 * @return int Number of value(s)
 */
int Action::Option::valuesCount(void)
{
    return _values.size();
}

/**
 * @brief Get all values
 * 
 * @return std::list<std::string> Values
 */
std::list<std::string> Action::Option::values(void)
{
    return _values;
}

/**
 * @brief Get the key of the option
 * 
 * @return std::string Option's key
 */
std::string Action::Option::key(void)
{
    return _key;
}

/**
 * @brief Get the option type
 * 
 * @return Action::Option::OptionType Option type
 */
Action::Option::OptionType Action::Option::optionType(void)
{
    return _optionType;
}

/**
 * @brief Get the value type
 * 
 * @return Action::Option::ValueType Value type
 */
Action::Option::ValueType Action::Option::valueType(void)
{
    return _valueType;
}

/**
 * @brief Returns the number of required fields for this options
 * 
 * @return unsigned int Number of required fields
 */
unsigned int Action::Option::requiredFieldsCount(void)
{
    return _requiredFields.size();
}

/**
 * @brief Returns the required field's key
 * 
 * @param index Index of the field
 * @return std::string Field's key
 */
std::string Action::Option::requiredField(unsigned int index)
{
    return index < requiredFieldsCount() ? _requiredFields.at(index) : "";
}

/**
 * @brief Add a new required field to the option
 * 
 * @param field Field's key
 * @return Action::Option* Pointer on this option
 */
Action::Option * Action::Option::addRequiredField(const std::string & field)
{
    _requiredFields.push_back(field);
    return this;
}

/**
 * @brief Mark this option as used to specify that a value has been set
 * 
 * @return Action::Option* Pointer on the opton
 */
Action::Option * Action::Option::markAsUsed(void)
{
    _isUsed = true;
    return this;
}

/**
 * @brief Returns the usage indicator
 * 
 * @return true If this option is used / set
 * @return false If this option isn't used / set
 */
bool Action::Option::isUsed(void)
{
    return _isUsed;
}