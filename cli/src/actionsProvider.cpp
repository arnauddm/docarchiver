#include "actionsProvider.hpp"

/**
 * @brief Construct a new Actions Provider:: Actions Provider object
 * 
 */
ActionsProvider::ActionsProvider(void)
{
}

/**
 * @brief Destroy the Actions Provider:: Actions Provider object
 * 
 */
ActionsProvider::~ActionsProvider(void)
{
}

/**
 * @brief Add an action to the provider
 * 
 * @param pattern Pattern used to "trigger" the action
 * @param constructor @ref std::function-based constructor on the "linked" action
 * @return ActionsProvider* Pointer on this @ref ActionsProvider
 */
ActionsProvider *ActionsProvider::add(const std::string & pattern, Action::ActionConstructor constructor)
{
    if (exist(pattern))
        _actions.erase(pattern);

    _actions.emplace(pattern, constructor);

    return this;
}

/**
 * @brief Get the action "triggered" by the pattern
 * 
 * @param pattern Action's pattern
 * @return Action* Pointer on the action if exists, @ref nullptr otherwise
 */
Action *ActionsProvider::get(std::string pattern)
{
    return _actions.count(pattern) != 0 ? _actions[pattern]() : nullptr;
}

/**
 * @brief Check if the pattern is supported
 * 
 * @param pattern Action's pattern
 * @return true If exists / supported
 * @return false Otherwise
 */
bool ActionsProvider::exist(std::string pattern)
{
    return _actions.find(pattern) != _actions.end();
}

/**
 * @brief Get all supported actions' names
 * 
 * @return std::list<std::string> All actions' names
 */
std::list<std::string> ActionsProvider::actionsName(void)
{
    std::list<std::string> names;
    for(std::map<std::string, Action::ActionConstructor>::iterator it = _actions.begin(); it != _actions.end(); it++)
        names.push_back(it->first);

    return names;
}