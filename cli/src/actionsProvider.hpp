#ifndef __ACTIONS_PROVIDER_HPP__
#define __ACTIONS_PROVIDER_HPP__

#include "action.hpp"
#include <map>
#include <string>
#include <functional>

class ActionsProvider {
    public:
        ActionsProvider(void);
        ~ActionsProvider(void);

        ActionsProvider * add(const std::string & pattern, Action::ActionConstructor fct);
        Action * get(std::string pattern);
        std::list<std::string> actionsName(void);

    private:
        std::map<std::string, Action::ActionConstructor> _actions;
        bool exist(std::string pattern);
};

#endif