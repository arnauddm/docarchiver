#include "actionsProviderFactory.hpp" 

#include "listAction.hpp"
#include "exitAction.hpp"
#include "helpAction.hpp"
#include "addAction.hpp"
#include "removeAction.hpp"
#include "findAction.hpp"
#include "saveAction.hpp"
#include "newAction.hpp"
 
/**
 * @brief Build as actions provider with all (or wanted) actions
 * 
 * @return ActionsProvider* Pointer on the @ref ActionsProvider
 */
ActionsProvider * ActionsProviderFactory::build(void)
{
    ActionsProvider * ap = new ActionsProvider();
    
    ap->add("list",     []() -> Action * { return new ListAction; });
    ap->add("exit",     []() -> Action * { return new ExitAction; });
    ap->add("add",      []() -> Action * { return new AddAction; });
    ap->add("remove",   []() -> Action * { return new RemoveAction; });
    ap->add("find",     []() -> Action * { return new FindAction; });
    ap->add("save",     []() -> Action * { return new SaveAction; });
    ap->add("help",     []() -> Action * { return new HelpAction; });
    ap->add("new",     []() -> Action * { return new NewAction; });

    return ap;
}