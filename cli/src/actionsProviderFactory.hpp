#ifndef __ACTIONS_PROVIDER_FACTORY_HPP__
#define __ACTIONS_PROVIDER_FACTORY_HPP__

#include "actionsProvider.hpp"

class ActionsProviderFactory
{
public:
    static ActionsProvider * build(void);
};

#endif