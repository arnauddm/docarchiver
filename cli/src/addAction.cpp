#include "addAction.hpp"
#include "jsonArchive.hpp"
#include "file.hpp"
#include "parser.hpp"
#include "archiveFactory.hpp"
#include "context.hpp"
#include "folderScanner.hpp"
#include "textParser.hpp"
#include "action.hpp"

#include <iostream>

/**
 * @brief Construct a new Add Action:: Add Action object
 * 
 */
AddAction::AddAction(void) : Action("addAction")
{
    addOption("--folder", Action::Option::OptionType::OPTIONAL_OR_MANDATORY, Action::Option::ValueType::STRING);
    addOption("--ext", Action::Option::OptionType::OPTIONAL, Action::Option::ValueType::STRING)->addRequiredField("--folder");
    addOption("", Action::Option::OptionType::OPTIONAL_OR_MANDATORY, Action::Option::ValueType::STRING);
}

/**
 * @brief Destroy the Add Action:: Add Action object
 * 
 */
AddAction::~AddAction(void)
{
}

/**
 * @brief Process the action
 * 
 * @return std::string Output string
 */
std::string AddAction::process(void)
{
    std::string result = "";

    Action::Options::iterator it;

    // Case of folder analysis
    
    if(_options["--folder"]->isUsed())
    {
        const std::string folderName = _options["--folder"]->value();
        const std::string folderPath = folderName[0] != '/' ? Context::getInstance()->launchedFrom() + "/" + folderName : folderName;
        
        FolderScanner scanner(folderPath);

        std::list<std::string> files = scanner.scan();
        for(std::list<std::string>::iterator itFile = files.begin(); itFile != files.end(); itFile++)
        {
            const std::string fileName = *itFile;

            // Check if this file matches with wanted extension(s)
            if(_options["--ext"]->isUsed())
            {
                std::string ext = _options["--ext"]->value();
                const std::string extensionsStr = ext;
                if(extensionsStr.find(fileName.substr(fileName.find_last_of(".") + 1)) == std::string::npos)
                    continue;
            }

            if(!result.empty())
                result += "\n";
            
            result += addFile(fileName);
        }

        return result;
    }

    // Case of "single file" = positionning the iterator to the first element (first option)
    return addFile(_options[""]->value());
    
}

/**
 * @brief Get action's description
 * 
 * @return std::string Output message
 */
std::string AddAction::description(void)
{
    return "Add a file to the archive";
}

/**
 * @brief Add file to the archive
 * 
 * @param filePath 
 * @return std::string Output message
 */
std::string AddAction::addFile(const std::string filePath)
{
    if (!File::exist(filePath))
        return "File (" + filePath + ") does not exist...";

    const std::string fullPath = filePath[0] != '/' ? Context::getInstance()->launchedFrom() + "/" + filePath : filePath;

    ArchivedFile archivedFile(fullPath);

    addAdditionalKeywords(&archivedFile);

    Archive * archive = ArchiveFactory::get();
    archive->addFile(&archivedFile);
    if(archive->save())
        return archivedFile.name() + " is successfully added !";

    return "Failed to add " + archivedFile.name();
}

/**
 * @brief Ask to the user if he want to add some additional keywords
 * 
 * @param file Pointer on the current file
 */
// LCOV_EXCL_START
void AddAction::addAdditionalKeywords(ArchivedFile * file)
{
    std::string userResponse = "";
    do
    {
        std::cout << "Do you want to add some additional keywords to this file ? [y/N]" << std::endl;
        std::cin >> userResponse;
        std::cout << userResponse << std::endl;
    } while(userResponse != "" && !userResponse.empty()
        && userResponse != "y" && userResponse != "Y"
        && userResponse != "n" && userResponse != "N");

    if(userResponse == "y" || userResponse == "Y")
    {
        std::cout << "Please type all keywords you want to add (separated with space or ';' for example) : " << std::endl;
        std::string keywords;
        std::cin >> keywords;

        TextParser parser;
        std::list<std::string> keywordsList = parser.parse(keywords);
        file->addKeywords(keywordsList);
    }
}
// LCOV_EXCL_STOP