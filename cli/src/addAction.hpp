#ifndef __ADD_ACTION_HPP__
#define __ADD_ACTION_HPP__

#include "action.hpp"
#include "archivedFile.hpp"

class AddAction : public Action {
    public:
        AddAction(void);
        ~AddAction(void);

        std::string process(void) override;
        std::string description(void) override;

    private:
        std::string addFile(const std::string filePath);

        virtual void addAdditionalKeywords(ArchivedFile * file);
};

#endif