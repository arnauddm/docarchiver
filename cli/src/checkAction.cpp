#include "checkAction.hpp"
#include "archiveChecker.hpp"
#include "archiveFactory.hpp"
#include "textFile.hpp"
#include "scriptParser.hpp"
#include "cliParser.hpp"
#include "actionsProviderFactory.hpp"
#include <sstream>

/**
 * @brief Construct a new Check Action:: Check Action object
 * 
 */
CheckAction::CheckAction(void) : Action("checkAction")
{
    addOption("--archive", Action::Option::OptionType::OPTIONAL_OR_MANDATORY, Action::Option::ValueType::NONE);
    addOption("--script", Action::Option::OptionType::OPTIONAL_OR_MANDATORY, Action::Option::ValueType::STRING);
}

/**
 * @brief Destroy the Check Action:: Check Action object
 * 
 */
CheckAction::~CheckAction(void)
{

}

/**
 * @brief Process the action
 * 
 * @return std::string Output string
 */
std::string CheckAction::process(void)
{
    std::stringstream ss;
    if(_options["--archive"]->isUsed())
        ss << checkArchive() << std::endl;

    if(_options["--script"]->isUsed())
        ss << checkScript(_options["--script"]->value()) << std::endl;

    return ss.str();
}

/**
 * @brief Get command's description
 * 
 * @return std::string Command description
 */
std::string CheckAction::description(void)
{
    std::stringstream ss;
    ss << "Check your environment." << std::endl;
    ss << "Options : " << std::endl;
    ss << "     * --archive : check your archive (if all files in the archive are still reachable)" << std::endl;

    return ss.str();
}

/**
 * @brief Check the archive
 * 
 * @return std::string Error output
 */
std::string CheckAction::checkArchive(void)
{
    std::stringstream ss;
    ArchiveChecker checker;
    checker.setArchive(ArchiveFactory::get());
    Checker::Errors errors = checker.check();
    
    if(errors.size() == 0)
    {
        ss << "No archive errors !";
    }
    else
    {
        ss << "Following files are not reachable :";
        for(Checker::Errors::iterator it = errors.begin(); it != errors.end(); it++)
            ss << "\n\t* " << *it;
    }

    return ss.str();
}

/**
 * @brief Check if the script contains errors
 * 
 * @param filepath 
 * @return std::string 
 */
std::string CheckAction::checkScript(const std::string & filepath)
{
    if(!TextFile::exist(filepath))
        return filepath + " not found";

    std::string content;

    {
        TextFile file(filepath);
        content = file.read();
    }

    CliParser cliParser(ActionsProviderFactory::build());
    std::list<std::string> errors;

    const std::list<std::string> lines = ScriptParser::parse(content);
    for(std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); it++ )
    {
        Action * action = cliParser.parse(*it);
        std::string result;
        if(!action)
            errors.push_back("Syntax not recognised for : " + *it);
        else if(!action->isCorrectOptions())
            errors.push_back(Action::WRONG_OPTION_STRING + " for : " + *it);
    }

    if(errors.size() == 0)
        return "No errors in this script !";

    std::stringstream ss;
    ss << errors.size() << " error(s) in this script :";
    for(std::list<std::string>::iterator it = errors.begin(); it != errors.end(); it++)
        ss << "\n\t* " << *it;

    return ss.str();
}