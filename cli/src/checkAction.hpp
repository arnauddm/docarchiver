#ifndef __CHECK_ACTION_HPP__
#define __CHECK_ACTION_HPP__

#include "action.hpp"

class CheckAction : public Action
{
public:
    CheckAction(void);
    ~CheckAction(void);

    std::string process(void) override;
    std::string description(void) override;

private:
    std::string checkArchive(void);
    std::string checkScript(const std::string & filepath);
};

#endif