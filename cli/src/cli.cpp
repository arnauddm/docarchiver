#include "cli.hpp"
#include <iostream>

#include "listAction.hpp"
#include "exitAction.hpp"
#include "helpAction.hpp"
#include "addAction.hpp"
#include "removeAction.hpp"
#include "findAction.hpp"
#include "actionsProviderFactory.hpp"

/**
 * @brief Construct a new Cli:: Cli object
 * 
 */
Cli::Cli(void) {
    _actionsProvider = ActionsProviderFactory::build();
    _cliParser = new CliParser(_actionsProvider);
}

/**
 * @brief Destroy the Cli:: Cli object
 * 
 */
Cli::~Cli(void) {
    if(_cliParser)
        delete _cliParser;

    if(_actionsProvider)
        delete _actionsProvider;
}

/**
 * @brief Run the CLI environment
 * 
 */
void Cli::run(void) {
    // Infite loop
    while(true) {
        std::string commandLine;

        std::cout << "> ";
        std::getline(std::cin, commandLine);

        Action * action = _cliParser->parse(commandLine);
        if(!action) {
            std::cout << "Syntax not recognised..." << std::endl;
            continue;
        }

        std::string result = action->doAction();
        std::cout << result << std::endl;

        if(dynamic_cast<ExitAction *>(action))
            break;

        if(dynamic_cast<HelpAction *>(action)) {
            std::list<std::string> actions = _actionsProvider->actionsName();
            for(std::list<std::string>::iterator it = actions.begin(); it != actions.end(); it++)
            {
                Action * act = _actionsProvider->get(*it);
                std::cout << "\n\t" << *it << "\t:\t" << act->description();
                delete act;
            }
            std::cout << std::endl;
        }
    }
}