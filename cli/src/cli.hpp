#ifndef __CLI_HPP__
#define __CLI_HPP__

#include "cliParser.hpp"
#include "actionsProvider.hpp"

class Cli {
    public:
        Cli(void);
        ~Cli(void);

        void run(void);

    private:
        CliParser * _cliParser;
        ActionsProvider * _actionsProvider;

};

#endif