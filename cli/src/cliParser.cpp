#include "cliParser.hpp"

/**
 * @brief Construct a new Cli Parser:: Cli Parser object
 * 
 * @param ActionsProvider ActionsProvider to use
 */
CliParser::CliParser(ActionsProvider * actionsProvider) {
    _actionsProvider = actionsProvider;
}

/**
 * @brief Destroy the Cli Parser:: Cli Parser object
 * 
 */
CliParser::~CliParser(void) {

}

/**
 * @brief Parse the command line
 * 
 * @param commandline 
 * @return Symbol* 
 */
Action * CliParser::parse(std::string commandline) {
    std::list<std::string> words = _textParser.parse(commandline, false);
    Action * action = _actionsProvider->get(*(words.begin()));
    
    if(!action)
        return nullptr;

    action->parseOptions(words);
    return action;
}