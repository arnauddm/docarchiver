#ifndef __CLI_PARSER_HPP__
#define __CLI_PARSER_HPP__

#include "textParser.hpp"
#include "actionsProvider.hpp"
#include <string>

class CliParser {
    public:
        CliParser(ActionsProvider * ActionsProvider);
        ~CliParser(void);

        Action * parse(std::string commandline);

    private:
        TextParser _textParser;
        ActionsProvider * _actionsProvider;
};

#endif