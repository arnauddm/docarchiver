#include "exitAction.hpp"

/**
 * @brief Construct a new Exit Action:: Exit Action object
 * 
 */
ExitAction::ExitAction(void) : Action("exitAction") {

}

/**
 * @brief Destroy the Exit Action:: Exit Action object
 * 
 */
ExitAction::~ExitAction(void) {

}

/**
 * @brief Process the action
 * 
 * @return std::string Output string
 */
std::string ExitAction::process(void) {
    return "Quiting...";
}

/**
 * @brief Get the action's description
 * 
 * @return std::string 
 */
std::string ExitAction::description(void) {
    return "Closing DocChiver";
}