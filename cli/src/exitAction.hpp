#ifndef __EXIT_ACTION_HPP__
#define __EXIT_ACTION_HPP__

#include "action.hpp"

class ExitAction : public Action {
    public:
        ExitAction(void);
        ~ExitAction(void);

        std::string process(void) override;
        std::string description(void) override;
};

#endif