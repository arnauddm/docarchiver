#include "findAction.hpp"
#include "archiveFactory.hpp"

/**
 * @brief Construct a new Find Action:: Find Action object
 * 
 */
FindAction::FindAction(void) : Action("findAction")
{
    addOption("", Action::Option::OptionType::MANDATORY, Action::Option::ValueType::STRING_LIST);
}

/**
 * @brief Destroy the Find Action:: Find Action object
 * 
 */
FindAction::~FindAction(void)
{
}

/**
 * @brief Process the action
 * 
 * @return std::string Output string
 */
std::string FindAction::process(void)
{
    Archive *archive = ArchiveFactory::get();

    std::string result = "";

    std::list<std::string> values = _options[""]->values();
    for(std::list<std::string>::iterator it = values.begin(); it != values.end(); it++)
    {
        const std::string keyword = *it;

        std::map<std::string, ArchivedFile *> files = archive->findByKeyword(keyword);

        if (files.size() == 0)
        {
            result = addLine(result, "No file contains '" + keyword + "'...");
            return result;
        }

        result = addLine(result, "'" + keyword + "' is found in :");
        for (
            std::map<std::string, ArchivedFile *>::iterator itFiles = files.begin();
            itFiles != files.end();
            itFiles++)
        {
            ArchivedFile * file = itFiles->second;
            result = addLine(result, "\t * " + file->name() + " [" + file->path() + "]");
        }
    }

    return result;
}

/**
 * @brief Get the action's description
 * 
 * @return std::string 
 */
std::string FindAction::description(void)
{
    return "Retuns a list of files that contain keyword";
}

/**
 * @brief Concatenate output result string
 * 
 * @param all : "current" content
 * @param line : line to add
 * @return std::string 
 */
std::string FindAction::addLine(const std::string & all, const std::string & line) {
    return all + (all == "" ? line : "\n" + line);
}