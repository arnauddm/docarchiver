#ifndef __FIND_ACTION_HPP__
#define __FIND_ACTION_HPP__

#include "action.hpp"
#include <string>

class FindAction : public Action {
    public:
        FindAction(void);
        ~FindAction(void);

        std::string process(void) override;
        std::string description(void) override;

    private:
        std::string addLine(const std::string & all, const std::string & line);
};

#endif