#include "helpAction.hpp"

/**
 * @brief Construct a new Help Action:: Help Action object
 * 
 */
HelpAction::HelpAction(void) : Action("helpAction") {

}

/**
 * @brief Destroy the Help Action:: Help Action object
 * 
 */
HelpAction::~HelpAction(void) {

}

/**
 * @brief Process the action
 * 
 * @return std::string Ouptut string
 */
std::string HelpAction::process(void) {
    return "This tool permits to archive file and research them by their keywords.";
}

/**
 * @brief Get the action's description
 * 
 * @return std::string 
 */
std::string HelpAction::description(void) {
    return "Display help content";
}