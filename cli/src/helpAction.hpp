#ifndef __HELP_ACTION_HPP__
#define __HELP_ACTION_HPP__

#include "action.hpp"

class HelpAction : public Action {
    public:
        HelpAction(void);
        ~HelpAction(void);

        std::string process(void) override;
        std::string description(void)override;
};

#endif