#include "listAction.hpp"
#include "jsonArchive.hpp"
#include "archiveFactory.hpp"

/**
 * @brief Construct a new List Action:: List Action object
 * 
 */
ListAction::ListAction(void) : Action("listAction")
{
}

/**
 * @brief Destroy the List Action:: List Action object
 * 
 */
ListAction::~ListAction(void)
{
}

/**
 * @brief Process the action
 * 
 * @return std::string Output string
 */
std::string ListAction::process(void)
{
    Archive * archive = ArchiveFactory::get();
    if (!archive->read())
    {
        delete archive;
        return "An error occured while reading archive file";
    }

    std::string resultStr = "Archived files :";
    std::map<std::string, ArchivedFile *> files = archive->allFiles();
    for (std::map<std::string, ArchivedFile *>::iterator it = files.begin(); it != files.end(); it++)
    {
        ArchivedFile * file = it->second;
        resultStr += "\n\t* " + file->name() + " [" + file->path() + "]";
    }

    delete archive;
    return resultStr;
}

/**
 * @brief Get the action's description
 * 
 * @return std::string 
 */
std::string ListAction::description(void)
{
    return "Listing all archived files";
}