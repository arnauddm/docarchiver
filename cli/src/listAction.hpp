#ifndef __LIST_ACTION_HPP__
#define __LIST_ACTION_HPP__

#include "action.hpp"

class ListAction : public Action {
    public:
        ListAction(void);
        ~ListAction(void);

        std::string process(void) override;
        std::string description(void) override;
};

#endif