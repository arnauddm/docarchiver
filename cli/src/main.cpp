#include "cli.hpp"
#include "scriptRunner.hpp"
#include "context.hpp"
#include "archiveFactory.hpp"
#include <iostream>
#include <string>

/**
 * @brief Main loop
 * 
 * @return int 
 */
int main(int argc, char * argv[]) {

    /**
     * Argument organisation :
     * 0 : executable name
     * 1 : archive filepath
     * 2 {optional} : --script
     * 3 {optional | if #2 is present} : script filepath
     */

    if(argc < 2 || std::string(argv[1]).find("--") != std::string::npos)
    {
        std::cout << "You must specify your input archive as fist parameter" << std::endl;
        return -1;
    }

    ArchiveFactory::setArchivePath(std::string(argv[1]));

    // Script passed in parameter
    if(argc == 4 && argv[2] == "--script")
    {
        ScriptRunner runner(argv[3]);
        std::cout << runner.run() << std::endl;
    }
    else
    {
        Cli cli;
        cli.run();
    }

    return 0;
}