#include "newAction.hpp"
#include "archiveFactory.hpp"

/**
 * @brief Construct a new New Action:: New Action object
 * 
 */
NewAction::NewAction(void) : Action("newAction")
{
    addOption("", Action::Option::OptionType::MANDATORY, Action::Option::ValueType::STRING);
}

/**
 * @brief Destroy the New Action:: New Action object
 * 
 */
NewAction::~NewAction(void)
{

}

/**
 * @brief Process the action
 * 
 * @return std::string Output string
 */
std::string NewAction::process(void)
{
    Archive * archive = ArchiveFactory::newOne(_options.begin()->second->value());
    const std::string output = "Archive (" + archive->archivePath() + ") created !";
    delete archive;
    
    return output;
}

/**
 * @brief Get the action's description
 * 
 * @return std::string 
 */
std::string NewAction::description(void)
{
    return "Create a new archive by specifying its name (with the extension).";
}