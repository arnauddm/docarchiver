#ifndef __NEW_ACTION_HPP__
#define __NEW_ACTION_HPP__

#include "action.hpp"

class NewAction : public Action
{
    public:
        NewAction(void);
        ~NewAction(void);

        std::string process(void) override;
        std::string description(void) override;
};

#endif