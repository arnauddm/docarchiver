#include "removeAction.hpp"
#include "archiveFactory.hpp"

/**
 * @brief Construct a new Remove Action:: Remove Action object
 * 
 */
RemoveAction::RemoveAction(void) : Action("removeAction")
{
    addOption("--name", Action::Option::OptionType::OPTIONAL_OR_MANDATORY, Action::Option::ValueType::STRING_LIST);
    addOption("--path", Action::Option::OptionType::OPTIONAL_OR_MANDATORY, Action::Option::ValueType::STRING_LIST);
}

/**
 * @brief Destroy the Remove Action:: Remove Action object
 * 
 */
RemoveAction::~RemoveAction(void)
{
}

/**
 * @brief Process the action
 * 
 * @return std::string Output string
 */
std::string RemoveAction::process(void)
{
    Archive *archive = ArchiveFactory::get();

    if(_options["--name"]->isUsed())
    {
        std::list<std::string> values = _options["--name"]->values();
        for(std::list<std::string>::iterator it = values.begin(); it != values.end(); it++)
        {
            if (!archive->removeFileWithName(*it))
                return "An error occured when trying to remove " + *it;
        }
    }

    if(_options["--path"]->isUsed())
    {
        std::list<std::string> values = _options["--path"]->values();
        for(std::list<std::string>::iterator it = values.begin(); it != values.end(); it++)
        {
            if (!archive->removeFileWithPath(*it))
                return "An error occured when trying to remove " + *it;
        }
    }

    return "Well removed";
}

/**
 * @brief Get action's description
 * 
 * @return std::string 
 */
std::string RemoveAction::description(void)
{
    return "Remove a file from the archive";
}