#ifndef __REMOVE_ACTION_HPP__
#define __REMOVE_ACTION_HPP__

#include "action.hpp"

class RemoveAction : public Action {
    public:
        RemoveAction(void);
        ~RemoveAction(void);

        std::string process(void) override;
        std::string description(void) override;
};

#endif