#include "saveAction.hpp"
#include "archiveFactory.hpp"
#include <sstream>

/**
 * @brief Construct a new Save Action:: Save Action object
 * 
 */
SaveAction::SaveAction(void) : Action("saveAction")
{
    addOption("--key", Action::Option::OptionType::OPTIONAL, Action::Option::ValueType::STRING);
    addOption("--encryption", Action::Option::OptionType::OPTIONAL, Action::Option::ValueType::BOOLEAN)->addRequiredField("--key");
    addOption("--name", Action::Option::OptionType::OPTIONAL, Action::Option::ValueType::STRING);
}

/**
 * @brief Destroy the Save Action:: Save Action object
 * 
 */
SaveAction::~SaveAction(void)
{

}

/**
 * @brief Process the action
 * 
 * @return std::string Output string
 */
std::string SaveAction::process(void)
{
    bool encryption = _options["--encryption"]->isUsed() && _options["--encryption"]->value() == "true";
    
    std::string name = ArchiveFactory::archivePath();
    if(_options["--name"]->isUsed())
        name.replace(0, name.find_first_of('.'), _options["--name"]->value());

    bool result;

    Archive * archive = ArchiveFactory::get();
    archive->setArchivePath(name);
    if(encryption)
        result = archive->saveWithEncryption(_options["--key"]->value());
    else
        result = archive->save();

    if(!result)
        return "An error occured during this operation";

    return "Archive well saved !";
}

/**
 * @brief Get action description
 * 
 * @return std::string Description
 */
std::string SaveAction::description(void)
{
    std::stringstream ss;
    ss << "Save manually the archive (this command does not disable the automatic save)." << std::endl;
    ss << "     --encryption=<true|false>" << std::endl;
    ss << "     --name=<...> | Only the name without the extension !"; 

    return ss.str();
}