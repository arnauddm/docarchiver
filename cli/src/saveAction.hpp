#ifndef __SAVE_ACTION_HPP__
#define __SAVE_ACTION_HPP__

#include "action.hpp"

class SaveAction : public Action
{
    public:
        SaveAction(void);
        ~SaveAction(void);

        std::string process(void) override;
        std::string description(void) override;
};

#endif // __SAVE_ACTION_HPP__