#include "scriptParser.hpp"

/**
 * @brief Parse the script to be executed line by line by the @ref ScriptRunner
 * 
 * @param content 
 * @return std::list<std::string> 
 */
std::list<std::string> ScriptParser::parse(std::string content)
{
    std::list<std::string> lines;

    int posStart = 0, posEnd = 0;

    // Add "\n" at the end (if not present) to break the while true loop after the last "command"
    if(content.find_last_of("\n") != content.length())
        content += "\n";

    while(true)
    {
        std::string line;
        posEnd = content.find("\n", posStart);
        if(posEnd == std::string::npos)
            break;

        line = content.substr(posStart, posEnd - posStart);
        lines.push_back(line);
        posStart = posEnd + 1;
    }

    return lines;
}