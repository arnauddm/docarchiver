#ifndef __SCRIPT_PARSER_HPP__
#define __SCRIPT_PARSER_HPP__

#include <list>
#include <string>

class ScriptParser
{
public:
    static std::list<std::string> parse(std::string content);
};

#endif