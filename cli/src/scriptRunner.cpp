#include "scriptRunner.hpp"
#include "actionsProviderFactory.hpp"
#include "textFile.hpp"
#include "context.hpp"
#include "scriptParser.hpp"
#include <iostream>

/**
 * @brief Construct a new Script Runner:: Script Runner object
 * 
 * @param filepath Filepath of the script
 */
ScriptRunner::ScriptRunner(const std::string & filepath)
{
    const std::string cwd = Context::getInstance()->launchedFrom();

    _scriptPath = cwd.at(0) == '/' ? filepath : cwd + "/" + filepath;

    _actionsProvider = ActionsProviderFactory::build();
    _cliParser = new CliParser(_actionsProvider);
}

/**
 * @brief Destroy the Script Runner:: Script Runner object
 * 
 */
ScriptRunner::~ScriptRunner(void)
{
    if(_cliParser)          delete _cliParser;
    if(_actionsProvider)    delete _actionsProvider;
}

/**
 * @brief Run the given script
 * 
 * @return std::string Output text of all commands
 */
std::string ScriptRunner::run(void)
{
    if(!TextFile::exist(_scriptPath))
        return _scriptPath + " not found";

    std::string content;

    {
        TextFile file(_scriptPath);
        content = file.read();
    }

    std::string output = "";

    const std::list<std::string> lines = ScriptParser::parse(content);
    for(std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); it++ )
    {
        Action * action = _cliParser->parse(*it);
        std::string result;
        if(!action)
            result = "Syntax not recognised for : " + *it;
        else
            result  = action->doAction();
        
        if(!output.empty())
            output += "\n";
        output += result;
    }
    return output;
}