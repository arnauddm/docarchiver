#ifndef __SCRIPT_RUNNER_HPP__
#define __SCRIPT_RUNNER_HPP__

#include <string>
#include "cliParser.hpp"
#include "actionsProvider.hpp"

class ScriptRunner 
{
public:
    ScriptRunner(const std::string & filepath);
    ~ScriptRunner(void);

    std::string run(void);

private:
    std::string _scriptPath;

protected:
    ActionsProvider * _actionsProvider;
    CliParser * _cliParser;
};

#endif