#include "test.hpp"
#include "action.hpp"
#include <list>
#include <string>


class ActionTest : public Action
{
    public:
        ActionTest(std::string name) : Action(name) {}
        ~ActionTest(void) {}

        std::string process(void) { return ""; }
        std::string description(void) { return ""; }
};

TEST(Action, ConstructorDestructor)
{
    ActionTest * action = new ActionTest("testAction");
    EXPECT_EQ(action->name(), "testAction");
    delete action;
}

TEST(Action, EmptyMandatoryOption)
{
    ActionTest * action = new ActionTest("testAction");
    action->addOption("--opt1", ActionTest::Option::OptionType::MANDATORY, ActionTest::Option::ValueType::NONE)->markAsUsed();

    // As value is filled when parsing the CLI, at this point, we can consider that the parser hasn't found this entry
    EXPECT_EQ(action->isCorrectOptions(), true);
    EXPECT_EQ(action->optionsCount(), 1);

    delete action;
}

TEST(Action, FilledMandatoryOption)
{
    ActionTest * action = new ActionTest("testAction");
    action->addOption("--opt1", ActionTest::Option::OptionType::MANDATORY, ActionTest::Option::ValueType::STRING)->addValue("test")->markAsUsed();

    // As value is filled when parsing the CLI, at this point, we can consider that the parser hasn't found this entry
    EXPECT_EQ(action->isCorrectOptions(), true);
    EXPECT_EQ(action->optionsCount(), 1);

    delete action;
}

TEST(Action, MandatoryButNotused)
{
    ActionTest * action = new ActionTest("testAction");
    action->addOption("--opt1", ActionTest::Option::OptionType::MANDATORY, ActionTest::Option::ValueType::NONE);

    // As value is filled when parsing the CLI, at this point, we can consider that the parser hasn't found this entry
    EXPECT_EQ(action->isCorrectOptions(), false);
    EXPECT_EQ(action->optionsCount(), 1);

    delete action;
}

TEST(Action, EmptyRequiredOption)
{
    ActionTest * action = new ActionTest("testAction");
    action->addOption("--opt1", ActionTest::Option::OptionType::OPTIONAL, ActionTest::Option::ValueType::NONE);
    action->addOption("--opt2", ActionTest::Option::OptionType::OPTIONAL, ActionTest::Option::ValueType::NONE)->addRequiredField("--opt1")->markAsUsed();

    // No error, opt2 requires opt1 and opt1 exists
    EXPECT_EQ(action->isCorrectOptions(), false);
    EXPECT_EQ(action->optionsCount(), 2);

    delete action;
}

TEST(Action, FilledRequiredOption)
{
    ActionTest * action = new ActionTest("testAction");
    action->addOption("--opt1", ActionTest::Option::OptionType::OPTIONAL, ActionTest::Option::ValueType::NONE)->markAsUsed();
    action->addOption("--opt2", ActionTest::Option::OptionType::OPTIONAL, ActionTest::Option::ValueType::NONE)->addRequiredField("--opt1");

    // No error, opt2 requires opt1 and opt1 exists
    EXPECT_EQ(action->isCorrectOptions(), true);
    EXPECT_EQ(action->optionsCount(), 2);

    delete action;
}


TEST(Action, NonExistentRequiredOption)
{
    ActionTest * action = new ActionTest("testAction");
    action->addOption("--opt2", ActionTest::Option::OptionType::OPTIONAL, ActionTest::Option::ValueType::NONE)->addRequiredField("--opt1")->markAsUsed();

    // No error, opt2 requires opt1 and opt1 exists
    EXPECT_EQ(action->isCorrectOptions(), false);
    EXPECT_EQ(action->optionsCount(), 1);

    delete action;
}

TEST(Action, TypeStringList)
{
    ActionTest * action = new ActionTest("testAction");
    action->addOption("--opt2", ActionTest::Option::OptionType::OPTIONAL, ActionTest::Option::ValueType::STRING_LIST)->addValue("hello")->addValue("world");

    // No error, opt2 requires opt1 and opt1 exists
    EXPECT_EQ(action->isCorrectOptions(), true);
    EXPECT_EQ(action->optionsCount(), 1);

    delete action;
}

TEST(Action, TypeStringWithList)
{
    ActionTest * action = new ActionTest("testAction");
    action->addOption("--opt2", ActionTest::Option::OptionType::OPTIONAL, ActionTest::Option::ValueType::STRING)->addValue("hello")->addValue("world");

    // No error, opt2 requires opt1 and opt1 exists
    EXPECT_EQ(action->isCorrectOptions(), false);
    EXPECT_EQ(action->optionsCount(), 1);

    delete action;
}

TEST(Action, GetOptionWithInBoundIndex)
{
    ActionTest action("testAction");
    action.addOption("--opt", ActionTest::Option::OptionType::OPTIONAL, ActionTest::Option::ValueType::NONE)->markAsUsed();
    EXPECT_NE(action.option(0), nullptr);
}

TEST(Action, GetOptionWithOutOfBoundIndex)
{
    ActionTest action("testAction");
    EXPECT_EQ(action.option(12), nullptr);
}

TEST(Action, FillDefaultValueWhichDoesNotExit)
{
    ActionTest action("actionTest");

    std::list<std::string> input;
    input.push_back("actionTest");
    input.push_back("test");

    action.parseOptions(input);
}

TEST(Action, CopyConstructor)
{
    ActionTest action("actionTest");
    action.addOption("--opt1", ActionTest::Option::OptionType::OPTIONAL, ActionTest::Option::ValueType::STRING);
    action.addOption("--opt2", ActionTest::Option::OptionType::OPTIONAL, ActionTest::Option::ValueType::STRING);
    ActionTest bis(action);

    EXPECT_EQ(action.name(), bis.name());
    EXPECT_EQ(action.optionsCount(), bis.optionsCount());
}