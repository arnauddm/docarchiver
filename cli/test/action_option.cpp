#include "test.hpp"
#include "action.hpp"

TEST(Action_Option, ConstructorDestructor) {
    Action::Option * option = new Action::Option("key", Action::Option::OptionType::OPTIONAL, Action::Option::ValueType::NONE);
    EXPECT_EQ(option->key(), "key");
    EXPECT_EQ(option->optionType(), Action::Option::OptionType::OPTIONAL);
    EXPECT_EQ(option->valueType(), Action::Option::ValueType::NONE);
    EXPECT_EQ(option->requiredFieldsCount(), 0);
    EXPECT_EQ(option->requiredField(11), "");
    delete option;
}

TEST(Action_Option, Value) {
    Action::Option * option = new Action::Option("key", Action::Option::OptionType::OPTIONAL, Action::Option::ValueType::STRING);
    option->addValue("valuevalue");
    EXPECT_EQ(option->key(), "key");
    EXPECT_EQ(option->optionType(), Action::Option::OptionType::OPTIONAL);
    EXPECT_EQ(option->valueType(), Action::Option::ValueType::STRING);
    EXPECT_EQ(option->requiredFieldsCount(), 0);
    EXPECT_EQ(option->requiredField(11), "");
    EXPECT_EQ(option->value(), "valuevalue");
    delete option;
}


TEST(Action_Option, AddRequiredField) {
    Action::Option * option = new Action::Option("key", Action::Option::OptionType::MANDATORY, Action::Option::ValueType::NONE);
    option->addRequiredField("--f1")->addRequiredField("--f2")->addRequiredField("--f3");
    EXPECT_EQ(option->key(), "key");
    EXPECT_EQ(option->optionType(), Action::Option::OptionType::MANDATORY);
    EXPECT_EQ(option->valueType(), Action::Option::ValueType::NONE);
    EXPECT_EQ(option->requiredFieldsCount(), 3);
    EXPECT_EQ(option->requiredField(0), "--f1");
    EXPECT_EQ(option->requiredField(1), "--f2");
    EXPECT_EQ(option->requiredField(2), "--f3");

    delete option;
}