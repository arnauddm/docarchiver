#include "actionsProvider.hpp"
#include "saveAction.hpp"
#include "newAction.hpp"
#include "test.hpp"


TEST(ActionsProvider, ExistingSymbol) {
    ActionsProvider provider;
    
    EXPECT_EQ(provider.add("save",      []() -> Action * { return new SaveAction; }), &provider);
    EXPECT_EQ(provider.get("save")->name(), "saveAction");
}

TEST(ActionsProvider, UnknownSymbol) {
    ActionsProvider provider;
    
    EXPECT_EQ(provider.add("save",      []() -> Action * { return new SaveAction; }), &provider);
    EXPECT_EQ(provider.get("!snjbfsjbf"), nullptr);
}

TEST(ActionsProvider, SymbolDuplication) {
    ActionsProvider provider;
    
    EXPECT_EQ(provider.add("save",      []() -> Action * { return new SaveAction; }), &provider);
    EXPECT_EQ(provider.add("save",      []() -> Action * { return new NewAction; }), &provider);
    
    EXPECT_EQ(provider.get("save")->name(), "newAction");
}

TEST(ActionsProvider, GetSymbols) {
    ActionsProvider provider;
    
    EXPECT_EQ(provider.add("save",      []() -> Action * { return new SaveAction; }), &provider);
    EXPECT_EQ(provider.add("new",      []() -> Action * { return new NewAction; }), &provider);

    EXPECT_EQ(provider.actionsName().size(), 2);
}

/*TEST(ActionsProvider, ExistAndNotExist) {
    ActionsProvider provider;
    
    EXPECT_EQ(provider.add("save",      []() -> Action * { return new SaveAction; }), &provider);
    EXPECT_EQ(provider.add("new",      []() -> Action * { return new NewAction; }), &provider);

    EXPECT_EQ(provider.exist("save"), true);
    EXPECT_EQ(provider.exist("blabla"), false);
}*/