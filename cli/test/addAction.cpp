#include "addActionMock.hpp"
#include "test.hpp"
#include "file.hpp"
#include "cliParser.hpp"
#include "actionsProvider.hpp"
#include "listAction.hpp"
#include "findAction.hpp"
#include "context.hpp"

class FileTest : public File
{
public:
    FileTest(std::string filename) : File(filename)
    {
    }

    ~FileTest(void)
    {
    }

    std::list<std::string> parse(void)
    {
        return std::list<std::string>();
    }
};

TEST(AddAction, Description) {
    AddAction action;

    EXPECT_EQ(action.description(), "Add a file to the archive");
}

TEST(AddAction, AddTextFile) {
    FileTest file("testFile.txt");
    file.write("key1");

    ActionsProvider * ap = new ActionsProvider();
    ap->add("add",      []() -> Action * { return new AddActionMock; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("add testFile.txt");

    AddActionMock * actionMock = dynamic_cast<AddActionMock *>(action);

    EXPECT_NE(actionMock, nullptr);

    // Do not add additional keywords during "add" process
    actionMock->addNextUserInput("n");
    EXPECT_EQ(actionMock->doAction(), "testFile.txt is successfully added !");

    ListAction listAction;
    const std::string expectedResult  = "Archived files :\n\t* testFile.txt [" + Context::getInstance()->launchedFrom() + "/testFile.txt]";
    const std::string result = listAction.doAction();

    EXPECT_EQ(result, expectedResult);
}

TEST(AddAction, NewPDFFile) {
    ActionsProvider * ap = new ActionsProvider();
    ap->add("add",      []() -> Action * { return new AddActionMock; });
    ap->add("find",      []() -> Action * { return new FindAction(); });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("add ../cli/test/files/pdfFile.pdf");

    AddActionMock * actionMock = dynamic_cast<AddActionMock *>(action);
    EXPECT_NE(actionMock, nullptr);

    // Do not add additional keywords during "add" process
    actionMock->addNextUserInput("n");
    EXPECT_EQ(actionMock->doAction(), "pdfFile.pdf is successfully added !");

    ListAction listAction;
    {
        const std::string expectedResult = "Archived files :\n\t* pdfFile.pdf [" + Context::getInstance()->launchedFrom() + "/../cli/test/files/pdfFile.pdf]";
        const std::string result = listAction.doAction();

        EXPECT_EQ(result, expectedResult);
    }

    {
        action = cliParser.parse("find Lorem");
        FindAction * findAction = dynamic_cast<FindAction *>(action);
        EXPECT_NE(findAction, nullptr);

        const std::string expectedStr = "'Lorem' is found in :\n\t * pdfFile.pdf [" + Context::getInstance()->launchedFrom() + "/../cli/test/files/pdfFile.pdf]";
        EXPECT_EQ(findAction->doAction(), expectedStr);
    }
}

TEST(AddAction, DoActionWithoutOption) {
    ActionsProvider * ap = new ActionsProvider();
    ap->add("add",      []() -> Action * { return new AddActionMock; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("add");

    AddActionMock * mockAction = dynamic_cast<AddActionMock *>(action);
    EXPECT_NE(mockAction, nullptr);

    EXPECT_EQ(mockAction->doAction(), Action::WRONG_OPTION_STRING);
}

TEST(AddAction, ScanFolder) {
    ActionsProvider * ap = new ActionsProvider();
    ap->add("add",      []() -> Action * { return new AddActionMock; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("add --folder ../cli/test/files");

    AddActionMock * mockAction = dynamic_cast<AddActionMock *>(action);
    EXPECT_NE(mockAction, nullptr);

    // Do not add additional keywords during "add" process
    mockAction->addNextUserInput("n");
    EXPECT_EQ(mockAction->doAction(), "pdfFile.pdf is successfully added !");
}

TEST(AddAction, ScanFolderPdfExtension) {
    ActionsProvider * ap = new ActionsProvider();
    ap->add("add",      []() -> Action * { return new AddActionMock; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("add --folder ../cli/test/files --ext pdf");

    AddActionMock * mockAction = dynamic_cast<AddActionMock *>(action);
    EXPECT_NE(mockAction, nullptr);

    // Do not add additional keywords during "add" process
    mockAction->addNextUserInput("n");
    EXPECT_EQ(mockAction->doAction(), "pdfFile.pdf is successfully added !");
}

TEST(AddAction, ScanFolderTxtExtension) {
    ActionsProvider * ap = new ActionsProvider();
    ap->add("add",      []() -> Action * { return new AddActionMock; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("add --folder ../cli/test/files --ext txt");

    AddActionMock * mockAction = dynamic_cast<AddActionMock *>(action);
    EXPECT_NE(mockAction, nullptr);

    // Do not add additional keywords during "add" process
    mockAction->addNextUserInput("n");
    EXPECT_EQ(mockAction->doAction(), "");
}

TEST(AddAction, NewTextFileWithAdditionalKeywords) {
    const std::string filename = "testFile.txt";
    const std::string filepath = Context::getInstance()->launchedFrom() + "/" + filename;

    FileTest file(filename);
    file.write("key1");

    ActionsProvider * ap = new ActionsProvider();
    ap->add("add",          []() -> Action * { return new AddActionMock; });
    ap->add("find",         []() -> Action * { return new FindAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("add " + filename);

    AddActionMock * mockAction = dynamic_cast<AddActionMock *>(action);
    EXPECT_NE(mockAction, nullptr);

    // Do not add additional keywords during "add" process
    mockAction->addNextUserInput("y");
    mockAction->addNextUserInput("additional keyword");
    EXPECT_EQ(mockAction->doAction(), filename + " is successfully added !");

    {
        ListAction listAction;
        const std::string expectedResult  = "Archived files :\n\t* " + filename + "[" + filepath + "]";
        const std::string result = listAction.doAction();
    }

    {
        Action * action = cliParser.parse("find additional keyword");
        FindAction * findAction = dynamic_cast<FindAction *>(action);
        EXPECT_NE(findAction, nullptr);
        const std::string expectedResult = "'additional' is found in :\n\t * " + filename + " [" + filepath + "]\n'keyword' is found in :\n\t * " + filename + " [" + filepath + "]";

        const std::string output = findAction->doAction();
        EXPECT_EQ(action->doAction(), expectedResult);
    }
}

TEST(AddAction, CheckDisplayedText) {
    ActionsProvider * ap = new ActionsProvider();
    ap->add("add",      []() -> Action * { return new AddActionMock; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("add ../cli/test/files/pdfFile.pdf");

    AddActionMock * mockAction = dynamic_cast<AddActionMock *>(action);
    EXPECT_NE(mockAction, nullptr);

    // Do not add additional keywords during "add" process
    mockAction->addNextUserInput("n");
    mockAction->doAction();
    
    const std::string expectedOutput = "Do you want to add some additional keywords to this file ? [y/N]\n";
    EXPECT_EQ(expectedOutput, mockAction->userOutput());
}

TEST(AddAction, CheckDisplayedTextWithWrongAnwser) {
    ActionsProvider * ap = new ActionsProvider();
    ap->add("add",      []() -> Action * { return new AddActionMock; });

    CliParser cliParser(ap);

    Action * act = cliParser.parse("add ../cli/test/files/pdfFile.pdf");
    AddActionMock * action = dynamic_cast<AddActionMock *>(act);
    
    EXPECT_NE(action, nullptr);

    const std::string simpleOutput = "Do you want to add some additional keywords to this file ? [y/N]";

    std::stringstream ss;
    ss << simpleOutput << std::endl;
    for(int i = 0; i < 10; i++)
    {
        action->addNextUserInput("b");
        ss << simpleOutput << std::endl;
    }
    action->addNextUserInput("n");

    action->doAction();
    EXPECT_EQ(ss.str(), action->userOutput());
}

TEST(AddAction, CheckDisplayedTextWithResponseYes) {
    ActionsProvider * ap = new ActionsProvider();
    ap->add("add",      []() -> Action * { return new AddActionMock; });

    CliParser cliParser(ap);

    Action * act = cliParser.parse("add ../cli/test/files/pdfFile.pdf");
    AddActionMock * action = dynamic_cast<AddActionMock *>(act);
    
    EXPECT_NE(action, nullptr);

    std::stringstream ss;

    ss << "Do you want to add some additional keywords to this file ? [y/N]" << std::endl;
    // Say "yes"
    action->addNextUserInput("y");

    ss << "Please type all keywords you want to add (separated with space or ';' for example) : " << std::endl;
    // Put some keywords
    action->addNextUserInput("hello world");

    action->doAction();
    EXPECT_EQ(ss.str(), action->userOutput());
}

TEST(AddAction, AddNonExistentFile) {
    ActionsProvider * ap = new ActionsProvider();
    ap->add("add",      []() -> Action * { return new AddActionMock; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("add helloWorld.txt");

    AddActionMock * mockAction = dynamic_cast<AddActionMock *>(action);
    EXPECT_NE(mockAction, nullptr);

    // Do not add additional keywords during "add" process
    mockAction->addNextUserInput("n");
    EXPECT_EQ(mockAction->doAction(), "File (helloWorld.txt) does not exist...");
}