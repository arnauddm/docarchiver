#include "test.hpp"
#include "checkAction.hpp"
#include "cliParser.hpp"
#include "archiveFactory.hpp"
#include "file.hpp"

class FileTest : public File
{
public:
    FileTest(void) : File() {}
    ~FileTest(void) {}

    std::list<std::string> parse(void) { return std::list<std::string>(); }
};

TEST(CheckAction, Name)
{
    CheckAction action;

    EXPECT_EQ(action.name(), "checkAction");
}

TEST(CheckAction, Description)
{
    CheckAction action;
    
    const std::string description = "Check your environment.\nOptions : \n     * --archive : check your archive (if all files in the archive are still reachable)\n";

    EXPECT_EQ(action.description(), description);
}

TEST(CheckAction, NoOptions)
{
    ActionsProvider * ap = new ActionsProvider();
    ap->add("check", []() -> Action * { return new CheckAction; });
    CliParser cliParser(ap);

    Action * action = cliParser.parse("check");
    EXPECT_NE(dynamic_cast<CheckAction *>(action), nullptr);

    const std::string expectedResult = Action::WRONG_OPTION_STRING;
    EXPECT_EQ(action->doAction(), expectedResult);
}

TEST(CheckAction, ArchiveTooManyOptions)
{
    ActionsProvider * ap = new ActionsProvider();
    ap->add("check", []() -> Action * { return new CheckAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("check --archive test");
    EXPECT_NE(dynamic_cast<CheckAction *>(action), nullptr);

    const std::string expectedResult = Action::WRONG_OPTION_STRING;
    EXPECT_EQ(action->doAction(), expectedResult);
}

TEST(CheckAction, ArchiveNoErrors)
{
    ActionsProvider * ap = new ActionsProvider();
    ap->add("check", []() -> Action * { return new CheckAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("check --archive");
    EXPECT_NE(dynamic_cast<CheckAction *>(action), nullptr);

    const std::string expectedResult = "No archive errors !\n";
    EXPECT_EQ(action->doAction(), expectedResult);
}

TEST(CheckAction, ArchiveErrors)
{
    Archive * archive = ArchiveFactory::newOne("archive.json");

    ArchivedFile * archivedFile = new ArchivedFile();
    archivedFile->setPath("/blabla/test.txt");

    archive->addFile(archivedFile);
    archive->save();

    ActionsProvider * ap = new ActionsProvider();
    ap->add("check", []() -> Action * { return new CheckAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("check --archive");
    EXPECT_NE(dynamic_cast<CheckAction *>(action), nullptr);

    const std::string expectedResult = "Following files are not reachable :\n\t* /blabla/test.txt\n";
    EXPECT_EQ(action->doAction(), expectedResult);

    delete archive;
}

TEST(CheckAction, ScriptEmptyOptions)
{
    ActionsProvider * ap = new ActionsProvider();
    ap->add("check", []() -> Action * { return new CheckAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("check --script");
    EXPECT_NE(dynamic_cast<CheckAction *>(action), nullptr);

    const std::string expectedResult = Action::WRONG_OPTION_STRING;
    EXPECT_EQ(action->doAction(), expectedResult);
}

TEST(CheckAction, ScriptWrongOptions)
{
    ActionsProvider * ap = new ActionsProvider();
    ap->add("check", []() -> Action * { return new CheckAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("check --script test.txt");
    EXPECT_NE(dynamic_cast<CheckAction *>(action), nullptr);

    const std::string expectedResult = "test.txt not found\n";
    EXPECT_EQ(action->doAction(), expectedResult);
}

TEST(CheckAction, ScriptNoErrors)
{
    {
        FileTest file;
        file.setFilename("scriptTest.txt");
        file.write("list");
    }

    ActionsProvider * ap = new ActionsProvider();
    ap->add("check", []() -> Action * { return new CheckAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("check --script scriptTest.txt");
    EXPECT_NE(dynamic_cast<CheckAction *>(action), nullptr);

    const std::string expectedResult = "No errors in this script !\n";
    EXPECT_EQ(action->doAction(), expectedResult);

    File::remove("scriptTest.txt");
}

TEST(CheckAction, ScriptErrors)
{
    {
        FileTest file;
        file.setFilename("scriptTest.txt");
        file.write("wrong");
    }

    ActionsProvider * ap = new ActionsProvider();
    ap->add("check", []() -> Action * { return new CheckAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("check --script scriptTest.txt");
    EXPECT_NE(dynamic_cast<CheckAction *>(action), nullptr);

    const std::string expectedResult = "1 error(s) in this script :\n\t* Syntax not recognised for : wrong\n";
    EXPECT_EQ(action->doAction(), expectedResult);

    File::remove("scriptTest.txt");
}

TEST(CheckAction, ScriptErrorsOptions)
{
    {
        FileTest file;
        file.setFilename("scriptTest.txt");
        file.write("add");
    }

    ActionsProvider * ap = new ActionsProvider();
    ap->add("check", []() -> Action * { return new CheckAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("check --script scriptTest.txt");
    EXPECT_NE(dynamic_cast<CheckAction *>(action), nullptr);

    const std::string expectedResult = "1 error(s) in this script :\n\t* " + Action::WRONG_OPTION_STRING + " for : add\n";
    EXPECT_EQ(action->doAction(), expectedResult);

    File::remove("scriptTest.txt");
}