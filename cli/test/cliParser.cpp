#include "cliParser.hpp"
#include "test.hpp"
#include "listAction.hpp"
#include "file.hpp"


TEST(CliParser, ParseCommandLine) {
    ActionsProvider provider;
    provider.add("list",      []() -> Action * { return new ListAction; });

    CliParser cliParser(&provider);

    Action * act = cliParser.parse("list");
    EXPECT_NE(act, nullptr);

    EXPECT_EQ(act->doAction(), "Archived files :");
}