#include "exitAction.hpp"
#include "test.hpp"

TEST(ExitAction, DoAction) {
    ExitAction action;
    EXPECT_EQ(action.doAction(), "Quiting...");
}

TEST(ExitAction, Description) {
    ExitAction action;
    EXPECT_EQ(action.description(), "Closing DocChiver");
}

TEST(ExitAction, Name) {
    ExitAction action;
    EXPECT_EQ(action.name(), "exitAction");
}