#include "findAction.hpp"
#include "archiveFactory.hpp"
#include "file.hpp"
#include "test.hpp"
#include "actionsProvider.hpp"
#include "cliParser.hpp"
#include "tools.hpp"

TEST(FindAction, DoActionSingleKeyWord) {  
    const std::string filePath = currentSourceFileDirectory(__FILE__) + "/files/pdfFile.pdf";
    Archive * archive = ArchiveFactory::get();
    
    ArchivedFile archivedFile(filePath);

    archive->addFile(&archivedFile);
    archive->save();

    ActionsProvider * ap = new ActionsProvider();
    ap->add("find",      []() -> Action * { return new FindAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("find consectetur");
    EXPECT_NE(dynamic_cast<FindAction *>(action), nullptr);

    const std::string expectedResult = "'consectetur' is found in :\n\t * pdfFile.pdf [" + filePath + "]";
    EXPECT_EQ(action->doAction(), expectedResult);
}

TEST(FindAction, DoActionMultipleKeyWordExist) {
    const std::string filePath = currentSourceFileDirectory(__FILE__) + "/files/pdfFile.pdf";
    Archive * archive = ArchiveFactory::get();
    
    ArchivedFile archivedFile(filePath);

    archive->addFile(&archivedFile);
    archive->save();

    ActionsProvider * ap = new ActionsProvider();
    ap->add("find",      []() -> Action * { return new FindAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("find consectetur eiusmod");
    EXPECT_NE(dynamic_cast<FindAction *>(action), nullptr);

    const std::string expectedResult = "'consectetur' is found in :\n\t * pdfFile.pdf [" + filePath + "]\n'eiusmod' is found in :\n\t * pdfFile.pdf [" + filePath + "]";
    EXPECT_EQ(action->doAction(), expectedResult);
}

TEST(FindAction, DoActionNotFoundKeyword) {    
    Archive * archive = ArchiveFactory::get();
    
    ArchivedFile archivedFile(currentSourceFileDirectory(__FILE__) + "/files/pdfFile.pdf");

    archive->addFile(&archivedFile);
    archive->save();

    ActionsProvider * ap = new ActionsProvider();
    ap->add("find",      []() -> Action * { return new FindAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("find unknown");
    EXPECT_NE(dynamic_cast<FindAction *>(action), nullptr);

    const std::string expectedResult = "No file contains 'unknown'...";
    EXPECT_EQ(action->doAction(), expectedResult);
}

TEST(FindAction, Description) {
    FindAction action;
    EXPECT_EQ(action.description(), "Retuns a list of files that contain keyword");
}

TEST(FindAction, Name) {
    FindAction action;
    EXPECT_EQ(action.name(), "findAction");
}