#include "helpAction.hpp"
#include "test.hpp"

TEST(HelpAction, DoAction) {
    HelpAction action;
    EXPECT_EQ(action.doAction(), "This tool permits to archive file and research them by their keywords.");
}

TEST(HelpAction, Description) {
    HelpAction action;
    EXPECT_EQ(action.description(), "Display help content");
}

TEST(HelpAction, Name) {
    HelpAction action;
    EXPECT_EQ(action.name(), "helpAction");
}