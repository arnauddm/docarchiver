#include "listAction.hpp"
#include "jsonArchive.hpp"
#include "file.hpp"
#include "test.hpp"
#include "textFile.hpp"


TEST(ListAction, doAction) {   
    JsonArchive archive("archive.json");
    
    ArchivedFile archivedFile;
    archivedFile.setPath("/home/file.txt");

    archive.addFile(&archivedFile);
    archive.save();
    
    ListAction action;

    const std::string expectedStr = "Archived files :\n\t* file.txt [/home/file.txt]";
    EXPECT_EQ(action.doAction(), expectedStr);
}

TEST(ListAction, doActionWithWrongArchive) {   
    TextFile archive("archive.json");
    EXPECT_EQ(archive.write("Hello world }"), true);
    
    ListAction action;

    const std::string expectedStr = "An error occured while reading archive file";
    EXPECT_EQ(action.doAction(), expectedStr);
}

TEST(ListAction, Description) {
    ListAction action;
    EXPECT_EQ(action.description(), "Listing all archived files");
}

TEST(ListAction, Name) {
    ListAction action;
    EXPECT_EQ(action.name(), "listAction");
}