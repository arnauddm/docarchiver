#include "test.hpp"
#include "newAction.hpp"
#include "actionsProvider.hpp"
#include "cliParser.hpp"
#include "context.hpp"

TEST(NewAction, Description)
{
    NewAction action;
    EXPECT_EQ(action.description(), "Create a new archive by specifying its name (with the extension).");
}

TEST(NewAction, Name)
{
    NewAction action;
    EXPECT_EQ(action.name(), "newAction");
}

TEST(NewAction, doActionNoOtions)
{   
    ActionsProvider * ap = new ActionsProvider();
    ap->add("new",      []() -> Action * { return new NewAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("new");

    EXPECT_NE(dynamic_cast<NewAction *>(action), nullptr);

    EXPECT_EQ(action->doAction(), Action::WRONG_OPTION_STRING);
}

TEST(NewAction, doActionWrongOtions)
{   
    ActionsProvider * ap = new ActionsProvider();
    ap->add("new",      []() -> Action * { return new NewAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("new test1 test2");

    EXPECT_NE(dynamic_cast<NewAction *>(action), nullptr);

    EXPECT_EQ(action->doAction(), Action::WRONG_OPTION_STRING);
}

TEST(NewAction, doActionCorrect)
{   
    ActionsProvider * ap = new ActionsProvider();
    ap->add("new",      []() -> Action * { return new NewAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("new archiveTest.json");

    EXPECT_NE(dynamic_cast<NewAction *>(action), nullptr);

    EXPECT_EQ(action->doAction(), "Archive (" + Context::getInstance()->launchedFrom() + "/archiveTest.json) created !");
}