#include "test.hpp"
#include "file.hpp"
#include "cliParser.hpp"
#include "actionsProvider.hpp"
#include "removeAction.hpp"
#include "archiveFactory.hpp"

class FileTest : public File
{
public:
    FileTest(std::string filename) : File(filename)
    {
    }

    ~FileTest(void)
    {
    }

    std::list<std::string> parse(void)
    {
        return std::list<std::string>();
    }
};

TEST(RemoveAction, ConstAndDest)
{
    RemoveAction * action = new RemoveAction();
    delete action;
}

TEST(RemoveAction, Description)
{
    RemoveAction action;
    EXPECT_EQ(action.description(), "Remove a file from the archive");
}

TEST(RemoveAction, RemoveExistentFileWithName) {
    Archive * archive = ArchiveFactory::get();

    ArchivedFile file("name.txt");
    file.addKeyword("keyword1");

    archive->addFile(&file);
    EXPECT_EQ(archive->save(), true);

    // Force reading the archive file
    delete archive;
    archive = ArchiveFactory::get();

    ActionsProvider * ap = new ActionsProvider();
    ap->add("remove",      []() -> Action * { return new RemoveAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("remove --name name.txt");

    EXPECT_NE(dynamic_cast<RemoveAction *>(action), nullptr);

    EXPECT_EQ(action->doAction(), "Well removed");
}

TEST(RemoveAction, RemoveNonExistentFileWithName) {
    Archive * archive = ArchiveFactory::get();

    ArchivedFile file("myPath.txt");
    file.addKeyword("keyword1");

    archive->addFile(&file);
    EXPECT_EQ(archive->save(), true);

    // Force reading the archive file
    delete archive;
    archive = ArchiveFactory::get();

    ActionsProvider * ap = new ActionsProvider();
    ap->add("remove",      []() -> Action * { return new RemoveAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("remove --name unknown");

    EXPECT_NE(dynamic_cast<RemoveAction *>(action), nullptr);

    EXPECT_EQ(action->doAction(), "An error occured when trying to remove unknown");
}

TEST(RemoveAction, RemoveExistentFileWithPath) {
    Archive * archive = ArchiveFactory::get();

    ArchivedFile file("/home/myPath.txt");
    file.addKeyword("keyword1");

    archive->addFile(&file);
    EXPECT_EQ(archive->save(), true);

    // Force reading the archive file
    delete archive;
    archive = ArchiveFactory::get();

    ActionsProvider * ap = new ActionsProvider();
    ap->add("remove",      []() -> Action * { return new RemoveAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("remove --path /home/myPath.txt");

    EXPECT_NE(dynamic_cast<RemoveAction *>(action), nullptr);

    EXPECT_EQ(action->doAction(), "Well removed");
}

TEST(RemoveAction, RemoveNonExistentFileWithPath) {
    Archive * archive = ArchiveFactory::get();

    ArchivedFile file("myPath.txt");
    file.addKeyword("keyword1");

    archive->addFile(&file);
    EXPECT_EQ(archive->save(), true);

    // Force reading the archive file
    delete archive;
    archive = ArchiveFactory::get();

    ActionsProvider * ap = new ActionsProvider();
    ap->add("remove",      []() -> Action * { return new RemoveAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("remove --path unknown");

    EXPECT_NE(dynamic_cast<RemoveAction *>(action), nullptr);

    EXPECT_EQ(action->doAction(), "An error occured when trying to remove unknown");
}

TEST(RemoveAction, NoOptions) {
    Archive * archive = ArchiveFactory::get();

    ArchivedFile file("myPath.txt");
    file.addKeyword("keyword1");

    archive->addFile(&file);
    EXPECT_EQ(archive->save(), true);

    // Force reading the archive file
    delete archive;
    archive = ArchiveFactory::get();

    ActionsProvider * ap = new ActionsProvider();
    ap->add("remove",      []() -> Action * { return new RemoveAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("remove");

    EXPECT_NE(dynamic_cast<RemoveAction *>(action), nullptr);

    EXPECT_EQ(action->doAction(), Action::WRONG_OPTION_STRING);
}

TEST(RemoveAction, WrongOption) {
    Archive * archive = ArchiveFactory::get();

    ArchivedFile file("file.txt");
    file.addKeyword("keyword1");

    archive->addFile(&file);
    EXPECT_EQ(archive->save(), true);

    // Force reading the archive file
    delete archive;
    archive = ArchiveFactory::get();

    ActionsProvider * ap = new ActionsProvider();
    ap->add("remove",      []() -> Action * { return new RemoveAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("remove --bla file.txt");

    EXPECT_NE(dynamic_cast<RemoveAction *>(action), nullptr);

    EXPECT_EQ(action->doAction(), Action::WRONG_OPTION_STRING);
}
