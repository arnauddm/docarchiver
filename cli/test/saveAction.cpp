#include "test.hpp"
#include "saveAction.hpp"
#include "actionsProvider.hpp"
#include "cliParser.hpp"
#include "file.hpp"
#include "encryptor.hpp"

TEST(SaveAction, doAction_NoOptions)
{  
    ActionsProvider * ap = new ActionsProvider();
    ap->add("save",      []() -> Action * { return new SaveAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("save");

    EXPECT_NE(dynamic_cast<SaveAction *>(action), nullptr);

    EXPECT_EQ(action->doAction(), "Archive well saved !");
}

TEST(SaveAction, doAction_Name)
{  
    ActionsProvider * ap = new ActionsProvider();
    ap->add("save",      []() -> Action * { return new SaveAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("save --name test");

    EXPECT_NE(dynamic_cast<SaveAction *>(action), nullptr);

    EXPECT_EQ(action->doAction(), "Archive well saved !");
    EXPECT_EQ(File::exist("test.json"), true);
}

TEST(SaveAction, doAction_BadEncryptionOption)
{  
    ActionsProvider * ap = new ActionsProvider();
    ap->add("save",      []() -> Action * { return new SaveAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("save --encryption tmp");

    EXPECT_NE(dynamic_cast<SaveAction *>(action), nullptr);

    EXPECT_EQ(action->doAction(), Action::WRONG_OPTION_STRING);
}

TEST(SaveAction, doAction_EncryptionNoKey)
{  
    ActionsProvider * ap = new ActionsProvider();
    ap->add("save",      []() -> Action * { return new SaveAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("save --encryption true");

    EXPECT_NE(dynamic_cast<SaveAction *>(action), nullptr);

    EXPECT_EQ(action->doAction(), Action::WRONG_OPTION_STRING);
}

TEST(SaveAction, doAction_Encryption)
{
    Encryptor encryptor;
    EXPECT_EQ(encryptor.createKeyNoPassword("test@docarchiver.com"), true);

    ActionsProvider * ap = new ActionsProvider();
    ap->add("save",      []() -> Action * { return new SaveAction; });

    CliParser cliParser(ap);

    Action * action = cliParser.parse("save --encryption true --key test@docarchiver.com");

    EXPECT_NE(dynamic_cast<SaveAction *>(action), nullptr);

    EXPECT_EQ(action->doAction(), "Archive well saved !");

    EXPECT_EQ(encryptor.deleteKey("test@docarchiver.com"), true);
}



TEST(SaveAction, Description)
{
    SaveAction action;
    
    std::stringstream ss;
    ss << "Save manually the archive (this command does not disable the automatic save)." << std::endl;
    ss << "     --encryption=<true|false>" << std::endl;
    ss << "     --name=<...> | Only the name without the extension !"; 
    
    EXPECT_EQ(action.description(), ss.str());
}

TEST(SaveAction, Name)
{
    SaveAction action;

    EXPECT_EQ(action.name(), "saveAction");
}