#include "textFile.hpp"
#include "test.hpp"
#include "tools.hpp"
#include "scriptRunnerMock.hpp"
#include "archiveFactory.hpp"

class FileTest : public File
{
public:
    FileTest(std::string filename) : File(filename)
    {
    }

    ~FileTest(void)
    {
    }

    std::list<std::string> parse(void)
    {
        return std::list<std::string>();
    }
};

TEST(ScriptRunner, RunScript) {
    Archive * archive = ArchiveFactory::newOne("archive.json");
    archive->save();

    const std::string filePath = currentSourceFileDirectory(__FILE__) + "/script.txt";
    FileTest file(filePath);

    std::string str;
    str += "add " + currentSourceFileDirectory(__FILE__) + "/files/pdfFile.pdf";
    str += "\n";
    str += "list";

    file.write(str);

    ScriptRunnerMock runner(filePath);

    std::string expectedString = "pdfFile.pdf is successfully added !\nArchived files :\n\t* pdfFile.pdf [" + currentSourceFileDirectory(__FILE__) + "/files/pdfFile.pdf]";

    std::string output = runner.run();

    EXPECT_EQ(output, expectedString);

    delete archive;
    File::remove(filePath);
}

TEST(ScriptRunner, RunNonExistentScript)
{
    ScriptRunnerMock runner("HelloWorld.txt");

    std::string expectedString = "HelloWorld.txt not found";

    std::string output = runner.run();

    EXPECT_EQ(output, expectedString);
}

TEST(ScriptRunner, RunScriptWithError) {
    Archive * archive = ArchiveFactory::newOne("archive.json");
    archive->save();

    const std::string filePath = currentSourceFileDirectory(__FILE__) + "/script.txt";
    FileTest file(filePath);

    std::string str;
    str += "addd";

    file.write(str);

    ScriptRunnerMock runner(filePath);

    std::string expectedString = "Syntax not recognised for : addd";

    std::string output = runner.run();

    EXPECT_EQ(output, expectedString);

    delete archive;
    File::remove(filePath);
}