#include "test.hpp"
#include "tools.hpp"
#include "actionsProviderFactoryMock.hpp"


TEST(ActionsProviderFactoryMock, Test) {
    ActionsProvider * provider = ActionsProviderFactoryMock::build();
    
    EXPECT_EQ(provider != nullptr, true);

    EXPECT_EQ(provider->actionsName().size(), 8);
    EXPECT_EQ(provider->get("list") != nullptr, true);
    EXPECT_EQ(provider->get("exit") != nullptr, true);
    EXPECT_EQ(provider->get("help") != nullptr, true);
    EXPECT_EQ(provider->get("add") != nullptr, true);
    EXPECT_EQ(provider->get("remove") != nullptr, true);
    EXPECT_EQ(provider->get("find") != nullptr, true);
    EXPECT_EQ(provider->get("save") != nullptr, true);
    EXPECT_EQ(provider->get("new") != nullptr, true);

    delete provider;
}