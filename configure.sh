#!/bin/sh

TRUE=1
FALSE=0
EMPTY= 

CMAKE_INSTALL_DIR=.
NO_TEST=$FALSE
NO_GUI=$FALSE
COMPILATION_MODE=-DCMAKE_BUILD_TYPE=Release
NINJA=$EMPTY
GUI=$EMPTY
COVERAGE=-DCODE_COVERAGE=OFF

for arg in "$@"
do
    case "$arg" in
        "--no-test")
            NO_TEST=$TRUE
            ;;
        "--no-gui")
            NO_GUI=$TRUE
            ;;
        "--debug")
            COMPILATION_MODE=-DCMAKE_BUILD_TYPE=Debug
            ;;
        "--ninja")
            NINJA=-GNinja
            ;;
        "--coverage")
            COVERAGE=-DCODE_COVERAGE=ON
            ;;
        *)
            echo "./configure.sh --(help|no-test|no-gui)"
            exit 1
            ;;
    esac
done

BUILD_TEST="TEST=True"
BUILD_GUI="GUI=True"
QT5_PREFIX=""

if [ $NO_TEST -eq $TRUE ]
then
    BUILD_TEST="TEST=False"
fi

if [ $NO_GUI -eq $TRUE ]
then
    BUILD_GUI="GUI=False"
else
    if [ $(uname) == "Darwin" ]
    then

        if [ -f $(command -v brew) ]
        then
            QT5_PREFIX=$(brew --prefix qt5)
        else
            echo "Can't determine where Qt5 is installed."
            echo "Aborting..."
            exit 1
        fi
        GUI=-DCMAKE_PREFIX_PATH=$QT5_PREFIX
    fi
fi

if [ -d "build/" ]
then
    rm -rf build
fi

mkdir build

# Change directory name to fit with username
if [ $NO_GUI -eq $FALSE ]
then
    cp -r .desktop/ build/.
    sed -i 's/<USER>/'${USER}'/g' build/.desktop/docarchiver.desktop
fi

# If the repository was cloned, applying "default" project configuration
if [ -e .git ]
then
    # Applying project commit template
    echo "Configuring git with project configuration"
    git config --local include.path ../.gitconfig
fi

(cd build && cmake .. $GUI -D"$BUILD_TEST" -D"$BUILD_GUI" $COMPILATION_MODE $NINJA $COVERAGE -DCMAKE_CXX_FLAGS=-I\ /usr/include/jsoncpp/json)