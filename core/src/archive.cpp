#include "archive.hpp"
#include "fileTypeProvider.hpp"

/**
 * @brief Construct a new Archive:: Archive object
 * 
 */
Archive::Archive(void)
{
    //read();
}

/**
 * @brief Destroy the Archive:: Archive object
 * 
 */
Archive::~Archive(void)
{
}

/**
 * @brief Add file to the archive
 * 
 * @param file 
 */
void Archive::addFile(ArchivedFile *file)
{
    _archivedFiles.insert({
        file->path(),
        file
    });
}

/**
 * @brief Find all files which contain the provided keyword
 * 
 * @param keyword Keyword to search
 * @return std::map<std::string, ArchivedFile *> All archived files which contain the given keyword
 */
std::map<std::string, ArchivedFile *> Archive::findByKeyword(std::string keyword)
{
    std::map<std::string, ArchivedFile *> files;
    for (std::map<std::string, ArchivedFile *>::iterator it = _archivedFiles.begin(); it != _archivedFiles.end(); it++)
    {
        ArchivedFile * tmp = new ArchivedFile(*it->second);

        File * file = FileTypeProvider::file(tmp->path());
        tmp->addKeywords(file->parse());
        delete file; file = nullptr;

        if (tmp->findKeyword(keyword) > 0)
            files.insert({
                tmp->path(),
                tmp
            });
        else
            delete tmp;
    }
    return files;
}

/**
 * @brief Get all archived files
 * 
 * @return std::map<ArchivedFile *> All files
 */
std::map<std::string, ArchivedFile *> Archive::allFiles(void)
{
    return _archivedFiles;
}

/**
 * @brief Remove a file from the archive according to its name
 * 
 * @param name : name of the file
 * @return true : the file is successfully removed
 * @return false : an error occured
 */
bool Archive::removeFileWithName(std::string name)
{
    for (
        std::map<std::string, ArchivedFile *>::iterator it = _archivedFiles.begin();
        it != _archivedFiles.end();
        it++)
    {
        if ((it->second)->name() == name)
        {
            _archivedFiles.erase(it->first);
            save();
            return true;
        }
    }
    return false;
}

/**
 * @brief Remove a file from the archive according to its path
 * 
 * @param path 
 * @return true : the file is successfully removed
 * @return false : an error occured
 */
bool Archive::removeFileWithPath(std::string path)
{
    if(_archivedFiles.count(path) == 0)
        return false;
        
    _archivedFiles.erase(path);
    save();

    return true;
}

/**
 * @brief Reset the archive
 * 
 */
void Archive::reset(void)
{
    _archivedFiles.clear();
}

/**
 * @brief Returns the archive path
 * 
 * @return std::string Filepath of the archive
 */
std::string Archive::archivePath(void)
{
    return _archivePath;
}

/**
 * @brief Set the archive path
 * 
 * @param path Filepath of the archive
 */
void Archive::setArchivePath(const std::string & path)
{
    _archivePath = path;
}

/**
 * @brief Returns Archived File associated 
 * 
 * @param filename Filename of the wanted file
 * @return ArchivedFile * Pointer on the instance, @ref nullptr if not found
 */
ArchivedFile * Archive::archivedFile(const std::string & filename)
{
    return _archivedFiles.count(filename) == 0 ? nullptr : _archivedFiles[filename];
}