#ifndef __ARCHIVE_HPP__
#define __ARCHIVE_HPP__

#include "archivedFile.hpp"
#include <map>

class Archive {
    public:
        typedef std::map<std::string, ArchivedFile *> ArchivedFiles;

        Archive(void);
        virtual ~Archive(void) = 0;

        void addFile(ArchivedFile * file);
        ArchivedFiles allFiles(void);
        bool removeFileWithName(std::string name);
        bool removeFileWithPath(std::string path);

        ArchivedFile * archivedFile(const std::string & filename);

        ArchivedFiles findByKeyword(std::string keyword);

        virtual bool read(void) = 0;
        virtual bool save(void) = 0;
        virtual bool saveWithEncryption(const std::string & userKey) = 0;
        virtual std::string content(void) = 0;

        std::string archivePath(void);
        void setArchivePath(const std::string & path);

        virtual std::string serialize(void) = 0;

    protected:
        void reset(void);

    private:
        ArchivedFiles _archivedFiles;
        std::string _archivePath;
};

#endif