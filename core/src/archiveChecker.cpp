#include "archiveChecker.hpp"
#include "file.hpp"

/**
 * @brief Construct a new Archive Checker:: Archive Checker object
 * 
 */
ArchiveChecker::ArchiveChecker(void) : _archive(nullptr)
{

}

/**
 * @brief Destroy the Archive Checker:: Archive Checker object
 * 
 */
ArchiveChecker::~ArchiveChecker(void)
{
    // As achive pointer is passed from another function, do not delete it here
}

/**
 * @brief Set the archive to check
 * 
 * @param archive Archive to check
 */
void ArchiveChecker::setArchive(Archive * archive)
{
    _archive = archive;
}

/**
 * @brief Check if all files in the archive exist
 * 
 * @return Checker::Errors If no archive, returns "No archive to check...", otherwise, returns all non-existant files path
 */
Checker::Errors ArchiveChecker::check(void)
{
    Checker::Errors errors;

    Archive::ArchivedFiles allFiles = _archive->allFiles();
    for(Archive::ArchivedFiles::iterator it = allFiles.begin();
        it != allFiles.end();
        it++)
    {
        ArchivedFile * file = it->second;
        if(!File::exist(file->path()))
            errors.push_back(file->path());
    }

    return errors;
}