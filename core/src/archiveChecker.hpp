#ifndef __ARCHIVE_CHECKER_HPP__
#define __ARCHIVE_CHECKER_HPP__

#include "checker.hpp"
#include "archive.hpp"

class ArchiveChecker : public Checker
{
public:
    ArchiveChecker(void);
    ~ArchiveChecker(void);

    Errors check(void) override;
    void setArchive(Archive * archive);

private:
    Archive * _archive;
};

#endif