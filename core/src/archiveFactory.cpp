#include "archiveFactory.hpp"
#include "jsonArchive.hpp"
#include "context.hpp"

/**
 * @brief Get a new reference to the archive "manager"
 * 
 * @return Archive* 
 */
Archive * ArchiveFactory::get(void) {
    return new JsonArchive(archivePath());
}

/**
 * @brief Set the archive filepath
 * 
 * @param path Filepath of the archive
 */
void ArchiveFactory::setArchivePath(const std::string & path)
{
    archivePath() = path;
}

/**
 * @brief Returns the archive filepath
 * 
 * @return std::string Filepath of the archive
 */
std::string & ArchiveFactory::archivePath(void)
{
    // "archive.json" is considered to be the default value (no problem today as we have only one archive format)
    // This default value will be changed by calling @ref ArchiveFactory::setArchivePath()
    static std::string path = "archive.json";
    return path;
}

/**
 * @brief Create a new archive
 * 
 * @param name Name of the archive
 * @return Archive* New archive instance
 */
Archive * ArchiveFactory::newOne(const std::string & name)
{
    setArchivePath(name[0] == '/' ? name : Context::getInstance()->launchedFrom() + "/" + name);
    Archive * archive = new JsonArchive(archivePath());
    archive->save();
    return archive;
}