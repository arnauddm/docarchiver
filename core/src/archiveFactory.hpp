#ifndef __ARCHIVE_FACTORY_HPP__
#define __ARCHIVE_FACTORY_HPP__

#include "archive.hpp"

class ArchiveFactory {
    public:
        static Archive * get(void);
        static Archive * newOne(const std::string & name);
        static void setArchivePath(const std::string & path);

        static std::string & archivePath(void);
};

#endif