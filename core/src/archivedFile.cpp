#include "archivedFile.hpp"
#include "textFile.hpp"
#include "pdfFile.hpp"
#include "fileTypeProvider.hpp"

/**
 * @brief Construct a new Archived File:: Archived File object
 * 
 */
ArchivedFile::ArchivedFile(void) {

}

/**
 * @brief Construct a new Archived File and parse its content
 * 
 * @param filepath
 */
ArchivedFile::ArchivedFile(std::string filepath) :
    _path(filepath),
    _name(extractName(filepath)),
    _extension(extractExtension(filepath))
{
}

/**
 * @brief Construct a new Archived File:: Archived File object
 * 
 * @param otherFile Object to copy
 */
ArchivedFile::ArchivedFile(ArchivedFile * otherFile)
{
    _name = otherFile->name();
    _path = otherFile->path();
    _keywords = otherFile->keywords();
    _personnalKeywords = otherFile->personnalKeywords();
}

/**
 * @brief Destroy the Archived File:: Archived File object
 * 
 */
ArchivedFile::~ArchivedFile(void) {
   
}

/**
 * @brief Get the name of the file
 * 
 * @return std::string
 */
std::string ArchivedFile::name(void) {
    return extractName(_path);
}

/**
 * @brief Get the path of the file
 * 
 * @return std::string 
 */
std::string ArchivedFile::path(void) {
    return _path;
}

/**
 * @brief Set the path of the file
 * 
 * @param path 
 */
void ArchivedFile::setPath(const std::string & path) {
    _path = path;
}

/**
 * @brief Get all keywords (which were found with the "auto-parse")
 * 
 * @return std::map<std::string, int> 
 */
std::map<std::string, int> ArchivedFile::keywords(void) {
    addKeywordsIfNeeded();
    return _keywords;
}

/**
 * @brief If no keywords in the structure, parse (again) and add them
 * 
 */
void ArchivedFile::addKeywordsIfNeeded(void)
{
    if(!_keywords.size() && File::exist(path()))
    {
        File * file = FileTypeProvider::file(path());
        if(!file)
            return;
        addKeywords(file->parse());
        delete file;
    }
}

/**
 * @brief Count provided keyword
 * 
 * @param keyword 
 * @return int : number of occurence of the provided keyword 
 */
int ArchivedFile::findKeyword(std::string keyword) {
    addKeywordsIfNeeded();

    std::map<std::string, int>::iterator itKeywords = _keywords.find(keyword);
    std::map<std::string, int>::iterator itPersonnal = _personnalKeywords.find(keyword);

    return
        (itKeywords == _keywords.end() ? 0 : itKeywords->second) +
        (itPersonnal == _personnalKeywords.end() ? 0 : itPersonnal->second);
}

/**
 * @brief Add a keyword
 * 
 * @param keyword 
 */
void ArchivedFile::addKeyword(std::string keyword) {
    _keywords[keyword]++;
}

/**
 * @brief Add multiple keywords at a time
 * 
 * @param keywords 
 */
void ArchivedFile::addKeywords(std::list<std::string> keywords) {
    for(std::list<std::string>::iterator it = keywords.begin(); it != keywords.end(); it++)
        addKeyword(*it);
}

/**
 * @brief Add a "personal keyword", eg: a keyword that the user want to add manually
 * 
 * @param keyword 
 */
void ArchivedFile::addPersonnalKeyword(std::string keyword) {
    _personnalKeywords[keyword]++;
}

/**
 * @brief Add multiple additional keywords at a time
 * 
 * @param keywords 
 */
void ArchivedFile::addPersonnalKeywords(std::list<std::string> keywords) {
    for(std::list<std::string>::iterator it = keywords.begin(); it != keywords.end(); it++)
        addPersonnalKeyword(*it);
}

/**
 * @brief Returns all "personal" keywords
 * 
 * @return std::map<std::string, int> 
 */
std::map<std::string, int> ArchivedFile::personnalKeywords(void) {
    return _personnalKeywords;
}

/**
 * @brief Get the extension of the file
 * 
 * @param filepath 
 * @return std::string 
 */
std::string ArchivedFile::extractExtension(std::string filepath) {
    return filepath.substr(filepath.find_last_of(".") + 1);
}

/**
 * @brief Get the name of the file 
 * 
 * @param filepath 
 * @return std::string 
 */
std::string ArchivedFile::extractName(std::string filepath) {
    return filepath.substr(filepath.find_last_of("/") + 1);
}

/**
 * @brief Returns the extension of the file
 * 
 * @return std::string Extension
 */
std::string ArchivedFile::extension(void)
{
    return extractExtension(_path);
}