#ifndef __ARCHIVED_FILE_HPP__
#define __ARCHIVED_FILE_HPP__

#include <string>
#include <map>
#include <list>

class ArchivedFile {
    public:
        ArchivedFile(void);
        ArchivedFile(std::string filepath);
        ArchivedFile(ArchivedFile * otherFile);
        ~ArchivedFile(void);

        std::string name(void);
        std::string extension(void);

        std::string path(void);
        void setPath(const std::string & path);

        std::map<std::string, int> keywords(void);
        std::map<std::string, int> personnalKeywords(void);
        int findKeyword(std::string);
        void addKeyword(std::string keyword);
        void addKeywords(std::list<std::string> keywords);
        void addPersonnalKeyword(std::string keyword);
        void addPersonnalKeywords(std::list<std::string> keywords);

    private:
        std::string _name;
        std::string _path;
        std::string _extension;
        std::map<std::string, int> _keywords;
        std::map<std::string, int> _personnalKeywords;

        std::string extractExtension(std::string filepath);
        std::string extractName(std::string filepath);
        void addKeywordsIfNeeded(void);
};

#endif