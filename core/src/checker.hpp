#ifndef __CHECKER_HPP__
#define __CHECKER_HPP__

#include <list>
#include <string>

class Checker
{
public:
    typedef std::list<std::string> Errors;

    Checker(void);
    ~Checker(void);

    virtual Errors check(void) = 0;
};

#endif