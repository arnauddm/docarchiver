#include "configuration.hpp"
#include <iostream>

/**
 * @brief Construct a new Configuration:: Configuration object
 * 
 */
Configuration::Configuration(void)
{
}

/**
 * @brief Destroy the Configuration:: Configuration object
 * 
 */
Configuration::~Configuration(void)
{
}

/**
 * @brief Set the directory where the archive file can be found
 * 
 * @param directory : directory where the archive is saved
 */
void Configuration::setArchiveDirectory(const std::string &directory)
{
    _archiveDirectory = directory;
}

/**
 * @brief Returns the directory where the archive file is saved
 * 
 * @return std::string 
 */
std::string Configuration::archiveDirectory(void)
{
    return _archiveDirectory;
}

/**
 * @brief Set all default configuration is case of non-existant configuration file
 * 
 */
void Configuration::setDefaultConfiguration(void) {
    _archiveDirectory = ".";
}

/**
 * @brief Add a new mappig
 * 
 * @param sourceType The extension you want to map
 * @param asType The extension which masks the `sourceType` type
 */
void Configuration::addFileTypeMapping(const std::string & sourceType, const std::string & asType)
{
    _fileTypesMapping.insert({
        sourceType,
        asType
    });
}

/**
 * @brief Return the mapping of file types
 * 
 * @return std::map<std::string, std::string> 
 */
std::map<std::string, std::string> Configuration::fileTypesMapping(void)
{
    return _fileTypesMapping;
}