#ifndef __CONFIGURATION_HPP__
#define __CONFIGURATION_HPP__

#include <string>
#include <map>

class Configuration
{
public:
    Configuration(void);
    virtual ~Configuration(void);

    std::string archiveDirectory(void);
    std::map<std::string, std::string> fileTypesMapping(void);

protected:
    void setArchiveDirectory(const std::string &path);
    void addFileTypeMapping(const std::string & sourceType, const std::string & asType);
    
    virtual void readConfiguration(void) = 0;
    void setDefaultConfiguration(void);

private:
    std::string _archiveDirectory;
    std::map<std::string, std::string> _fileTypesMapping;
};

#endif