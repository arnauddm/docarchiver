#include "context.hpp"
#include <filesystem>
#include "jsonConfiguration.hpp"

Context *Context::_instance = nullptr;

/**
 * @brief Construct a new Context:: Context object
 * 
 */
Context::Context(void)
{
    _launchedFrom = std::filesystem::current_path();
    _configuration = new JsonConfiguration();
}

/**
 * @brief Destroy the Context:: Context object
 * 
 */
Context::~Context(void)
{
}

/**
 * @brief Returns the directory where the application is launched from
 * 
 * @return std::string 
 */
std::string Context::launchedFrom(void)
{
    return _launchedFrom;
}

/**
 * @brief Get the Context instance (singleton)
 * 
 * @return Context* 
 */
Context *Context::getInstance(void)
{
    if (!_instance)
        _instance = new Context();
    return _instance;
}

/**
 * @brief Reset the instance to refresh the configuration for example
 * 
 */
void Context::resetInstance(void) {
    if(getInstance())
        delete getInstance();

    _instance = new Context();
}

/**
 * @brief Returns the configuration
 * 
 * @return Configuration* 
 */
Configuration *Context::configuration(void)
{
    return _configuration;
}