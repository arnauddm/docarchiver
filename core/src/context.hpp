#ifndef __CONTEXT_HPP__
#define __CONTEXT_HPP__

#include <string>
#include "configuration.hpp"

class Context
{
public:
    static Context * getInstance(void);
    static void resetInstance(void);
    std::string launchedFrom(void);
    Configuration * configuration(void);
    
private:
    Context(void);
    ~Context(void);

    std::string _launchedFrom;
    Configuration * _configuration;

    static Context * _instance;
};

#endif