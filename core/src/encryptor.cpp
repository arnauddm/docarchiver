#include "encryptor.hpp"
#include <list>
#include <string>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/**
 * @brief Construct a new Encryptor:: Encryptor object
 * 
 */
Encryptor::Encryptor(void)
{
    _context = new gpgme_ctx_t();
    setlocale (LC_ALL, "");
    gpgme_check_version (NULL);
    gpgme_set_locale (NULL, LC_CTYPE, setlocale (LC_CTYPE, NULL));
    gpgme_new(_context);
    gpgme_set_armor(*_context, 1);
    gpgme_set_textmode(*_context, 0);
    gpgme_engine_check_version(GPGME_PROTOCOL_OpenPGP);
}

/**
 * @brief Destroy the Encryptor:: Encryptor object
 * 
 */
Encryptor::~Encryptor(void)
{
    gpgme_release(*_context);

    if(_context)
        delete _context;
}

/**
 * @brief 
 * 
 * @param in String to encrypt
 * @param out Ciphered string
 * @param keyId user id with which "in" parameter will be encrypted
 * @return true In case of success
 * @return false In case of error
 */
bool Encryptor::encrypt(const std::string & in, std::string & out, const std::string & keyId)
{
    const gpgme_encrypt_flags_t flags = GPGME_ENCRYPT_NO_ENCRYPT_TO;
    gpgme_data_t plaintext;
    gpgme_data_t ciphered;
    gpgme_key_t userKey = getKey(keyId);

    if(!userKey)
        return false;

    if(gpgme_data_new_from_mem(&plaintext, in.c_str(), in.size(), 0) != GPG_ERR_NO_ERROR
    ||  gpgme_data_new (&ciphered) != GPG_ERR_NO_ERROR)
        return false;

    gpgme_key_t recipients[] = {
        userKey,
        nullptr,
    };

    if(gpgme_op_encrypt(*_context, recipients, flags, plaintext, ciphered) != GPG_ERR_NO_ERROR)
        return false;

    out = dataStructToString(ciphered);

    gpgme_data_release(plaintext);
    gpgme_data_release(ciphered);
    
    return true;
}

std::string Encryptor::dataStructToString(gpgme_data_t data)
{
    std::string out;
    int size;

    gpgme_data_seek (data,0, SEEK_SET);

    do
    {
        int bufferSize = 512;
        char buffer[bufferSize];
        memset(buffer, 0, bufferSize);
        size = gpgme_data_read (data, &buffer, bufferSize);

        if(size > 0)
        {
            out += std::string(buffer).substr(0, size);
        }

    } while(size > 0);  

    return out;
}

/**
 * @brief Decrypt a ciphered string
 * 
 * @param in Ciphered content
 * @param out Decrypted content
 * @param keyId user id with which "in" parameter will be decrypted
 * @return true in case of success
 * @return false in case of error
 */
bool Encryptor::decrypt(std::string in, std::string & out)
{
    gpgme_data_t ciphered;

    if(gpgme_data_new_from_mem(&ciphered, in.c_str(), in.length(), 1) != GPG_ERR_NO_ERROR)
        return false;

    bool result = decryptData(ciphered, out);

    gpgme_data_release(ciphered);
    
    return result;
}

/**
 * @brief Decrypt a ciphered string
 * 
 * @param in Ciphered content
 * @param out Decrypted content
 * @param keyId user id with which "in" parameter will be decrypted
 * @return true in case of success
 * @return false in case of error
 */
bool Encryptor::decryptFile(const std::string & filename, std::string & out)
{
    gpgme_data_t ciphered;

    if(gpgme_data_new_from_fd (&ciphered, open(filename.c_str(), O_LARGEFILE | O_RDONLY)) != GPG_ERR_NO_ERROR)
        return false;

    bool result = decryptData(ciphered, out);

    gpgme_data_release(ciphered);
    
    return result;
}

/**
 * @brief Decrypt @ref gpgme_data_t input
 * 
 * @param ciphered Ciphered data
 * @param out Output result
 * @return true If success
 * @return false If an error occured
 */
bool Encryptor::decryptData(gpgme_data_t ciphered, std::string & out)
{
    gpgme_data_t plaintext;

    if(gpgme_data_new (&plaintext) != GPG_ERR_NO_ERROR)
        return false;

    if(gpgme_op_decrypt_verify(*_context, ciphered, plaintext) != GPG_ERR_NO_ERROR)
        return false;

    out = dataStructToString(plaintext);
    gpgme_data_release(plaintext);

    return true;
}

/**
 * @brief Create a new entry key
 * 
 * @param user User id
 * @param flag Parameter flag
 * @return true in case of success
 * @return false in case of error
 */
bool Encryptor::createKey(const std::string & user, int flag /* = GPGME_CREATE_NOEXPIRE */)
{
    return gpgme_op_createkey (*_context, user.c_str(), NULL, 0, 0, NULL, flag) == 0;
}

/**
 * @brief Create a new entry key without asking a password
 * 
 * @param user User id
 * @return true if the key was successfully created
 * @return false if an error was encountered
 */
bool Encryptor::createKeyNoPassword(const std::string & user)
{
    return createKey(user, GPGME_CREATE_NOEXPIRE | GPGME_CREATE_NOPASSWD);
}

/**
 * @brief Delete a key
 * 
 * @param user User id to delete
 * @return true If success
 * @return false If error
 */
bool Encryptor::deleteKey(const std::string & user)
{
    gpgme_key_t userKey = getKey(user);
    if(!userKey)
        return false;
        
    return gpgme_op_delete_ext (*_context, userKey, GPGME_DELETE_FORCE | GPGME_DELETE_ALLOW_SECRET) == GPG_ERR_NO_ERROR;
}

/**
 * @brief Returns a key according to the user id
 * 
 * @param user user id
 * @return gpgme_key_t Returns NULL if an error occured, otherwise, returns the key struct
 */
gpgme_key_t Encryptor::getKey(const std::string & user)
{
    if(gpgme_op_keylist_start(*_context, user.c_str(), 1))
        return NULL;

    gpgme_key_t userKey = (gpgme_key_t) malloc(sizeof(struct _gpgme_key));
    return gpgme_op_keylist_next (*_context, &userKey) ? NULL : userKey;
}