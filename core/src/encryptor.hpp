#ifndef __ENCRYPTOR_HPP__
#define __ENCRYPTOR_HPP__

#include <gpgme.h>
#include <string>

class Encryptor {
    public:
        Encryptor(void);
        ~Encryptor(void);

        bool encrypt(const std::string & in, std::string & out, const std::string & keyId);
        bool decrypt(std::string in, std::string & out);
        bool decryptFile(const std::string & filename, std::string & out);

        bool createKey(const std::string & user, int flag = GPGME_CREATE_NOEXPIRE);
        bool createKeyNoPassword(const std::string & user);

        bool deleteKey(const std::string & user);
    private:
        gpgme_ctx_t * _context;

        inline gpgme_key_t getKey(const std::string & user);

        std::string dataStructToString(gpgme_data_t data);
        bool decryptData(gpgme_data_t ciphered, std::string & out);
};

#endif