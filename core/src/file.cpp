#include "file.hpp"
#include <fstream>
#include <cstdio>
#include "encryptor.hpp"

/**
 * @brief Construct a new File:: File object
 * 
 */
File::File(void)
{
}

/**
 * @brief Construct a new File:: File object
 * 
 * @param name 
 */
File::File(const std::string &name)
{
    setFilename(name);
}

/**
 * @brief Destroy the File:: File object
 * 
 */
File::~File(void)
{
}

/**
 * @brief Set file name
 * 
 * @param name 
 */
void File::setFilename(std::string name)
{
    _filename = name;
}

/**
 * @brief Get the name of the file
 * 
 * @return std::string 
 */
std::string File::filename(void)
{
    return _filename;
}

/**
 * @brief Write the content to the file
 * 
 * @param content 
 * @return true 
 * @return false 
 */
bool File::write(std::string content)
{
    std::ofstream file(_filename);
    file << content;
    file.close();
    return true;
}

/**
 * @brief Read the content of the file
 * 
 * @return std::string 
 */
std::string File::read(void)
{
    std::string allContent;

    // In case of encrypted file
    if(_filename.find(".gpg") != std::string::npos)
    {
        Encryptor encryptor;
        encryptor.decryptFile(_filename, allContent);
        return allContent;
    }

    std::string line;
    std::ifstream file(_filename);
    while (getline(file, line))
    {
        if(!allContent.empty()) allContent += "\n";
        allContent += line;
    }
    return allContent;
}

/**
 * @brief Check if the file pointed by the provided path exists
 * 
 * @param path 
 * @return true 
 * @return false 
 */
bool File::exist(std::string path) {
    if(path.empty())
        return false;
        
    std::ifstream f(path);
    return f.good();
}

/**
 * @brief Remove the file pointed by the provided path
 * 
 * @param path 
 */
void File::remove(std::string path) {
    std::remove(path.c_str());
}