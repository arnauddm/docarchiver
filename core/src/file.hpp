#ifndef __FILE_HPP__
#define __FILE_HPP__

#include <string>
#include <fstream>
#include <list>

class File {
    private:
        std::string _filename;

    public:
        File(void);
        File(const std::string & name);
        virtual ~File(void);

        void setFilename(std::string name);
        std::string filename(void);

        bool write(std::string content);
        std::string read(void);

        static bool exist(std::string path);
        static void remove(std::string path);

        virtual std::list<std::string> parse(void) = 0;
};

#endif