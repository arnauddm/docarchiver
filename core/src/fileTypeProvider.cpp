#include "fileTypeProvider.hpp"

#include "textFile.hpp"
#include "pdfFile.hpp"

#include "context.hpp"

#include <map>

/**
 * @brief Get the File object according to the extension of the given file
 * 
 * @param input Filename of the file
 */
File * FileTypeProvider::file(const std::string & filename)
{
    std::map<std::string, std::string> fileTypesMapping = Context::getInstance()->configuration()->fileTypesMapping();
    std::map<std::string, std::string>::iterator it = fileTypesMapping.find(filename.substr(filename.find_last_of(".")+1));
    
    if(it == fileTypesMapping.end())
    {
        fileTypesMapping = defaultTypes();
        it = fileTypesMapping.find(filename.substr(filename.find_last_of(".")+1));
    }

    const std::string type = it->second;

    if(type == "Text")
        return new TextFile(filename);

    if(type == "Pdf")
        return new PdfFile(filename);

    return nullptr;
}

/**
 * @brief Returns the default "configuration"
 * 
 * @return std::map<std::string, std::string> Default configuration 
 */
std::map<std::string, std::string> FileTypeProvider::defaultTypes(void)
{
    std::map<std::string, std::string> defaultTypes = {
        {
            "txt",
            "Text"
        },
        {
            "pdf",
            "Pdf"
        }
    };

    return defaultTypes;
}