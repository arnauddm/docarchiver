#ifndef __FILE_TYPE_PROVIDER_HPP__
#define __FILE_TYPE_PROVIDER_HPP__

#include "file.hpp"
#include <map>

class FileTypeProvider
{
    public:
        static File * file(const std::string & filename);

    private:
        static std::map<std::string, std::string> defaultTypes(void);
};

#endif