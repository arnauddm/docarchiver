#include "folderScanner.hpp"

#include <filesystem>
#include <iostream>

/**
 * @brief Construct a new Folder Scanner:: Folder Scanner object
 * 
 * @param currentFolder 
 */
FolderScanner::FolderScanner(const std::string & currentFolder) : _currentFolder(currentFolder)
{
}

/**
 * @brief Destroy the Folder Scanner:: Folder Scanner object
 * 
 */
FolderScanner::~FolderScanner(void)
{
}

/**
 * @brief Returns a list of filepath which are recursively found into the given directory (via constructor)
 * 
 * @return std::list<std::string> 
 */
std::list<std::string> FolderScanner::scan(void)
{
    std::list<std::string> allFiles;

    for(const auto& p : std::filesystem::recursive_directory_iterator(_currentFolder)) {
        if (!std::filesystem::is_directory(p)) {
            allFiles.push_back(p.path());
        }
    }

    return allFiles;
}