#ifndef __FOLDER_SCANNER_HPP__
#define __FOLDER_SCANNER_HPP__

#include "scanner.hpp"

class FolderScanner : public Scanner {
    public:
        FolderScanner(const std::string & currentFolder);
        ~FolderScanner(void);

        std::list<std::string> scan(void);

    private:
        std::string _currentFolder;
};

#endif