#include "jsonArchive.hpp"
#include <fstream>
#include <sstream>
#include <iostream>
#include <memory>
#include <json/writer.h>
#include "textFile.hpp"
#include "context.hpp"
#include "encryptor.hpp"

/**
 * @brief Construct a new Json Archive and automatically read the archive file
 * 
 */
JsonArchive::JsonArchive(const std::string & archivePath)
{
    setArchivePath(archivePath);
    read();
}

/**
 * @brief Destroy the Json Archive:: Json Archive object
 * 
 */
JsonArchive::~JsonArchive(void)
{
}

/**
 * @brief Read and parse the json archive
 * 
 * @return true : parsed successfully
 * @return false : an error occured
 */
bool JsonArchive::read(void)
{
    reset();

    const std::string entirePath = archivePath();
    std::string content;

    TextFile file(entirePath);
    if((content = file.read()) == "")
        return true;

    std::stringstream stream;
    stream << content;

    Json::Value root;

    std::string errors;
    Json::CharReaderBuilder builder;

    if (!Json::parseFromStream(builder, stream, &root, &errors)) {
        std::cerr << errors << std::endl;
        return false;
    }

    const Json::Value jsonFiles = root["files"];
    for (unsigned int i = 0; i < jsonFiles.size(); i++)
    {
        Json::Value jsonFile = jsonFiles[i];

        ArchivedFile *f = new ArchivedFile();
        // f->setName(jsonFile.get("name", "").asString());
        f->setPath(jsonFile.get("path", "").asString());

        Json::Value personnalKeywords = jsonFile["personnalKeywords"];
        for (int i = 0; i < personnalKeywords.size(); i++)
        {
            f->addKeyword(personnalKeywords[i].asString());
        }

        addFile(f);
    }

    return true;
}

/**
 * @brief Save the archive in json format
 * 
 * @return true : successfully saved
 * @return false : an error occured
 */
bool JsonArchive::save(void)
{
    return saveToFile(serialize());
}

/**
 * @brief Returns the content of the archive
 * 
 * @return std::string Content of the file
 */
std::string JsonArchive::content(void)
{
    return TextFile(archivePath()).read();
}

/**
 * @brief Encrypt and save the archive
 * 
 * @param userKey GPG key of the user
 * @return true in case of success
 * @return false in case of failure
 */
bool JsonArchive::saveWithEncryption(const std::string & userKey)
{
    std::string out;

    Encryptor encryptor;
    encryptor.encrypt(serialize(), out, userKey);

    return saveToFile(out, true);
}

/**
 * @brief Save the the provided content
 * 
 * @param content Content to save
 * @param encrypt encrypt or not the content
 * @return true in case of success
 * @return false in case of failure
 */
bool JsonArchive::saveToFile(const std::string & content, bool encrypt /* = false */)
{
    setArchivePath(verifiedFilename(encrypt));
    std::ofstream outfile(archivePath());
    outfile << content;
    return true;
}

/**
 * @brief Serialize the archive
 * 
 * @return std::string Archive to the json format
 */
std::string JsonArchive::serialize(void)
{
    Json::Value root;
    
    Json::Value jsonFiles(Json::arrayValue);

    std::map<std::string, ArchivedFile *>::iterator it;
    std::map<std::string, ArchivedFile *> files = allFiles();
    int i;
    for (i = 0, it = files.begin(); it != files.end(); it++, i++)
    {
        ArchivedFile *f = it->second;

        Json::Value jsonFile;
        jsonFile["name"] = f->name();
        jsonFile["path"] = f->path();

        Json::Value jsonPersonnalKeywords = Json::arrayValue;
        std::map<std::string, int> personnalKeywords = f->personnalKeywords();
        for (std::map<std::string, int>::iterator k = personnalKeywords.begin(); k != personnalKeywords.end(); k++)
        {
            for (int i = 0; i < k->second; i++)
            {
                jsonPersonnalKeywords.append(k->first);
            }
        }

        jsonFile["personnalKeywords"] = jsonPersonnalKeywords;
        jsonFiles[i] = jsonFile;
    }

    root["files"] = jsonFiles;

    Json::StreamWriterBuilder builder;
    std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());

    std::stringstream stream;

    return writer->write(root, &stream) == 0 ? stream.str() : "";
}

/**
 * @brief Check if the filename is correct (in case of encrypted file)
 * 
 * @param encrypt Encrypted file
 * @return Correct filename
 */
std::string JsonArchive::verifiedFilename(bool encrypt /* = false */)
{
    std::string filename = archivePath();
    
    // Check that the 'gpg' extension is present
    if(encrypt)
    {
        if(filename.find(".gpg") == std::string::npos)
            filename += ".gpg";
    }
    else
    {
        int pos;
        if((pos = filename.find(".gpg")) != std::string::npos)
            filename.replace(pos, std::string(".gpg").length(), "");
    }

    return filename;
}