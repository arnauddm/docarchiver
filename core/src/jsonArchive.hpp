#ifndef __JSON_ARCHIVE_HPP__
#define __JSON_ARCHIVE_HPP__

#include "archive.hpp"
#include <json/json.h>

class JsonArchive : public Archive {
    public:
        JsonArchive(const std::string & archivePath);
        ~JsonArchive(void);

        bool read(void) override;
        bool save(void) override;
        bool saveWithEncryption(const std::string & userKey) override;
        std::string content(void);

        std::string serialize(void) override;

    private:
        bool saveToFile(const std::string & content, bool encrypt = false);
        std::string verifiedFilename(bool encrypt = false);
};

#endif