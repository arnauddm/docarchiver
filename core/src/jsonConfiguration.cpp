#include "jsonConfiguration.hpp"
#include "textFile.hpp"
#include <json/writer.h>
#include <json/json.h>
#include <iostream>

/**
 * @brief Construct a new Json Configuration:: Json Configuration object
 * 
 */
JsonConfiguration::JsonConfiguration(void)
{
    readConfiguration();
}

/**
 * @brief Destroy the Json Configuration:: Json Configuration object
 * 
 */
JsonConfiguration::~JsonConfiguration(void) {

}

/**
 * @brief Read the configuration file and set all fields
 * 
 */
void JsonConfiguration::readConfiguration(void) {
    {
        TextFile file(defaultFileName);
        if(file.read() == "")
            return setDefaultConfiguration();
    }
    
    std::ifstream archiveDoc(defaultFileName, std::ifstream::binary);
    if(!archiveDoc.good()) {
        return setDefaultConfiguration();
    }

    Json::Value root;
    Json::CharReaderBuilder rbuilder;

    std::string errors;
    if (!Json::parseFromStream(rbuilder, archiveDoc, &root, &errors)) {
        std::cerr << errors << std::endl;
        return setDefaultConfiguration();
    }

    setArchiveDirectory(root.get("archiveDirectory", "").asString());

    //const Json::Value fileTypesMapping = root.get("fileTypes", "");
    const Json::Value fileTypesMapping = root["fileTypes"];

    for(Json::Value::const_iterator it = fileTypesMapping.begin(); it != fileTypesMapping.end(); it++)
    {
        std::string key = it.key().asString();
        std::string name = it.name();
        addFileTypeMapping(it.name(), fileTypesMapping[it.name()].asString());
    }
}