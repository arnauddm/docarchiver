#ifndef __JSON_CONFIGURATION_HPP__
#define __JSON_CONFIGURATION_HPP__

#include "configuration.hpp"

class JsonConfiguration : public Configuration {
public:
    JsonConfiguration(void);
    ~JsonConfiguration(void);

private:
    const std::string defaultFileName = "configuration.json";

    void readConfiguration(void);
};

#endif