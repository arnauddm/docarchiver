#ifndef __PARSER_HPP__
#define __PARSER_HPP__

#include <string>
#include <list>

class Parser {
    public:
        Parser(void);
        virtual ~Parser(void);

        virtual std::list<std::string> parse(std::string content, bool enableRobustness = true) = 0;
};

#endif