#include <podofo/podofo.h>

#include "pdfFile.hpp"

/**
 * @brief Construct a new Pdf File:: Pdf File object
 * 
 * @param filename 
 */
PdfFile::PdfFile(const std::string &filename) : File(filename)
{
    _parser = new PdfParser();
}

/**
 * @brief Destroy the Pdf File:: Pdf File object
 * 
 */
PdfFile::~PdfFile(void)
{
}

/**
 * @brief Parse the PDF file to extract all keywords
 * 
 * @return std::list<std::string> 
 */
std::list<std::string> PdfFile::parse(void)
{
    if (!File::exist(filename()))
        return std::list<std::string>();

    std::string content = getContent();
    return parse(content);
}

/**
 * @brief Parse the content string
 * 
 * @param content : content to parse
 * @param enableRobustness : if true, each keywords that were found will be parsed one more time to remove specials chars, etc...
 * 
 */
std::list<std::string> PdfFile::parse(std::string content, bool enableRobustness /* = true */)
{
    return _parser->parse(_parser->replaceIllegalChar(content));
}

/**
 * @brief Returns the content of the PDF file
 * 
 * @return std::string 
 */
std::string PdfFile::getContent(void)
{
    std::string content = "";
    PoDoFo::PdfMemDocument pdf(filename().c_str());
    for (int pn = 0; pn < pdf.GetPageCount(); ++pn) 
    {
        PoDoFo::PdfPage* page = pdf.GetPage(pn);
        PoDoFo::PdfContentsTokenizer tok(page);
        const char* token = NULL;
        PoDoFo::PdfVariant var;
        PoDoFo::EPdfContentsType type;
        std::string previousStr = "";
        while (tok.ReadNext(type, token, var)) 
        {
            if (type == PoDoFo::ePdfContentsType_Keyword)
            {
                // process type, token & var
                if (var.IsArray()) 
                {
                    PoDoFo::PdfArray& a = var.GetArray();
                    std::string str = "";
                    for (size_t i = 0; i < a.GetSize(); i++)
                    {
                        if (a[i].IsString())
                        {
                            str += a[i].GetString().GetStringUtf8();
                        }
                    }
                    if(str != previousStr)
                        content += str;
                    previousStr = str;
                }
            }
        }
    }

    return content;
}