#ifndef __PDF_FILE_HPP__
#define __PDF_FILE_HPP__

#include "file.hpp"
#include "pdfParser.hpp"

class PdfFile : public File {
    public:
        PdfFile(const std::string & filename);
        ~PdfFile(void);

        std::list<std::string> parse(void);
        std::list<std::string> parse(std::string content, bool enableRobustness = true);

    private:
        PdfParser * _parser;

        std::string getContent(void);
};

#endif