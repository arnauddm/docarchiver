#ifndef __SCANNER_HPP__
#define __SCANNER_HPP__

#include <string>
#include <list>

class Scanner {
    public:
        virtual std::list<std::string> scan(void) = 0;
};

#endif // __SCANNER_HPP__