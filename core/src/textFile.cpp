#include "textFile.hpp"
#include "textParser.hpp"

/**
 * @brief Construct a new Text File with tis filename
 * 
 * @param filename 
 */
TextFile::TextFile(const std::string & filename) : File(filename) {
    _parser = new TextParser();
}

/**
 * @brief Destroy the Text File:: Text File object
 * 
 */
TextFile::~TextFile(void) {
    if(_parser) delete _parser;
}

/**
 * @brief Parse the text file
 * 
 * @return std::list<std::string> 
 */
std::list<std::string> TextFile::parse(void) {
    return parse(read());
}

/**
 * @brief Parse the the text content passed in parameter
 * 
 * @param content : content
 * @param enableRobustness : enable (or not) the robustness, if enabled, each keyword will be parsed a second time to removed unwanted characters
 * 
 * @return std::list<std::string> : list of keywords
 * 
 */
std::list<std::string> TextFile::parse(std::string content, bool enableRobustness /* = true */) {
    return _parser->parse(content);
}