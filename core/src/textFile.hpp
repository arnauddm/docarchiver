#ifndef __TEXT_FILE_HPP__
#define __TEXT_FILE_HPP__

#include "file.hpp"
#include "textParser.hpp"

class TextFile : public File {
    public:
        TextFile(const std::string & filename);
        ~TextFile(void);

        std::list<std::string> parse(void);
        std::list<std::string> parse(std::string content, bool enableRobustness = true);

    private:
        TextParser * _parser;
};

#endif