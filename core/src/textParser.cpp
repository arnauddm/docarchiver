#include "textParser.hpp"

/**
 * @brief Construct a new Text Parser:: Text Parser object
 * 
 */
TextParser::TextParser(void)
{
}

/**
 * @brief Destroy the Text Parser:: Text Parser object
 * 
 */
TextParser::~TextParser(void)
{
}

/**
 * @brief Parse the content
 * 
 * @param content : text content to parse
 * @param enableRobustness : enable (or not) the robustness, if enabled, it will parse a second time each keyword to remove unwanted characters
 * 
 */
std::list<std::string> TextParser::parse(std::string content, bool enableRobustness /* = true */)
{
    std::list<std::string> keywords;
    int currentPos = 0;
    int nextPos = 0;
    do
    {
        nextPos = content.find(" ", currentPos);
        
        // Robustness
        const std::string tmpContent = content.substr(currentPos, nextPos - currentPos);
        const std::string keyword = enableRobustness ? replaceIllegalChar(tmpContent) : tmpContent;
        if(keyword.size() != 0) keywords.push_back(keyword);
        
        currentPos = nextPos + 1;
    } while (nextPos != std::string::npos);

    return keywords;
}

/**
 * @brief Replace all unwanted characters
 * 
 * @param content 
 * @return std::string 
 */
std::string TextParser::replaceIllegalChar(std::string content)
{
    return replaceChars(
        replaceSpecialChar(content),
        illegalChar()
    );
}

/**
 * @brief Replace all special characters
 * 
 * @param content 
 * @return std::string 
 */
std::string TextParser::replaceSpecialChar(std::string content)
{
    return replaceChars(content, specialChar());
}

/**
 * @brief Remove all unwanted characters given in parameter (through a @ref std::list)
 * 
 * @param content 
 * @param chars 
 * @return std::string 
 */
std::string TextParser::replaceChars(std::string content, const std::list<std::string> chars)
{
    //for (unsigned int i = 0; i < sizeof(chars) / sizeof(chars[0]); i++)
    for(std::list<std::string>::const_iterator it = chars.begin(); it != chars.end(); it++)
    {
        const std::string pattern = *it;
        while (content.find(pattern) != std::string::npos)
        {
            content.replace(content.find(pattern), 1, "");
        }
    }

    return content;
}