#ifndef __TEXT_PARSER_HPP__
#define __TEXT_PARSER_HPP__

#include "parser.hpp"

class TextParser : public Parser
{
public:
    static const std::list<std::string> illegalChar() {
        return {
            ",",
            ";",
            ".",
            "?",
            "!",
            "(",
            ")",
            "[",
            "]"
        };
    };

    static const std::list<std::string> specialChar() {
        return {
             "\\",
            "\n",
            "\t"
        };
    };

    TextParser(void);
    ~TextParser(void);

    std::list<std::string> parse(std::string content, bool enableRobustness = true);

    static std::string replaceIllegalChar(std::string content);
    static std::string replaceSpecialChar(std::string content);

private:
    static std::string replaceChars(std::string content, std::list<std::string> chars);
};

#endif