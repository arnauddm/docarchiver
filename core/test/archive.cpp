#include "archive.hpp"
#include "test.hpp"
#include "archiveFactory.hpp"
#include "file.hpp"

TEST(Archive, ExistantKeyWord) {
    ArchivedFile file;
    file.setPath("test/path/myName.txt");
    file.addKeyword("key");

    Archive * archive = ArchiveFactory::get();
    archive->addFile(&file);

    std::map<std::string, ArchivedFile *> files = archive->findByKeyword("key");
    EXPECT_EQ(files.size(), 1);
    std::map<std::string, ArchivedFile *>::iterator it = files.begin();
    EXPECT_EQ((it->second)->name(), "myName.txt");
    
    delete archive;
}

TEST(Archive, InexistantKeyWord) {
    ArchivedFile file;
    file.setPath("test/path/myName.txt");
    file.addKeyword("key");

    Archive * archive = ArchiveFactory::get();
    archive->addFile(&file);

    std::map<std::string, ArchivedFile *> files = archive->findByKeyword("keyUnknown");
    EXPECT_EQ(files.size(), 0);
    delete archive;
}

TEST(Archive, RemoveExistentFileWithName) {
    ArchivedFile file;
    file.setPath("test/path/myName.txt");
    file.addKeyword("key");

    Archive * archive = ArchiveFactory::get();
    archive->addFile(&file);

    EXPECT_EQ(archive->removeFileWithName("myName.txt"), true);
}

TEST(Archive, RemoveNonExistentFileWithName) {
    ArchivedFile file;
    file.setPath("test/path/myName.txt");
    file.addKeyword("key");

    Archive * archive = ArchiveFactory::get();
    archive->addFile(&file);

    EXPECT_EQ(archive->removeFileWithName("myNameNonExistent"), false);
}

TEST(Archive, RemoveExistentFileWithPath) {
    ArchivedFile file;
    file.setPath("test/path/myName.txt");
    file.addKeyword("key");

    Archive * archive = ArchiveFactory::get();
    archive->addFile(&file);

    EXPECT_EQ(archive->removeFileWithPath("test/path/myName.txt"), true);
}

TEST(Archive, RemoveNonExistentFileWithPath) {
    ArchivedFile file;
    file.setPath("test/path/myName.txt");
    file.addKeyword("key");

    Archive * archive = ArchiveFactory::get();
    archive->addFile(&file);

    EXPECT_EQ(archive->removeFileWithName("test/path/unknown.tx"), false);
}

TEST(Archive, GetExistingFile) {
    const std::string filename = "test/path/file.txt";

    ArchivedFile file;
    file.setPath(filename);

    Archive * archive = ArchiveFactory::get();
    archive->addFile(&file);

    EXPECT_NE(archive->archivedFile(filename), nullptr);
}

TEST(Archive, GetNonExistingFile) {
    const std::string filename = "test/path/file.txt";

    ArchivedFile file;
    file.setPath(filename);

    Archive * archive = ArchiveFactory::get();
    archive->addFile(&file);

    EXPECT_EQ(archive->archivedFile("blablabla"), nullptr);
}