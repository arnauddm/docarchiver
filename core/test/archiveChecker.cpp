#include "archiveChecker.hpp"
#include "archiveFactory.hpp"
#include "test.hpp"
#include "tools.hpp"

TEST(ArchiveChecker, NoFileInArchive) {
    Archive * archive = ArchiveFactory::newOne("archive.json");

    ArchiveChecker checker;
    checker.setArchive(archive);

    EXPECT_EQ(checker.check().size(), 0);
    
    delete archive;
}

TEST(ArchiveChecker, AllFilesExist) {
    Archive * archive = ArchiveFactory::newOne("archive.json");
    ArchiveChecker checker;

    ArchivedFile * archived = new ArchivedFile();
    archived->setPath(currentSourceFileDirectory(__FILE__) + "/files/microsoftWord.pdf");

    archive->addFile(archived);

    checker.setArchive(archive);
    
    ArchiveChecker::Errors errors = checker.check();

    EXPECT_EQ(errors.size(), 0);
}

TEST(ArchiveChecker, FileNotExist) {
    Archive * archive = ArchiveFactory::newOne("archive.json");
    ArchiveChecker checker;

    ArchivedFile * archived = new ArchivedFile();
    archived->setPath("/blabla/file.txt");

    archive->addFile(archived);

    checker.setArchive(archive);
    ArchiveChecker::Errors errors = checker.check();

    EXPECT_EQ(errors.size(), 1);
    EXPECT_EQ(*(errors.begin()), "/blabla/file.txt");
}