#include "test.hpp"
#include "archiveFactory.hpp"
#include "file.hpp"
#include "tools.hpp"
#include "context.hpp"

TEST(ArchiveFactory, Default) {
    EXPECT_EQ(ArchiveFactory::get()->archivePath(), Context::getInstance()->launchedFrom() + "/archive.json");
}

TEST(ArchiveFactory, SetAndGet) {
    ArchiveFactory::setArchivePath("myArchive.txt");
    EXPECT_EQ(ArchiveFactory::get()->archivePath(), "myArchive.txt");
}

TEST(ArchiveFactory, NewArchive) {
   ArchiveFactory::setArchivePath("archive.json");
   Archive * archive1 = ArchiveFactory::get();
   EXPECT_EQ(archive1->save(), true);
   EXPECT_NE(archive1, nullptr);

   EXPECT_EQ(File::exist("archive.json"), true);

   Archive * archive2 = ArchiveFactory::newOne("newArchive.json");
   EXPECT_EQ(archive2->save(), true);
   EXPECT_EQ(File::exist("newArchive.json"), true);

   delete archive1;
   delete archive2;

   // Reset archive path
   ArchiveFactory::setArchivePath("archive.json");
}