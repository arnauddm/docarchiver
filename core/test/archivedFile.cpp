#include "archivedFile.hpp"
#include "test.hpp"
#include "file.hpp"
#include "fileTypeProvider.hpp"
#include "tools.hpp"

class FileTest : public File
{
public:
    FileTest(std::string filename) : File(filename)
    {
    }

    ~FileTest(void)
    {
    }

    std::list<std::string> parse(void)
    {
        return std::list<std::string>();
    }
};

TEST(ArchivedFile, CopyConstructor)
{
    ArchivedFile file1("file1.txt");
    file1.addKeyword("k1");
    file1.addKeyword("k2");
    file1.addPersonnalKeyword("pk1");

    ArchivedFile file2(&file1);
    
    EXPECT_EQ(file1.name(), file2.name());
    EXPECT_EQ(file1.keywords().size(), file1.keywords().size());
    EXPECT_EQ(file1.findKeyword("k1"), file2.findKeyword("k1"));
    EXPECT_EQ(file1.findKeyword("k2"), file2.findKeyword("k2"));
    EXPECT_EQ(file1.findKeyword("pk1"), file2.findKeyword("pk1"));
}

TEST(ArchivedFile, FileExtension)
{
    {
        ArchivedFile file1("file.txt");
        EXPECT_EQ(file1.extension(), "txt");
    }
    {
        ArchivedFile file1("file.trol");
        EXPECT_EQ(file1.extension(), "trol");
    }
    {
        ArchivedFile file1("/home/lol/file.panda");
        EXPECT_EQ(file1.extension(), "panda");
    }
    {
        ArchivedFile file1("file.elephant");
        EXPECT_EQ(file1.extension(), "elephant");
    }
}


TEST(ArchivedFile, FileName) {
    {
        ArchivedFile file("myName.txt");
        EXPECT_EQ(file.name(), "myName.txt");
    }
    {
        ArchivedFile file("path/myName.txt");
        EXPECT_EQ(file.name(), "myName.txt");
    }
    {
        ArchivedFile file("/home/test/myName.txt");
        EXPECT_EQ(file.name(), "myName.txt");
    }
    {
        ArchivedFile file("myName.trololo");
        EXPECT_EQ(file.name(), "myName.trololo");
    }
}

TEST(ArchivedFile, SetAndGetPath) {
    ArchivedFile file;
    file.setPath("./myFile");

    EXPECT_EQ(file.path(), "./myFile");
}

TEST(ArchivedFile, AutoKeywords) {
    ArchivedFile file;
    file.addKeyword("existant");

    EXPECT_EQ(file.findKeyword("existant"), 1);
    EXPECT_EQ(file.findKeyword("nonexistant"), 0);
}

TEST(ArchivedFile, PersonnalKeywords) {
    ArchivedFile file;
    file.addPersonnalKeyword("existant");

    EXPECT_EQ(file.findKeyword("existant"), 1);
    EXPECT_EQ(file.findKeyword("nonexistant"), 0);
}

TEST(ArchivedFile, AutoAndPersonnalKeywords) {
    ArchivedFile file;
    file.addKeyword("existant");
    file.addPersonnalKeyword("existant");

    EXPECT_EQ(file.findKeyword("existant"), 2);
    EXPECT_EQ(file.findKeyword("nonexistant"), 0);
}

TEST(ArchivedFile, AutoKeywordsList) {
    std::list<std::string> keywords;
    keywords.push_back("key1");
    keywords.push_back("key2");
    keywords.push_back("key3");

    ArchivedFile file;
    file.addKeywords(keywords);

    EXPECT_EQ(file.findKeyword("key1"), 1);
    EXPECT_EQ(file.findKeyword("key2"), 1);
    EXPECT_EQ(file.findKeyword("key3"), 1);
    EXPECT_EQ(file.findKeyword("key4"), 0);
}

TEST(ArchivedFile, InitFromFilePath) {
    FileTest * file = new FileTest("test.txt");
    file->write("key1");
    delete file;

    File * file2 = FileTypeProvider::file("test.txt");
    std::list<std::string> keywords = file2->parse();
    ArchivedFile archivedFile("test.txt");
    archivedFile.addKeywords(keywords);

    EXPECT_EQ(archivedFile.findKeyword("key1"), 1);
    
    remove("test.txt");
}

TEST(ArchivedFile, AddKeywordIfNeeded)
{
    ArchivedFile file(currentSourceFileDirectory(__FILE__) + "/files/parenthesis.pdf");
    EXPECT_NE(file.keywords().size(), 0);
}