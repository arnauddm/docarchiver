#include "test.hpp"
#include "context.hpp"

TEST(Context, launchedFrom) {
    EXPECT_NE(Context::getInstance(), nullptr);
    EXPECT_TRUE(Context::getInstance()->launchedFrom().find("/docarchiver/build") != std::string::npos);
}