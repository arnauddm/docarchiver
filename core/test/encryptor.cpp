#include "encryptor.hpp"
#include "test.hpp"
#include "tools.hpp"
#include "file.hpp"

class FileTest : public File
{
    public:
        FileTest(const std::string & filename) : File(filename) {}
        ~FileTest(void) {}
        std::list<std::string> parse(void) { return std::list<std::string>(); }
        
};

TEST(Encryptor, CreateAndDeleteKey)
{
    Encryptor encryptor;

    EXPECT_EQ(encryptor.createKeyNoPassword("test_key@test.com"), true);
    EXPECT_EQ(encryptor.deleteKey("test_key@test.com"), true);
}

TEST(Encryptor, Encrypt)
{
    Encryptor encryptor;

    std::string userKey = "test_key@test.com";
    std::string in = "Hello World";
    std::string out;

    EXPECT_EQ(encryptor.createKeyNoPassword(userKey), true);

    // By saving the result to test it later, we avoid "non-deleted" user key in case of failure
    bool resEncryt = encryptor.encrypt(in, out, userKey);

    EXPECT_EQ(encryptor.deleteKey(userKey), true);

    EXPECT_EQ(resEncryt, true);
    EXPECT_NE(out, "");
    EXPECT_NE(out, in);
}

TEST(Encryptor, EncryptDecrypt)
{
    Encryptor encryptor;

    std::string userKey = "test_key@test.com";
    std::string plain = "Hello World";
    std::string ciphered;

    EXPECT_EQ(encryptor.createKeyNoPassword(userKey), true);

    // By saving the result to test it later, we avoid "non-deleted" user key in case of failure
    bool resEncrypt = encryptor.encrypt(plain, ciphered, userKey);
    std::string clear;
    bool resDecrypt = encryptor.decrypt(ciphered, clear);

    EXPECT_EQ(encryptor.deleteKey(userKey), true);
    EXPECT_EQ(resEncrypt, true);
    EXPECT_NE(ciphered, plain);
    EXPECT_EQ(clear, plain);
}

TEST(Encryptor, EncryptDecryptJson)
{
    Encryptor encryptor;

    std::string userKey = "test_key@test.com";
    std::string plain = "{\"files\" : [{\"name\" : \"myName\",\"path\" : \"myPath\",\"personnalKeywords\" : []}]}";;
    std::string ciphered;

    EXPECT_EQ(encryptor.createKeyNoPassword(userKey), true);

    // By saving the result to test it later, we avoid "non-deleted" user key in case of failure
    bool resEncrypt = encryptor.encrypt(plain, ciphered, userKey);
    std::string clear;
    bool resDecrypt = encryptor.decrypt(ciphered, clear);

    EXPECT_EQ(encryptor.deleteKey(userKey), true);
    EXPECT_EQ(resEncrypt, true);
    EXPECT_NE(ciphered, plain);
    EXPECT_NE(ciphered, "");
    EXPECT_EQ(clear, plain);
}

TEST(Encryptor, EncryptDecryptLongText)
{
    Encryptor encryptor;

    std::string userKey = "test_key@test.com";
    std::string plain =
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        "Ullamcorper morbi tincidunt ornare massa."
        "Phasellus vestibulum lorem sed risus ultricies tristique nulla aliquet enim."
        "Rutrum quisque non tellus orci ac."
        "Nisi vitae suscipit tellus mauris a diam maecenas sed enim. Aenean et tortor at risus."
        "Dis parturient montes nascetur ridiculus mus mauris vitae."
        "Nec feugiat nisl pretium fusce id velit ut."
        "Facilisi nullam vehicula ipsum a arcu cursus vitae congue mauris."
        "Ut porttitor leo a diam sollicitudin tempor id."
        "Pellentesque elit ullamcorper dignissim cras tincidunt."
        "Enim facilisis gravida neque convallis a."
        "Dui id ornare arcu odio ut sem nulla."
        "Pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum."
        "Massa ultricies mi quis hendrerit dolor magna."
        "Ut tortor pretium viverra suspendisse potenti nullam."
        "Netus et malesuada fames ac turpis egestas maecenas pharetra."
        "Imperdiet proin fermentum leo vel orci porta non pulvinar neque."
        "Integer eget aliquet nibh praesent."
        "Scelerisque eleifend donec pretium vulputate sapien nec sagittis aliquam.";
    std::string ciphered;

    EXPECT_EQ(encryptor.createKeyNoPassword(userKey), true);

    // By saving the result to test it later, we avoid "non-deleted" user key in case of failure
    bool resEncrypt = encryptor.encrypt(plain, ciphered, userKey);
    std::string clear;
    bool resDecrypt = encryptor.decrypt(ciphered, clear);

    EXPECT_EQ(encryptor.deleteKey(userKey), true);
    EXPECT_EQ(resEncrypt, true);
    EXPECT_NE(ciphered, plain);
    EXPECT_NE(ciphered, "");
    EXPECT_EQ(clear, plain);
}

TEST(Encryptor, EncryptDecryptLongJson)
{
    Encryptor encryptor;

    std::string userKey = "test_key@test.com";
    std::string plain = 
    "["
    "{"
    "    \"_id\": \"60605600b40175d46f323430\","
    "    \"index\": 0,"
    "    \"guid\": \"c373d804-9c37-4384-b1af-873d9f9364d2\","
    "    \"tags\": ["
    "    \"cupidatat\","
    "    \"officia\","
    "    \"velit\","
    "    \"duis\","
    "    \"incididunt\","
    "    \"ad\","
    "    \"tempor\""
    "    ],"
    "    \"friends\": ["
    "    {"
    "        \"id\": 0,"
    "        \"name\": \"Renee Hurst\""
    "    },"
    "    {"
    "        \"id\": 1,"
    "        \"name\": \"Cora Lamb\""
    "    },"
    "    {"
    "        \"id\": 2,"
    "        \"name\": \"Hill Boone\""
    "    }"
    "    ],"
    "    \"greeting\": \"Hello, undefined! You have 5 unread messages.\","
    "    \"favoriteFruit\": \"banana\""
    "},"
    "{"
    "    \"_id\": \"6060560099e2335816114cfe\","
    "    \"index\": 1,"
    "    \"guid\": \"16e8ebb2-8e26-4de1-bfeb-5de4bd26056d\","
    "    \"tags\": ["
    "    \"eiusmod\","
    "    \"culpa\","
    "    \"in\","
    "    \"eiusmod\","
    "    \"ipsum\","
    "    \"irure\","
    "    \"adipisicing\""
    "    ],"
    "    \"friends\": ["
    "    {"
    "        \"id\": 0,"
    "        \"name\": \"Barlow Strong\""
    "    },"
    "    {"
    "        \"id\": 1,"
    "        \"name\": \"Alexandria Cabrera\""
    "    },"
    "    {"
    "        \"id\": 2,"
    "        \"name\": \"Herrera Nicholson\""
    "    }"
    "    ],"
    "    \"greeting\": \"Hello, undefined! You have 6 unread messages.\","
    "    \"favoriteFruit\": \"apple\""
    "}"
    "]";

    std::string ciphered;

    EXPECT_EQ(encryptor.createKeyNoPassword(userKey), true);

    // By saving the result to test it later, we avoid "non-deleted" user key in case of failure
    bool resEncrypt = encryptor.encrypt(plain, ciphered, userKey);
    std::string clear;
    bool resDecrypt = encryptor.decrypt(ciphered, clear);

    EXPECT_EQ(encryptor.deleteKey(userKey), true);
    EXPECT_EQ(resEncrypt, true);
    EXPECT_NE(ciphered, plain);
    EXPECT_NE(ciphered, "");
    EXPECT_EQ(clear, plain);
}

TEST(Encryptor, DecryptFileText)
{
    const std::string filename = currentSourceFileDirectory(__FILE__) + "/test.txt.gpg";

    Encryptor encryptor;

    std::string userKey = "test_key@test.com";
    std::string plain = "Hello World";
    std::string ciphered;

    EXPECT_EQ(encryptor.createKeyNoPassword(userKey), true);

    // By saving the result to test it later, we avoid "non-deleted" user key in case of failure
    bool resEncrypt = encryptor.encrypt(plain, ciphered, userKey);
    std::string clear;

    FileTest file(filename);
    file.write(ciphered);

    bool resDecrypt = encryptor.decryptFile(filename, clear);

    EXPECT_EQ(encryptor.deleteKey(userKey), true);
    EXPECT_EQ(resEncrypt, true);
    EXPECT_NE(ciphered, plain);
    EXPECT_EQ(clear, plain);

    FileTest::remove(filename);
}

TEST(Encryptor, DecryptFileLongText)
{
    const std::string filename = currentSourceFileDirectory(__FILE__) + "/test.txt.gpg";

    Encryptor encryptor;

    std::string userKey = "test_key@test.com";
    std::string plain =
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        "Ullamcorper morbi tincidunt ornare massa."
        "Phasellus vestibulum lorem sed risus ultricies tristique nulla aliquet enim."
        "Rutrum quisque non tellus orci ac."
        "Nisi vitae suscipit tellus mauris a diam maecenas sed enim. Aenean et tortor at risus."
        "Dis parturient montes nascetur ridiculus mus mauris vitae."
        "Nec feugiat nisl pretium fusce id velit ut."
        "Facilisi nullam vehicula ipsum a arcu cursus vitae congue mauris."
        "Ut porttitor leo a diam sollicitudin tempor id."
        "Pellentesque elit ullamcorper dignissim cras tincidunt."
        "Enim facilisis gravida neque convallis a."
        "Dui id ornare arcu odio ut sem nulla."
        "Pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum."
        "Massa ultricies mi quis hendrerit dolor magna."
        "Ut tortor pretium viverra suspendisse potenti nullam."
        "Netus et malesuada fames ac turpis egestas maecenas pharetra."
        "Imperdiet proin fermentum leo vel orci porta non pulvinar neque."
        "Integer eget aliquet nibh praesent."
        "Scelerisque eleifend donec pretium vulputate sapien nec sagittis aliquam.";
    std::string ciphered;

    EXPECT_EQ(encryptor.createKeyNoPassword(userKey), true);

    // By saving the result to test it later, we avoid "non-deleted" user key in case of failure
    bool resEncrypt = encryptor.encrypt(plain, ciphered, userKey);
    std::string clear;

    FileTest file(filename);
    file.write(ciphered);

    bool resDecrypt = encryptor.decryptFile(filename, clear);

    EXPECT_EQ(encryptor.deleteKey(userKey), true);
    EXPECT_EQ(resEncrypt, true);
    EXPECT_NE(ciphered, plain);
    EXPECT_EQ(clear, plain);

    FileTest::remove(filename);
}

TEST(Encryptor, DecryptFileJson)
{
    const std::string filename = currentSourceFileDirectory(__FILE__) + "/test.txt.gpg";

    Encryptor encryptor;

    std::string userKey = "test_key@test.com";
    std::string plain = "{\"files\" : [{\"name\" : \"myName\",\"path\" : \"myPath\",\"personnalKeywords\" : []}]}";;
    std::string ciphered;

    EXPECT_EQ(encryptor.createKeyNoPassword(userKey), true);

    // By saving the result to test it later, we avoid "non-deleted" user key in case of failure
    bool resEncrypt = encryptor.encrypt(plain, ciphered, userKey);
    std::string clear;

    FileTest file(filename);
    file.write(ciphered);

    bool resDecrypt = encryptor.decryptFile(filename, clear);

    EXPECT_EQ(encryptor.deleteKey(userKey), true);
    EXPECT_EQ(resEncrypt, true);
    EXPECT_NE(ciphered, plain);
    EXPECT_EQ(clear, plain);

    FileTest::remove(filename);
}

TEST(Encryptor, DecryptFileLongJson)
{
    const std::string filename = currentSourceFileDirectory(__FILE__) + "/test.txt.gpg";

    Encryptor encryptor;

    std::string userKey = "test_key@test.com";
    std::string plain = 
        "["
        "{"
        "    \"_id\": \"60605600b40175d46f323430\","
        "    \"index\": 0,"
        "    \"guid\": \"c373d804-9c37-4384-b1af-873d9f9364d2\","
        "    \"tags\": ["
        "    \"cupidatat\","
        "    \"officia\","
        "    \"velit\","
        "    \"duis\","
        "    \"incididunt\","
        "    \"ad\","
        "    \"tempor\""
        "    ],"
        "    \"friends\": ["
        "    {"
        "        \"id\": 0,"
        "        \"name\": \"Renee Hurst\""
        "    },"
        "    {"
        "        \"id\": 1,"
        "        \"name\": \"Cora Lamb\""
        "    },"
        "    {"
        "        \"id\": 2,"
        "        \"name\": \"Hill Boone\""
        "    }"
        "    ],"
        "    \"greeting\": \"Hello, undefined! You have 5 unread messages.\","
        "    \"favoriteFruit\": \"banana\""
        "},"
        "{"
        "    \"_id\": \"6060560099e2335816114cfe\","
        "    \"index\": 1,"
        "    \"guid\": \"16e8ebb2-8e26-4de1-bfeb-5de4bd26056d\","
        "    \"tags\": ["
        "    \"eiusmod\","
        "    \"culpa\","
        "    \"in\","
        "    \"eiusmod\","
        "    \"ipsum\","
        "    \"irure\","
        "    \"adipisicing\""
        "    ],"
        "    \"friends\": ["
        "    {"
        "        \"id\": 0,"
        "        \"name\": \"Barlow Strong\""
        "    },"
        "    {"
        "        \"id\": 1,"
        "        \"name\": \"Alexandria Cabrera\""
        "    },"
        "    {"
        "        \"id\": 2,"
        "        \"name\": \"Herrera Nicholson\""
        "    }"
        "    ],"
        "    \"greeting\": \"Hello, undefined! You have 6 unread messages.\","
        "    \"favoriteFruit\": \"apple\""
        "}"
        "]";
    std::string ciphered;

    EXPECT_EQ(encryptor.createKeyNoPassword(userKey), true);

    // By saving the result to test it later, we avoid "non-deleted" user key in case of failure
    bool resEncrypt = encryptor.encrypt(plain, ciphered, userKey);
    std::string clear;

    FileTest file(filename);
    file.write(ciphered);

    bool resDecrypt = encryptor.decryptFile(filename, clear);

    EXPECT_EQ(encryptor.deleteKey(userKey), true);
    EXPECT_EQ(resEncrypt, true);
    EXPECT_NE(ciphered, plain);
    EXPECT_EQ(clear, plain);

    FileTest::remove(filename);
}

TEST(Encryptor, EncryptDecryptExternalKey)
{
    Encryptor * encryptor = new Encryptor();

    std::string userKey = "test_key@test.com";
    std::string plain = "Hello World";
    std::string ciphered;

    EXPECT_EQ(encryptor->createKeyNoPassword(userKey), true);

    // By saving the result to test it later, we avoid "non-deleted" user key in case of failure
    bool resEncrypt = encryptor->encrypt(plain, ciphered, userKey);

    delete encryptor;
    encryptor = new Encryptor();
    std::string clear;
    bool resDecrypt = encryptor->decrypt(ciphered, clear);

    EXPECT_EQ(encryptor->deleteKey(userKey), true);
    EXPECT_EQ(resEncrypt, true);
    EXPECT_NE(ciphered, plain);
    EXPECT_EQ(clear, plain);
}