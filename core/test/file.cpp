#include "file.hpp"
#include "test.hpp"

class FileTest : public File
{
public:
    FileTest(std::string filename) : File(filename)
    {
    }

    ~FileTest(void)
    {
    }

    std::list<std::string> parse(void)
    {
        return std::list<std::string>();
    }
};

TEST(file, ReadWrite)
{
    FileTest file("test.txt");
    file.write("Test");
    std::string res = file.read();

    EXPECT_EQ(res, "Test");
}