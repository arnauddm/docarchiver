#include "test.hpp"
#include "folderScanner.hpp"
#include "tools.hpp"

#include <iterator>
#include <algorithm>

TEST(FolderScanner, ScanFilesFolder) {
    const std::string currentFolder = currentSourceFileDirectory(__FILE__);

    FolderScanner scanner(currentFolder + "/files");

    std::list<std::string> expectedFiles;
    expectedFiles.push_back(currentFolder + "/files/macosPages.pdf");
    expectedFiles.push_back(currentFolder + "/files/microsoftWord.pdf");
    expectedFiles.push_back(currentFolder + "/files/multipages.pdf");
    expectedFiles.push_back(currentFolder + "/files/parenthesis.pdf");

    std::list<std::string> result = scanner.scan();

    EXPECT_EQ(expectedFiles.size(), result.size());

    for(std::list<std::string>::iterator it = result.begin(); it != result.end(); it++)
    {
        const bool exist = std::distance(expectedFiles.end(), std::find(expectedFiles.begin(), expectedFiles.end(), *it));
        EXPECT_EQ(exist, true);
    }
}