#include "jsonArchive.hpp"
#include "file.hpp"
#include "test.hpp"
#include "textParser.hpp"
#include "tools.hpp"
#include "encryptor.hpp"

class FileTest : public File
{
public:
    FileTest(std::string filename) : File(filename)
    {
    }

    ~FileTest(void)
    {
    }

    std::list<std::string> parse(void)
    {
        return std::list<std::string>();
    }
};

TEST(JsonArchive, SaveArchive)
{
    JsonArchive jsonArchive("archive.json");

    ArchivedFile file("myPath.txt");
    file.addKeyword("keyword1");

    jsonArchive.addFile(&file);

    EXPECT_EQ(jsonArchive.save(), true);

    std::string expectedOutput = "{\"files\" : [{\"name\" : \"myPath.txt\",\"path\" : \"myPath.txt\",\"personnalKeywords\" : []}]}";
    FileTest f("archive.json");
    std::string content = f.read();
    content = TextParser::replaceSpecialChar(f.read());
    EXPECT_EQ(content, expectedOutput);

    FileTest::remove("archive.json");
}

TEST(JsonArchive, WriteAndReadArchive)
{
    const std::string filePath = currentSourceFileDirectory(__FILE__) + "/../files/parenthesis.pdf";

    JsonArchive jsonArchive("archive.json");

    ArchivedFile file(filePath);

    jsonArchive.addFile(&file);

    EXPECT_EQ(jsonArchive.save(), true);

    EXPECT_EQ(jsonArchive.read(), true);

    std::map<std::string, ArchivedFile *> files = jsonArchive.allFiles();
    EXPECT_EQ(files.size(), 1);
    EXPECT_EQ(((files.begin()->second))->name(), "parenthesis.pdf");
}

TEST(JsonArchive, WriteAndReadEncryptedArchive)
{
    const std::string filePath = currentSourceFileDirectory(__FILE__) + "/../files/parenthesis.pdf";
    const std::string userKey = "WriteAndReadEncryptedArchive@test.com";

    Encryptor encryptor;
    EXPECT_EQ(encryptor.createKeyNoPassword(userKey), true);

    JsonArchive jsonArchive("archive.json");

    ArchivedFile file(filePath);

    jsonArchive.addFile(&file);

    EXPECT_EQ(jsonArchive.saveWithEncryption(userKey), true);

    EXPECT_EQ(jsonArchive.read(), true);

    File::remove(filePath);
    EXPECT_EQ(encryptor.deleteKey(userKey), true);

    std::map<std::string, ArchivedFile *> files = jsonArchive.allFiles();
    EXPECT_EQ(files.size(), 1);
    EXPECT_EQ(((files.begin()->second))->name(), "parenthesis.pdf");
}

TEST(JsonArchive, ArchiveContent)
{
    JsonArchive jsonArchive("archive.json");
    jsonArchive.addFile(
        new ArchivedFile("testFile.txt")
    );
    EXPECT_EQ(jsonArchive.save(), true);

    std::string expectedOutput = "{\n\t\"files\" : \n\t[\n\t\t{\n\t\t\t\"name\" : \"testFile.txt\",\n\t\t\t\"path\" : \"testFile.txt\",\n\t\t\t\"personnalKeywords\" : []\n\t\t}\n\t]\n}";
    EXPECT_EQ(jsonArchive.content(), expectedOutput);
}

TEST(JsonArchive, ReWriteEncryptedToClear)
{
    const std::string filePath = currentSourceFileDirectory(__FILE__) + "/../files/parenthesis.pdf";
    const std::string userKey = "ReWriteEncryptedToClear@test.com";

    Encryptor encryptor;
    EXPECT_EQ(encryptor.createKeyNoPassword(userKey), true);

    JsonArchive jsonArchive("archive.json");

    ArchivedFile file(filePath);

    jsonArchive.addFile(&file);

    EXPECT_EQ(jsonArchive.saveWithEncryption(userKey), true);

    EXPECT_EQ(jsonArchive.read(), true);

    EXPECT_EQ(jsonArchive.save(), true);

    EXPECT_EQ(encryptor.deleteKey(userKey), true);
}