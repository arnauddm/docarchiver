#include "test.hpp"
#include "context.hpp"
#include "textFile.hpp"
#include "jsonConfiguration.hpp"


TEST(JsonConfiguration, ConstructorDestructor)
{
    JsonConfiguration * conf = new JsonConfiguration();
    delete conf;
}

TEST(JsonConfiguration, DefaultConfiguration)
{
    EXPECT_EQ(Context::getInstance()->configuration()->archiveDirectory(), ".");
}

TEST(JsonConfiguration, CustomConfiguration)
{
    std::string customConfiguration = "{\n\t\"archiveDirectory\": \"/var\"\n}";
    TextFile jsonFile("configuration.json");
    jsonFile.write(customConfiguration);

    Context::resetInstance();

    EXPECT_EQ(Context::getInstance()->configuration()->archiveDirectory(), "/var");
}

TEST(JsonConfiguration, FileTypeMapping)
{
    std::string customConfiguration = "{\n\t\"fileTypes\":\n\t{\n\t\t\"txt\": \"txxt\",\n\t\t\"pdf\": \"pddf\"\n\t}\n}";
    TextFile jsonFile("configuration.json");
    jsonFile.write(customConfiguration);

    Context::resetInstance();

    std::map<std::string, std::string> fileTypesMapping = Context::getInstance()->configuration()->fileTypesMapping();

    EXPECT_EQ(fileTypesMapping["txt"], "txxt");
    EXPECT_EQ(fileTypesMapping["pdf"], "pddf");
}

TEST(JsonConfiguration, NotGoodConfiguration)
{
    TextFile file("configuration.json");
    EXPECT_EQ(file.write("Hello world"), true);

    Context::resetInstance();
    EXPECT_EQ(Context::getInstance()->configuration()->archiveDirectory(), ".");
}