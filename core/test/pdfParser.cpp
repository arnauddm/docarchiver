#include "pdfFile.hpp"
#include "test.hpp"
#include "tools.hpp"

TEST(PdfFile, ParserConstructorDestructor)
{
    PdfParser * parser = new PdfParser();
    delete parser;
}

TEST(Pdf, NonExistentFile)
{
    PdfFile file("blabla.pdf");
    EXPECT_EQ(file.parse().size(), 0);
}

TEST(PdfFile, ParseWithMacosPagesPDF)
{
    const std::string content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."; // => 19 "keywords"
    std::list<std::string> expectedList;
    {
        TextParser parser;
        expectedList = parser.parse(parser.replaceIllegalChar(content));
    }

    PdfFile file(currentSourceFileDirectory(__FILE__) + "/files/macosPages.pdf");

    std::list<std::string> result = file.parse();

        // Check the size
    EXPECT_EQ(result.size(), expectedList.size());

    // Check the content
    std::list<std::string>::iterator itResult;
    std::list<std::string>::iterator itExpected;
    for (itResult = result.begin(), itExpected = expectedList.begin();
        itResult != result.end();
        itResult++, itExpected++)
    {
        EXPECT_EQ(*itResult, *itExpected);
    }
}

TEST(PdfFile, ParseWithMicrosoftWordPDF)
{
    const std::string content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."; // => 10 "keywords"

    std::list<std::string> expectedList;
    {
        TextParser parser;
        expectedList = parser.parse(parser.replaceIllegalChar(content));
    }

    PdfFile file(currentSourceFileDirectory(__FILE__) + "/files/microsoftWord.pdf");
    std::list<std::string> result = file.parse();

    // Check the size
    EXPECT_EQ(result.size(), expectedList.size());

    // Check the content
    std::list<std::string>::iterator itResult;
    std::list<std::string>::iterator itExpected;
    for (itResult = result.begin(), itExpected = expectedList.begin();
        itResult != result.end();
        itResult++, itExpected++)
    {
        EXPECT_EQ(*itResult, *itExpected);
    }
}

TEST(PdfFile, ParseWithMultiPagePdf)
{
    const std::string content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."; // => 19 "keywords"
    std::list<std::string> expectedList;
    {
        TextParser parser;
        expectedList = parser.parse(parser.replaceIllegalChar(content + " " + content));
    }

    PdfFile file(currentSourceFileDirectory(__FILE__) + "/files/multipages.pdf");

    std::list<std::string> result = file.parse();

    // Check the size
    EXPECT_EQ(result.size(), expectedList.size());

    // Check the content
    std::list<std::string>::iterator itResult;
    std::list<std::string>::iterator itExpected;
    for (itResult = result.begin(), itExpected = expectedList.begin();
        itResult != result.end();
        itResult++, itExpected++)
    {
        EXPECT_EQ(*itResult, *itExpected);
    }
}

TEST(PdfFile, ParseWithParenthesis)
{
    const std::string content = "Test with (some) parenthesis in this doc(ument).";
    std::list<std::string> expectedList;
    {
        TextParser parser;
        expectedList = parser.parse(parser.replaceIllegalChar(content));
    }

    PdfFile file(currentSourceFileDirectory(__FILE__) +  "/files/parenthesis.pdf");

    std::list<std::string> result = file.parse();

    // Check the size
    EXPECT_EQ(result.size(), expectedList.size());

    // Check the content
    std::list<std::string>::iterator itResult;
    std::list<std::string>::iterator itExpected;
    for (itResult = result.begin(), itExpected = expectedList.begin();
        itResult != result.end();
        itResult++, itExpected++)
    {
        EXPECT_EQ(*itResult, *itExpected);
    }
}