#include "textFile.hpp"
#include "test.hpp"

class FileTest : public File
{
public:
    FileTest(std::string filename) : File(filename)
    {
    }

    ~FileTest(void)
    {
    }

    std::list<std::string> parse(void)
    {
        return std::list<std::string>();
    }
};

TEST(textFile, SimpleParse)
{
    FileTest file("test.txt");
    file.write("Hello World!");

    TextFile textFile("test.txt");

    std::list<std::string> result = textFile.parse(textFile.read());

    EXPECT_EQ(result.size(), 2);
    
    std::list<std::string>::iterator it = result.begin();
    EXPECT_EQ(*it, "Hello");

    std::advance(it, 1);
    EXPECT_EQ(*it, "World");

    remove("test.txt");
}