#include "textParser.hpp"
#include "test.hpp"

class TestTextParser : public TextParser
{
public:
    static std::string replaceIllegalChar(std::string content)
    {
        return TextParser::replaceIllegalChar(content);
    }
};

TEST(textParser, ReplaceIllegalChar)
{
    std::string from = "Test with, (illegal); [char] !?";

    EXPECT_EQ(TestTextParser::replaceIllegalChar(from), "Test with illegal char ");
}

TEST(textParser, parseSingleWord)
{
    std::string str = "Word";

    TextParser parser;
    std::list<std::string> result = parser.parse(str);
    std::list<std::string>::iterator it = result.begin();

    EXPECT_EQ(result.size(), 1); // Single word
    EXPECT_EQ(*it, "Word");
}

TEST(textParser, parseString)
{
    const int size = 5;
    std::string keywords[size] = {"This", "is", "an", "official", "document"};

    std::string str = "";
    for (unsigned int i = 0; i < size; i++)
    {
        if (i != 0)
            str += " ";
        str += keywords[i];
    }
    str = "? " + str + ".";

    TextParser parser;
    std::list<std::string> result = parser.parse(str);
    std::list<std::string>::iterator it = result.begin();

    EXPECT_EQ(result.size(), size); // 5 words
    for (unsigned int i = 0; i < size; i++)
    {
        EXPECT_EQ(*it, keywords[i]);
        std::advance(it, 1);
    }
}