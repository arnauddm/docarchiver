#include "additionalKeywordsEditor.hpp"
#include <QString>

/**
 * @brief Construct a new Additional Keywords Editor:: Additional Keywords Editor object
 * 
 */
AdditionalKeywordsEditor::AdditionalKeywordsEditor(void)
{
    setupUi(this);

    QObject::connect(keywordsEditorLineEdit, SIGNAL(textChanged(QString)), this, SLOT(updatekeywords(void)));
}

/**
 * @brief Destroy the Additional Keywords Editor:: Additional Keywords Editor object
 * 
 */
AdditionalKeywordsEditor::~AdditionalKeywordsEditor(void)
{

}

/**
 * @brief Returns additional keywords
 * 
 * @return std::list<std::string> Additional keywords list
 */
std::list<std::string> AdditionalKeywordsEditor::additionalKeywords(void)
{
    return _keywords;
}

/**
 * @brief Update @ref _keywords and view when focus on the line edit is lost
 * 
 */
void AdditionalKeywordsEditor::updatekeywords(void)
{
    _keywords.clear();
    keywordsViewerListWidget->clear();

    QStringList content = keywordsEditorLineEdit->text().split(" ");
    for(int i = 0; i < content.size(); i++)
    {
        keywordsViewerListWidget->addItem(content.at(i));
        _keywords.push_back(content.at(i).toStdString());
    }
}