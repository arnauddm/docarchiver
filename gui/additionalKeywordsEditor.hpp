#ifndef __ADDITIONAL_KEYWORDS_EDITOR_HPP__
#define __ADDITIONAL_KEYWORDS_EDITOR_HPP__

#include <QDialog>
#include <list>
#include <string>
#include "ui_additionalKeywordsEditor.h"

class AdditionalKeywordsEditor : public QDialog, public Ui_AdditionalKeywordsEditor
{
    Q_OBJECT
    public:
        AdditionalKeywordsEditor(void);
        ~AdditionalKeywordsEditor(void);

        std::list<std::string> additionalKeywords(void);
    private:
        std::list<std::string> _keywords;

    private slots:
        void updatekeywords(void);

};

#endif