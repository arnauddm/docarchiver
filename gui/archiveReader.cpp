#include "archiveReader.hpp"
#include "archiveFactory.hpp"

#include <QPlainTextEdit>

/**
 * @brief Construct a new Archive Reader:: Archive Reader object
 * 
 */
ArchiveReader::ArchiveReader(void)
{
    setupUi(this);

    QString raw = QString::fromStdString(
        ArchiveFactory::get()->content());

    
    this->renderer->setText(raw.replace("\t", "    "));
}

/**
 * @brief Destroy the Archive Reader:: Archive Reader object
 * 
 */
ArchiveReader::~ArchiveReader(void)
{

}