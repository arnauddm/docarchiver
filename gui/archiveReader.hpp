#ifndef __ARCHIVE_READER_HPP__
#define __ARCHIVE_READER_HPP__

#include "filter.hpp"

#include <QDialog>

#include "ui_archiveReader.h"

class ArchiveReader : public QDialog, public Ui_ArchiveReader
{
    Q_OBJECT

public:
    ArchiveReader(void);
    ~ArchiveReader(void);
};
#endif