#include "docChiver.hpp"
#include "archiveFactory.hpp"
#include "archivedFile.hpp"
#include "archiveReader.hpp"
#include "folderScanner.hpp"
#include "actionsProviderFactory.hpp"
#include "additionalKeywordsEditor.hpp"
#include "archiveChecker.hpp"
#include "scriptEditor.hpp"
#include "textFile.hpp"

#include <QTableWidgetItem>
#include <QString>
#include <QTableWidget>
#include <QFileDialog>
#include <QMessageBox>
#include <QStringList>
#include <QInputDialog>
#include <QMap>
#include <QLayoutItem>
#include <QTreeWidgetItem>

#include <iostream>
#include <sstream>
#include <iterator>

#include <QDebug>

/**
 * @brief Construct a new Doc Chiver:: Doc Chiver object
 * 
 * @param parent Parent of this instance
 */
DocChiver::DocChiver(QWidget *parent) : QMainWindow(parent)
{
    setupUi(this);

    QFileDialog openArchive;
    openArchive.setFileMode(QFileDialog::AnyFile);
    openArchive.setWindowTitle("Create or open an archive");
    openArchive.setFocus();
    openArchive.setAcceptMode(QFileDialog::AcceptOpen);

    QString archivePath;
    int res;
    if(!openArchive.exec())
    {
        if(QMessageBox::question(this, "Create a new archive ?", "No archive select... Do you want to create new one ?", QMessageBox::StandardButton::Yes, QMessageBox::StandardButton::No) == QMessageBox::StandardButton::No)
            this->close();
        else
            archivePath = QFileDialog::getSaveFileName(this, "Archive", "Please select the destination");

    }
    else
        archivePath = openArchive.selectedFiles().at(0);

    if(QFile::exists(archivePath))
        ArchiveFactory::setArchivePath(archivePath.toStdString());
    else
        ArchiveFactory::newOne(archivePath.toStdString());

    init();

    _cliParser = new CliParser(ActionsProviderFactory::build());
}

/**
 * @brief Destroy the Doc Chiver:: Doc Chiver object
 * 
 */
DocChiver::~DocChiver(void)
{
    qDebug() << "Quiting...";

    if (_filter)
        delete _filter;

    if (_archive)
        delete _archive;
}

/**
 * @brief Initialize all the Gui
 * 
 */
void DocChiver::init(void)
{
    _filter = new Filter();

    this->statusProgressBar->setEnabled(false);

    QObject::connect(
        this,
        SIGNAL(archiveChanged()),
        this,
        SLOT(refreshArchiveTable()));
    this->statusProgressBar->setValue(100);

    QObject::connect(
        this->filesTreeWidget,
        SIGNAL(itemClicked(QTreeWidgetItem *, int)),
        this,
        SLOT(showKeywordsList(QTreeWidgetItem *, int)));

    emit archiveChanged();

    this->statusLabel->setText("Archive is well imported");
}

/**
 * @brief Refresh the table's content which display archived files
 * 
 */
void DocChiver::refreshArchiveTable(void)
{
    this->filesTreeWidget->clear();

    // Save and clear all extensions checkBoxes
    std::list<std::string> exts;
    for(int row = 0; row < this->extensionsGridLayout->rowCount(); row++)
    {
        for(int column = 0; column < this->extensionsGridLayout->columnCount(); column++)
        {
            QLayoutItem * item = this->extensionsGridLayout->itemAtPosition(row, column);
            if(!item) continue;

            QCheckBox * box;
            if(item->widget())
            {
                if((box = dynamic_cast<QCheckBox *>(item->widget())) && box->isChecked())
                    exts.push_back(box->text().toStdString());
                this->extensionsGridLayout->removeWidget(item->widget());
            }
            else
            {
                this->extensionsGridLayout->removeItem(item);
            }
        }
    }
    _extensionsCheckBoxes.clear();

    _archive = ArchiveFactory::get();
    std::map<std::string, ArchivedFile *> allFiles = _archive->allFiles();
    _archivedFiles.clear();

    for (std::map<std::string, ArchivedFile *>::const_iterator it = allFiles.cbegin(); it != allFiles.cend(); it++)
    {
        const std::string & extension = it->second->extension();
        QCheckBox * box = new QCheckBox(QString::fromStdString(extension));

        _extensionsCheckBoxes.insert({extension, box});
        this->extensionsGridLayout->addWidget(box, _extensionsCheckBoxes.size() / 3, _extensionsCheckBoxes.size() % 3);
        if(!(exts.size() > 0 && std::find(exts.begin(), exts.end(), it->second->extension()) == exts.end()))
        {
            _archivedFiles.append(it->second);
            addFileToArchiveTable(it->second);
            box->setChecked(true);
        }
        QObject::connect(box, SIGNAL(stateChanged(int)), this, SLOT(refreshArchiveTable(void)));
    }
}

/**
 * @brief Add a new file (row) into the "archived file table"
 * 
 * @param itFile 
 */
void DocChiver::addFileToArchiveTable(ArchivedFile * file)
{
    const std::string filepath = file->path();
    QTreeWidgetItem * item = new QTreeWidgetItem(filesTreeWidget);
    item->setText(0, QString::fromStdString(filepath));
    filesTreeWidget->addTopLevelItem(item);
}

/**
 * @brief Slot called on "add file" menu bar button
 * 
 */
void DocChiver::on_addFileAction_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    ".",
                                                    tr("*.txt *.pdf"));

    if(fileName.isEmpty())
    {
        QMessageBox::warning(this, "No file", "No file selected...");
        return;
    }

    ArchivedFile archivedFile(fileName.toStdString());

    AdditionalKeywordsEditor editor;
    if(editor.exec() == QDialog::Accepted)
        archivedFile.addPersonnalKeywords(editor.additionalKeywords());

    Archive * archive = ArchiveFactory::get();
    archive->addFile(&archivedFile);
    
    if(archive->save())
        QMessageBox::information(this, "Success", "File successfully added !");
    else
        QMessageBox::critical(this, "Error", "An error occured...");

    emit archiveChanged();
}

/**
 * @brief Slot called on "Remove file" menu bar button
 * 
 */
void DocChiver::on_removeFileAction_triggered(void)
{
    QStringList files;
    _archive = ArchiveFactory::get();
    std::map<std::string, ArchivedFile *> allFiles = _archive->allFiles();
    for (std::map<std::string, ArchivedFile *>::iterator it = allFiles.begin(); it != allFiles.end(); it++)
        files << QString::fromStdString(it->first);

    QString fileToRemove = QInputDialog::getItem(this, "Remove a file", "Remove a file from the archive", files);

    if(ArchiveFactory::get()->removeFileWithPath(fileToRemove.toStdString()))
        QMessageBox::information(this, "Success", "File removed successfully");
    else
        QMessageBox::warning(this, "Error", "An error occured");

    emit archiveChanged();
}

/**
 * @brief Display archive's content in a pop-up
 * 
 */
void DocChiver::on_showArchiveAction_triggered(void)
{
    ArchiveReader reader;
    reader.exec();
}

/**
 * @brief List all keywords of a file in a pop-up
 * 
 * @param row Row of the table which was clicked
 * @param column Column of the table which was clicked
 */
void DocChiver::showKeywordsList(QTreeWidgetItem * item, int column)
{
    const std::string filepath = item->text(0).toStdString();
    
    ArchivedFile * file = nullptr;
    for(int i = 0; i < _archivedFiles.size() && !file; i++)
    {
        if(_archivedFiles.at(i)->path() == filepath)
            file = _archivedFiles.at(i);
    }
    if(!file)
        return;

    std::map<std::string, int> keywords = file->keywords();
    // keywords.insert(file->personnalKeywords().begin(), file->personnalKeywords().end());

    this->keywordsTableWidget->clear();
    this->keywordsTableWidget->setRowCount(keywords.size());
    this->keywordsTableWidget->setColumnCount(2);
    for(std::map<std::string, int>::const_iterator it = keywords.cbegin(); it != keywords.cend(); it++)
    {
        const int row = std::distance(keywords.cbegin(), it);
        this->keywordsTableWidget->setItem(row, 0, new QTableWidgetItem(QString::fromStdString(it->first)));
        this->keywordsTableWidget->setItem(row, 1, new QTableWidgetItem(QString::number(it->second)));
    }
}

/**
 * @brief Scan the folder selected via a pop-up
 * 
 */
void DocChiver::on_scanFolderAction_triggered(void)
{
    QString folder = QFileDialog::getExistingDirectory(this, "Folder", "Select the folder you want to analyse (recursivelly)");
    if(folder.isEmpty())
    {
        QMessageBox::information(this, "No folder selected", "No folder selected.. Aborting this operation..");
        return;
    }

    QString extensions = QInputDialog::getText(this, "Extensions", "Fill in extensions you want to scan recursivelly. Example : \"txt pdf\" or \"txt ; pdf\" or \"txt / pdf\" (Separator is free to choose)");

    FolderScanner scanner(folder.toStdString());
    std::list<std::string> files = scanner.scan();

    QString output = "";

    for(std::list<std::string>::iterator it = files.begin(); it != files.end(); it++)
    {
        if(!extensions.isEmpty() && !extensions.contains(QString::fromStdString(*it)))
            continue;

        ArchivedFile * file = new ArchivedFile(*it);
        _archive->addFile(file);    
    }

    if(_archive->save())
        QMessageBox::information(this, "Folder scanned", folder + " is successfully scanned");
    else
        QMessageBox::warning(this, "Error", "An error occured");

    emit archiveChanged();
}

/**
 * @brief Slot called when return key is pressed under the "command line input"
 * 
 */
void DocChiver::on_commandLineInputLineEdit_returnPressed(void)
{
    const QString cmd = commandLineInputLineEdit->text();
    if(cmd.isEmpty())
        return;

    commandLineInputLineEdit->clear();

    commandLineOutputPlainTextEdit->appendPlainText(QString("> ") + cmd);

    Action * action = _cliParser->parse(cmd.toStdString());
    
    QString cmdOut;
    if(action)
        cmdOut = QString::fromStdString(action->doAction());
    else
        cmdOut = QString("Error : '") + cmd + QString("' not found");
    
    commandLineOutputPlainTextEdit->appendPlainText(cmdOut);
}

/**
 * @brief Save the archive (in clear | default mode)
 * 
 */
void DocChiver::on_saveAction_triggered(void)
{
    if(_archive->save())
        QMessageBox::information(this, "Archive saved", "Archive successfully saved");
    else
        QMessageBox::warning(this, "Archive not saved", "An error occured...");
}

/**
 * @brief Save the archive with another name
 * 
 */
void DocChiver::on_saveAsAction_triggered(void)
{
    QString archivePath = QFileDialog::getSaveFileName(this, "Select the archive", "Please select the archive file to use");
    
    if(archivePath.isEmpty())
    {
        QMessageBox::warning(this, "Empty filename", "No filename selected...");
        return;
    }

    _archive->setArchivePath(archivePath.toStdString());
    on_saveAction_triggered();
}

/**
 * @brief Save the archive by encrypting it
 * 
 */
void DocChiver::on_saveEncryptionAction_triggered(void)
{
    QString userKey = QInputDialog::getText(this, "GPG Key", "Enter your GPG key id");
    if(userKey.isEmpty())
    {
        QMessageBox::warning(this, "Empty GPG key", "No GPG key...");
        return;
    }

    if(_archive->saveWithEncryption(userKey.toStdString()))
        QMessageBox::information(this, "Archive saved", "Archive successfully saved");
    else
        QMessageBox::warning(this, "Archive not saved", "An error occured...");
}

/**
 * @brief Create a new empty archive
 * 
 */
void DocChiver::on_newEmptyArchiveAction_triggered(void)
{
    QString name = QInputDialog::getText(this, "Archive name", "Choose the name of your new archive (eg: archive.json)");
    
    if(name.isEmpty())
    {
        QMessageBox::warning(this, "Empty name", "Archive name can't be empty");
        return;
    }

    if(!name.contains('.'))
    {
        QMessageBox::warning(this, "Name format", "The archive name must contains the extension...");
        return;
    }

    Archive * archive = ArchiveFactory::newOne(name.toStdString());
    if(archive)
        delete _archive;
    _archive = archive;
    QMessageBox::information(this, "Archive created", "Archive created !");
}

/**
 * @brief Check if the archive contains error
 * 
 */
void DocChiver::on_checkArchiveAction_triggered(void)
{
    Archive * archive = ArchiveFactory::get();
    ArchiveChecker checker;
    checker.setArchive(archive);

    Checker::Errors errors = checker.check();
    std::stringstream ss;
    if(errors.size() == 0)
    {
        ss << "No error found";
    }
    else
    {
        for(Checker::Errors::iterator it = errors.begin(); it != errors.end(); it++)
            ss << *it << "\n";
    }

    if(errors.size() == 0)
        QMessageBox::information(this, "Archive check", QString::fromStdString(ss.str()));
    else
        QMessageBox::warning(this, "Archive check", QString::fromStdString(ss.str()));
}

/**
 * @brief Create and edit a new script
 * 
 */
void DocChiver::on_newScriptAction_triggered(void)
{
    ScriptEditor editor;
    int result;

    if(editor.exec() == QDialog::Accepted)
    {
        QString content = editor.scriptContent();
        QString scriptPath = QFileDialog::getSaveFileName(this, "Select the archive", "Please select the archive file to use");
    
        if(scriptPath.isEmpty())
        {
            QMessageBox::warning(this, "Empty filename", "No filename selected...");
            return;
        }

        TextFile file(scriptPath.toStdString());
        if(!file.write(content.toStdString()))
            QMessageBox::warning(this, "Error", "Cannot save the script...");
    }
}

/**
 * @brief Open and edit an existing script
 * 
 */
void DocChiver::on_editScriptAction_triggered(void)
{
    QString scriptPath = QFileDialog::getOpenFileName(this, "Select the archive", "Please select the archive file to use");
    if(scriptPath.isEmpty())
    {
        QMessageBox::warning(this, "Error", "Script path cannot be empty...");
        return;
    }

    ScriptEditor editor(scriptPath);
    int result;
    if(editor.exec() == QDialog::Accepted)
    {
        QString content = editor.scriptContent();
    
        TextFile file(scriptPath.toStdString());
        if(!file.write(content.toStdString()))
            QMessageBox::warning(this, "Error", "Cannot save the script...");
    }
}