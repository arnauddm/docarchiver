#ifndef __DOC_CHIVER_HPP__
#define __DOC_CHIVER_HPP__

#include <QMainWindow>
#include <QVector>
#include <QCheckBox>
#include "ui_docChiver.h"
#include "archive.hpp"
#include "filter.hpp"
#include "cliParser.hpp"

class DocChiver : public QMainWindow, Ui_DocChiver
{
    Q_OBJECT
public:
    DocChiver(QWidget * parent = nullptr);
    ~DocChiver(void);

private:
    void init(void);
    void addFileToArchiveTable(ArchivedFile * file);

    Archive * _archive;
    Filter * _filter;
    CliParser * _cliParser;


    QVector<ArchivedFile *> _archivedFiles;
    std::map<std::string, QCheckBox *> _extensionsCheckBoxes;

    const unsigned int NAME_COLUMN = 0;
    const unsigned int KEYWORDS_COUNT_COLUMN = NAME_COLUMN + 1;
    const unsigned int PATH_COLUMN = KEYWORDS_COUNT_COLUMN + 1;

signals:
    void archiveChanged(void);

private slots:
    void refreshArchiveTable(void);
    void showKeywordsList(QTreeWidgetItem * item, int column);

// Slots connected to GUI items (buttons, actions, ...)
private slots:
    void on_addFileAction_triggered(void);
    void on_removeFileAction_triggered(void);
    void on_scanFolderAction_triggered(void);

    void on_showArchiveAction_triggered(void);
    void on_checkArchiveAction_triggered(void);

    void on_saveAction_triggered(void);
    void on_saveAsAction_triggered(void);
    void on_saveEncryptionAction_triggered(void);

    void on_newEmptyArchiveAction_triggered(void);

    void on_commandLineInputLineEdit_returnPressed(void);

    void on_newScriptAction_triggered(void);
    void on_editScriptAction_triggered(void);

};

#endif