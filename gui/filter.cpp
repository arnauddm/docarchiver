#include "filter.hpp"

/**
 * @brief Construct a new Filter:: Filter object
 * 
 */
Filter::Filter(void)
{

}

/**
 * @brief Destroy the Filter:: Filter object
 * 
 */
Filter::~Filter(void)
{

}

/**
 * @brief Add a new extension
 * 
 * @param extension Extension to add
 */
void Filter::addExtension(const std::string & extension)
{
    _extensions.push_back(extension);
}

/**
 * @brief Returns the extensions list
 * 
 * @return std::list<std::string> All extensions
 */
std::list<std::string> Filter::extensions(void)
{
    return _extensions;
}

/**
 * @brief Add a keyword
 * 
 * @param keyword Keyword to add
 */
void Filter::addKeyword(const std::string & keyword)
{
    _keywords.push_back(keyword);
}

/**
 * @brief Returns all keywords
 * 
 * @return std::list<std::string> All keywords
 */
std::list<std::string> Filter::keywords(void)
{
    return _keywords;
}

/**
 * @brief Add a path where to start the research
 * 
 * @param path 
 */
void Filter::addPath(const std::string & path)
{
    _paths.push_back(path);
}

/**
 * @brief Returns all paths
 * 
 * @return std::list<std::string> All paths
 */
std::list<std::string> Filter::paths(void)
{
    return _paths;
}