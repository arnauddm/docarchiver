#ifndef __FILTER_HPP__
#define __FILTER_HPP__

#include <string>
#include <list>

class Filter
{
public:
    Filter(void);
    ~Filter(void);

    void addExtension(const std::string & extension);
    std::list<std::string> extensions(void);

    void addKeyword(const std::string & keyword);
    std::list<std::string> keywords(void);
    
    void addPath(const std::string & path);
    std::list<std::string> paths(void);

private:
    std::list<std::string> _extensions;
    std::list<std::string> _keywords;
    std::list<std::string> _paths;
};

#endif