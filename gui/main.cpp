#include <QApplication>
#include "docChiver.hpp"

/**
 * @brief Entry point of the application
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
int main(int argc, char * argv[])
{
    QApplication a(argc, argv);
    a.setQuitOnLastWindowClosed(true);
    DocChiver w;
    w.show();

    return a.exec();
}