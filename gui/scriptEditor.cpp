#include "scriptEditor.hpp"
#include "actionsProviderFactory.hpp"
#include "textFile.hpp"

#include <QTextStream>

/**
 * @brief Construct a new Script Editor:: Script Editor object
 * 
 */
ScriptEditor::ScriptEditor(void) : 
    _cliParser(ActionsProviderFactory::build()),
    _filename("Unsaved")
{
    setupUi(this);
    editorPlainTextEdit->setLineWrapMode(QPlainTextEdit::WidgetWidth);
}

/**
 * @brief Construct a new Script Editor:: Script Editor object
 * 
 * @param filename Filename of the existing archive
 */
ScriptEditor::ScriptEditor(QString filename) : ScriptEditor()
{
    _filename = filename;
    TextFile file(filename.toStdString());
    editorPlainTextEdit->setPlainText(QString::fromStdString(file.read()));
    on_checkEditorPushButton_clicked();
}

/**
 * @brief Destroy the Script Editor:: Script Editor object
 * 
 */
ScriptEditor::~ScriptEditor(void)
{

}

/**
 * @brief Check if the current script (that the user is currently typing) contains errors
 * 
 */
void ScriptEditor::on_checkEditorPushButton_clicked(void)
{
    QStringList lines = editorPlainTextEdit->toPlainText().split("\n");

    for(unsigned int i = 0; i < lines.size(); i++)
    {
        QString line = lines.at(i);
        Action * action = _cliParser.parse(line.toStdString());
        if(!action)
            line = "<span style='background-color: red'>" + line + "</span>";
        else if(!action->isCorrectOptions())
            line = "<span style='background-color: orange'>" + line + "</span>";
        
        lines.replace(i, line);
    }

    editorPlainTextEdit->clear();
    editorPlainTextEdit->appendHtml(lines.join("<br/>"));
}

/**
 * @brief Returns the script content
 * 
 * @return QString Script content
 */
QString ScriptEditor::scriptContent(void)
{
    return editorPlainTextEdit->toPlainText();
}