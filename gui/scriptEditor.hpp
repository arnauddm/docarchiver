#ifndef __SCRIPT_EDITOR_GUI_HPP__
#define __SCRIPT_EDITOR_GUI_HPP__

#include "ui_scriptEditor.h"

#include <QListWidgetItem>
#include <QDialog>

#include "cliParser.hpp"

class ScriptEditor : public QDialog, public Ui_ScriptEditor
{
    Q_OBJECT
    public:
        ScriptEditor(void);
        ScriptEditor(QString filename);
        ~ScriptEditor(void);

        QString scriptContent(void);

    private:
        CliParser _cliParser;
        QString _filename;

    private slots:
        void on_checkEditorPushButton_clicked(void);
};

#endif