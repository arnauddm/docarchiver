project(TestingSrc)

FILE (GLOB SOURCES *.cpp)
FILE (GLOB HEADERS *.hpp)

find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})

add_library(testing STATIC ${HEADERS} ${SOURCES})
target_link_libraries(testing ${GTEST_LIBRARIES})