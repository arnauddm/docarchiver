#ifndef __TEST_HPP__
#define __TEST_HPP__

#include <gtest/gtest.h>

class Test : public testing::Test {
    public:
        Test(void);
        ~Test(void);

        // Overriden methods
        void SetUp(void) override;
        void TearDown(void) override;
};

#ifdef TEST
#undef TEST
#endif
#define TEST(test_suite_name, test_name) GTEST_TEST(test_suite_name, test_name)

#ifdef GTEST_TEST
#undef GTEST_TEST
#endif
#define GTEST_TEST(test_suite_name, test_name)             \
  GTEST_TEST_(test_suite_name, test_name, Test, \
              ::testing::internal::GetTestTypeId())

#endif