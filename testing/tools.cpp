#include <string>

std::string currentSourceFileDirectory(const std::string & filename)
{
    return filename.substr(0, filename.rfind("/"));
}