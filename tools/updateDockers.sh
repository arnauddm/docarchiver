#!/bin/bash

if [ $# -ne 1 ]
then
    echo "You must provide 'parent' directory of docker images"
    exit -1
fi

ROOT=$1

folders=$(find $ROOT -type d)
for folder in $folders
do
    if [ "$folder" == "$ROOT" ]
    then
        continue
    fi

    name=${folder##*/}

    docker build -t registry.gitlab.com/arnauddm/docarchiver:${name} $folder
    docker push registry.gitlab.com/arnauddm/docarchiver:${name}
done